//
// (C) Copyright 2020 Tony Mason
//

#if defined(__linux__)
#include <linux/sysincludes.h>
#else
#error "Unknown platform"
#endif
