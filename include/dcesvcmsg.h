/* Generated from ../msg/svc.sams on 2021-01-20-00:13:42.000 */
/* Do not edit! */
#if	!defined(_DCE_DCESVCMSG_)
#define _DCE_DCESVCMSG_
#define svc_s_ok                        0x00000000
#define msg_s_ok                        0x00000000
#define svc_s_no_memory                 0x173ca001
#define svc_s_unknown_component         0x173ca002
#define svc_s_no_filter                 0x173ca003
#define svc_s_bad_routespec             0x173ca004
#define svc_s_cantopen                  0x173ca005
#define svc_s_at_end                    0x173ca006
#define svc_s_assertion_failed          0x173ca007
#define svc_s_no_stats                  0x173ca008
#define svc_s_no_perm                   0x173ca009
#define svc_s_acl_corrupt               0x173ca00a
#define svc_s_acl_open_error            0x173ca00b
#define svc_s_acl_read_error            0x173ca00c
#define svc_s_acl_write_error           0x173ca00d
#define svc_s_dump_usage                0x173ca00e
#define msg_s_bad_id                    0x173ca00f
#define msg_s_no_memory                 0x173ca010
#define msg_s_no_default                0x173ca011
#define msg_s_not_found                 0x173ca012
#define msg_s_no_cat_open               0x173ca013
#define msg_s_no_cat_perm               0x173ca014
#define msg_s_no_catalog                0x173ca015
#define msg_s_ok_text                   0x173ca016

#define smallest_svc_message_id         0x00000000
#define biggest_svc_message_id          0x173ca016

#if	defined(_DCE_MSG_H)
extern dce_msg_table_t dce_svc_g_table[22];
#endif	/* defined(_DCE_MSG_H) */
#endif	/* !defined(_DCE_DCESVCMSG_) */
