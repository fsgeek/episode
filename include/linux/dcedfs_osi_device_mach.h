/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 */
/*
 *      Copyright (C) 1993 Transarc Corporation
 *      All rights reserved.
 */

/* $Header: /u0/rcs_trees/dce/rcs/file/osi/HPUX/osi_device_mach.h,v 1.1.26.1 1996/10/02 17:57:27 damon Exp $ */

/*
 * HPUX device I/O definitions
 */
#ifndef TRANSARC_OSI_DEVICE_MACH_H
#define TRANSARC_OSI_DEVICE_MACH_H

#ifdef _KERNEL
#define osi_inc_ru_oublock(n) (u.u_ru.ru_oublock += (n))
#define osi_inc_ru_inblock(n) (u.u_ru.ru_inblock += (n))
#endif

#ifdef _KERNEL
#define osi_strategy(bp) \
    ((*(bdevsw[osi_major(osi_bdev(bp))].d_strategy))(bp), 0)
#else
/*extern int us_strategy(struct buf *bp);*/
#define osi_strategy(bp) us_strategy(bp)
#endif /* _KERNEL */

#define osi_major(d) major(d)
#define osi_minor(d) minor(d)
#define osi_makedev(maj, min) makedev(maj, min)

// Copied over from BSD

#if !defined(DEV_BSHIFT)
#define DEV_BSHIFT (9) /* log2(DEV_BSIZE) */
#endif

#if !defined(DEV_BSIZE)
#define DEV_BSIZE (1 << DEV_BSHIFT)
#endif

/*
 * btodb() is messy and perhaps slow because `bytes' may be an off_t.  We
 * want to shift an unsigned type to avoid sign extension and we don't
 * want to widen `bytes' unnecessarily.  Assume that the result fits in
 * a daddr_t.
 */
#ifndef btodb
#define btodb(bytes) /* calculates (bytes / DEV_BSIZE) */       \
    (sizeof(bytes) > sizeof(long)                               \
         ? (daddr_t)((unsigned long long)(bytes) >> DEV_BSHIFT) \
         : (daddr_t)((unsigned long)(bytes) >> DEV_BSHIFT))
#endif

#endif /* TRANSARC_OSI_DEVICE_MACH_H */
