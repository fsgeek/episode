/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 */
/*
 *      Copyright (C) 1996, 1990 Transarc Corporation
 *      All rights reserved.
 */

#ifndef TRANSARC_LINUX_DCEDFS_OSI_BUF_MACH_H
#define TRANSARC_LINUX_DCEDFS_OSI_BUF_MACH_H

#include <sys/param.h>
// #include <sys/buf.h>
#include <dcedfs_stds.h>
#include <stdint.h>

#if !defined(DEV_BSHIFT)
#define DEV_BSHIFT 9 /* log2(DEV_BSIZE) */
#endif

#if !defined(DEV_BSIZE)
#define DEV_BSIZE 512
#endif // DEF_BSIZE

_Static_assert((1 << DEV_BSHIFT) == DEV_BSIZE, "BSHIFT/BSIZE mismatch");

// Linux does not have a sys/buf.h so for the moment I'm stealing the definition from FreeBSD and hacking it a bit

#if 0
// Definitions from BSD
#define B_AGE 0x00000001		  /* Move to age queue when I/O done. */
#define B_NEEDCOMMIT 0x00000002	  /* Append-write in progress. */
#define B_ASYNC 0x00000004		  /* Start I/O, do not wait. */
#define B_DIRECT 0x00000008		  /* direct I/O flag (pls free vmio) */
#define B_DEFERRED 0x00000010	  /* Skipped over for cleaning */
#define B_CACHE 0x00000020		  /* Bread found us in the cache. */
#define B_VALIDSUSPWRT 0x00000040 /* Valid write during suspension. */
#define B_DELWRI 0x00000080		  /* Delay I/O until buffer reused. */
#define B_CKHASH 0x00000100		  /* checksum hash calculated on read */
#define B_DONE 0x00000200		  /* I/O completed. */
#define B_EINTR 0x00000400		  /* I/O was interrupted */
#define B_NOREUSE 0x00000800	  /* Contents not reused once released. */
#define B_REUSE 0x00001000		  /* Contents reused, second chance. */
#define B_INVAL 0x00002000		  /* Does not contain valid info. */
#define B_BARRIER 0x00004000	  /* Write this and all preceding first. */
#define B_NOCACHE 0x00008000	  /* Do not cache block after use. */
#define B_MALLOC 0x00010000		  /* malloced b_data */
#define B_CLUSTEROK 0x00020000	  /* Pagein op, so swap() can count it. */
#define B_00040000 0x00040000	  /* Available flag. */
#define B_00080000 0x00080000	  /* Available flag. */
#define B_00100000 0x00100000	  /* Available flag. */
#define B_00200000 0x00200000	  /* Available flag. */
#define B_RELBUF 0x00400000		  /* Release VMIO buffer. */
#define B_FS_FLAG1 0x00800000	  /* Available flag for FS use. */
#define B_NOCOPY 0x01000000		  /* Don't copy-on-write this buf. */
#define B_INFREECNT 0x02000000	  /* buf is counted in numfreebufs */
#define B_PAGING 0x04000000		  /* volatile paging I/O -- bypass VMIO */
#define B_MANAGED 0x08000000	  /* Managed by FS. */
#define B_RAM 0x10000000		  /* Read ahead mark (flag) */
#define B_VMIO 0x20000000		  /* VMIO flag */
#define B_CLUSTER 0x40000000	  /* pagein op, so swap() can count it */
#define B_REMFREE 0x80000000	  /* Delayed bremfree */
#endif							  // 0

#if 0
// replaced with definitions below from a BSD copy of sys/buf.h

#define B_DONE 0x00000200  /* I/O completed. */
#define B_ASYNC 0x00000004 /* Start I/O, do not wait. */
#define B_BUSY 0x00020000
#define B_READ 0x00040000
#define B_WRITE 0x00080000
#define B_ERROR 0x00100000
#define B_CALL 0x00200000
#endif // 0
typedef char *caddr_t;
typedef unsigned short int uint16_t;
// typedef unsigned long long daddr_t; <-- already defined by linux
// typedef unsigned long long uint64_t; <-- already defined by linux
typedef unsigned int uint32_t;
typedef unsigned char uint8_t;
typedef unsigned long long b_xflags_t;
typedef void *vm_page_t;

struct lock
{
	int foo;
	int bar;
};

//
// The original buf struct that was being used seems to be close to the AIX
// version, which is documented here: https://www.ibm.com/support/knowledgecenter/ssw_aix_72/kerneltechref/buf.html
//
// The version I've been using below is actually from current FreeBSD sources, so it seems to have drifted
// more than AIX has from the original BSD 4.3 code base.
//
//
// Here's an ACTUAL copy of this file at some point (BSD 4.2?)
//
// http://pdp11.sytse.net/usr/include/sys/buf.h
//

//////////////////// BEGIN imported sys/buf.h contents

/*
 * Copyright (c) 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)buf.h	1.4 (2.11BSD GTE) 1996/9/13
 */

/*
 * The header for buffers in the buffer pool and otherwise used
 * to describe a block i/o request is given here.
 *
 * Each buffer in the pool is usually doubly linked into 2 lists:
 * hashed into a chain by <dev,blkno> so it can be located in the cache,
 * and (usually) on (one of several) queues.  These lists are circular and
 * doubly linked for easy removal.
 *
 * There are currently two queues for buffers:
 * 	one for buffers containing ``useful'' information (the cache)
 *	one for buffers containing ``non-useful'' information
 *		(and empty buffers, pushed onto the front)
 * These queues contain the buffers which are available for
 * reallocation, are kept in lru order.  When not on one of these queues,
 * the buffers are ``checked out'' to drivers which use the available list
 * pointers to keep track of them in their i/o active queues.
 */

/*
 * Bufhd structures used at the head of the hashed buffer queues.
 * We only need three words for these, so this abbreviated
 * definition saves some space.
 */
struct bufhd
{
	short b_flags;				 /* see defines below */
	struct buf *b_forw, *b_back; /* fwd/bkwd pointer in chain */
};

struct buf
{
	short b_flags;				   /* see defines below */
	struct buf *b_forw, *b_back;   /* hash chain (2 way street) */
	struct buf *av_forw, *av_back; /* position on free list if not BUSY */
#define b_actf av_forw			   /* alternate names for driver queue */
#define b_actl av_back			   /*    head - isn't history wonderful */
	u_short b_bcount;			   /* transfer count */
#define b_active b_bcount		   /* driver queue head: drive active */
	char b_error;				   /* returned after I/O */
	char b_xmem;				   /* high order core address */
	dev_t b_dev;				   /* major+minor device name */
	union
	{
		caddr_t b_addr; /* low order core address */
	} b_un;
	daddr_t b_blkno;				/* block # on device */
	u_short b_resid;				/* words not transferred after error */
#define b_cylin b_resid				/* disksort */
#define b_errcnt b_resid			/* while i/o in progress: # retries */
	void *b_vp;						// ADDED: vnode pointer
	void (*b_iodone)(struct buf *); // ADDED: iodone function
	void *b_pfcent;					// ADDED: b_work/b_pfcent pointer
};

/*
 * We never use BQ_LOCKED or BQ_EMPTY, but if you want the 4.X block I/O
 * code to drop in, you have to have BQ_AGE and BQ_LRU *after* the first
 * queue, and it only costs 6 bytes of data space.
 */
#define BQUEUES 3	/* number of free buffer queues */
#define BQ_LOCKED 0 /* super-blocks &c */
#define BQ_LRU 1	/* lru, useful buffers */
#define BQ_AGE 2	/* rubbish */
#define BQ_EMPTY 3	/* buffer headers with no memory */

/* Flags to low-level allocation routines. */
#define B_CLRBUF 0x01 /* Request allocated buffer be cleared. */
#define B_SYNC 0x02	  /* Do all allocations synchronously. */

#define bawrite(bp)               \
	{                             \
		(bp)->b_flags |= B_ASYNC; \
		bwrite(bp);               \
	}

#define bfree(bp) (bp)->b_bcount = 0

#define bftopaddr(bp) ((u_int)(bp)->b_un.b_addr >> 6 | (bp)->b_xmem << 10)

#if defined(KERNEL) && !defined(SUPERVISOR)

#define BUFHSZ 16 /* must be power of 2 */
#define BUFHASH(dev, blkno) ((struct buf *)&bufhash[((long)(dev) + blkno) & ((long)(BUFHSZ - 1))])
extern int nbuf;			   /* number of buffer headers */
extern struct bufhd bufhash[]; /* heads of hash lists */
extern struct buf bfreelist[]; /* heads of available lists */
struct buf *balloc();
struct buf *getblk();
struct buf *geteblk();
struct buf *getnewbuf();
struct buf *bread();
struct buf *breada();
#endif

/*
 * These flags are kept in b_flags.
 */
#define B_WRITE 0x00000	   /* non-read pseudo-flag */
#define B_READ 0x00001	   /* read when I/O occurs */
#define B_DONE 0x00002	   /* transaction finished */
#define B_ERROR 0x00004	   /* transaction aborted */
#define B_BUSY 0x00008	   /* not on av_forw/back list */
#define B_PHYS 0x00010	   /* physical IO */
#define B_MAP 0x00020	   /* alloc UNIBUS */
#define B_WANTED 0x00040   /* issue wakeup when BUSY goes off */
#define B_AGE 0x00080	   /* delayed write for correct aging */
#define B_ASYNC 0x00100	   /* don't wait for I/O completion */
#define B_DELWRI 0x00200   /* write at exit of avail list */
#define B_TAPE 0x00400	   /* this is a magtape (no bdwrite) */
#define B_INVAL 0x00800	   /* does not contain valid info */
#define B_BAD 0x01000	   /* bad block revectoring in progress */
#define B_LOCKED 0x02000   /* locked in core (not reusable) */
#define B_UBAREMAP 0x04000 /* addr UNIBUS virtual, not physical */
#define B_RAMREMAP 0x08000 /* remapped into ramdisk */

#define B_CALL 0x00200000 // ADDED

/*
 * Insq/Remq for the buffer hash lists.
 */
#define bremhash(bp)                         \
	{                                        \
		(bp)->b_back->b_forw = (bp)->b_forw; \
		(bp)->b_forw->b_back = (bp)->b_back; \
	}

#define binshash(bp, dp)             \
	{                                \
		(bp)->b_forw = (dp)->b_forw; \
		(bp)->b_back = (dp);         \
		(dp)->b_forw->b_back = (bp); \
		(dp)->b_forw = (bp);         \
	}

/*
 * Insq/Remq for the buffer free lists.
 */
#define bremfree(bp)                            \
	{                                           \
		(bp)->av_back->av_forw = (bp)->av_forw; \
		(bp)->av_forw->av_back = (bp)->av_back; \
	}

#define binsheadfree(bp, dp)           \
	{                                  \
		(dp)->av_forw->av_back = (bp); \
		(bp)->av_forw = (dp)->av_forw; \
		(dp)->av_forw = (bp);          \
		(bp)->av_back = (dp);          \
	}

#define binstailfree(bp, dp)           \
	{                                  \
		(dp)->av_back->av_forw = (bp); \
		(bp)->av_back = (dp)->av_back; \
		(dp)->av_back = (bp);          \
		(bp)->av_forw = (dp);          \
	}

/*
 * Take a buffer off the free list it's on and
 * mark it as being use (B_BUSY) by a device.
 */
#define notavail(bp)               \
	{                              \
		register int x = splbio(); \
		bremfree(bp);              \
		(bp)->b_flags |= B_BUSY;   \
		splx(x);                   \
	}

#define iodone biodone
#define iowait biowait

//////////////////// END imported sys/buf.h contents

#if 0
struct buf
{
	struct bufobj *b_bufobj;
	dev_t b_dev;
	long b_bcount;
	void *b_caller1;
	caddr_t b_data;
	int b_error;
	uint16_t b_iocmd;	/* BIO_* bio_cmd from bio.h */
	uint16_t b_ioflags; /* BIO_* bio_flags from bio.h */
	off_t b_iooffset;
	long b_resid;
	void (*b_iodone)(struct buf *);
	void (*b_ckhashcalc)(struct buf *);
	uint64_t b_ckhash; /* B_CKHASH requested check-hash */
	daddr_t b_blkno;   /* Underlying physical block number. */
	off_t b_offset;	   /* Offset into file. */
	// TAILQ_ENTRY(buf)	b_bobufs;			   /* (V) Buffer's associated vnode. */
	uint32_t b_vflags;	   /* (V) BV_* flags */
	uint8_t b_qindex;	   /* (Q) buffer queue index */
	uint8_t b_domain;	   /* (Q) buf domain this resides in */
	uint16_t b_subqueue;   /* (Q) per-cpu q if any */
	uint32_t b_flags;	   /* B_* flags. */
	b_xflags_t b_xflags;   /* extra flags */
	struct lock b_lock;	   /* Buffer lock */
	long b_bufsize;		   /* Allocated buffer size. */
	int b_runningbufspace; /* when I/O is running, pipelining */
	int b_kvasize;		   /* size of kva for buffer */
	int b_dirtyoff;		   /* Offset in buffer of dirty region. */
	int b_dirtyend;		   /* Offset of end of dirty region. */
	caddr_t b_kvabase;	   /* base kva for buffer */
	daddr_t b_lblkno;	   /* Logical block number. */
	struct vnode *b_vp;	   /* Device vnode. */
	struct ucred *b_rcred; /* Read credentials reference. */
	struct ucred *b_wcred; /* Write credentials reference. */
	union
	{
		// TAILQ_ENTRY(buf) b_freelist; /* (Q) */
		struct
		{
			void (*b_pgiodone)(void *, vm_page_t *, int, int);
			int b_pgbefore;
			int b_pgafter;
		};
	};
#if 0
	union cluster_info
	{
		TAILQ_HEAD(cluster_list_head, buf) cluster_head;
		TAILQ_ENTRY(buf)
		cluster_entry;
	} b_cluster;
#endif // 0
	int b_npages;
	// struct workhead b_dep; /* (D) List of filesystem dependencies. */
	void *b_fsprivate1;
	void *b_fsprivate2;
	void *b_fsprivate3;

#if defined(FULL_BUF_TRACKING)
#define BUF_TRACKING_SIZE 32
#define BUF_TRACKING_ENTRY(x) ((x) & (BUF_TRACKING_SIZE - 1))
	const char *b_io_tracking[BUF_TRACKING_SIZE];
	uint32_t b_io_tcnt;
#elif defined(BUF_TRACKING)
	const char *b_io_tracking;
#endif
	struct vm_page *b_pages[];
};
#endif // 0

/*
 * Conversion between bytes, FS blocks and disk blocks
 *
 * XXX Name space pollution
 */
#define osi_btoab(d, b) ((b) >> (d)->logBlkSize)
#define osi_abtob(d, b) ((b) << (d)->logBlkSize)
#define dbtoab(d, b) ((b) >> ((d)->logBlkSize - DEV_BSHIFT))
#define abtodb(d, b) ((b) << ((d)->logBlkSize - DEV_BSHIFT))
#define dbroundup(b) roundup(b, DEV_BSIZE)
#define dbround(b) (b & ~(DEV_BSIZE - 1))

#if !defined(btodb)
#define btodb(size) ((size) & (DEV_BSIZE - 1))
#endif // btodb

#define b_work b_pfcent /* use as driver work area */

#ifdef KERNEL
/* special macro that provides uniform access to bufsize */
#define b_Bufsize(b) ((b)->b_bufsize)
#endif

/* special macro that allows b_work to be accessed as an lvalue */
#define B_Work(bp) (*(struct buf **)(&(bp)->b_work))

#ifndef KERNEL
#undef iodone /* get the test_vm procedure */
#endif

#ifdef KERNEL
#define b_Wanted(bp) (((bp)->b_flags & B_WANTED)            \
						  ? ((bp)->b_flags &= ~B_WANTED, 1) \
						  : 0)
#define b_Want(bp) ((bp)->b_flags |= B_WANTED)
#else /* KERNEL */
#define b_Wanted(bp) 1
#define b_Want(bp) 1
#endif /* KERNEL */

#define b_Call(bp) (((bp)->b_flags & B_CALL) ? ((*(bp)->b_iodone)(bp), 1) : 0)

#ifndef B_PFSTORE
#define B_PFSTORE 0x40000000
#endif

/*
 * Get and set device number.
 */
#define osi_bdev(bp) ((bp)->b_dev)
#define osi_set_bdev(bp, d) (osi_bdev(bp) = (d))

typedef int osi_iodone_t;

#define osi_iodone_return(n) return (n)

#define osi_set_iodone(bp, func) \
	((bp)->b_iodone = (func), (bp)->b_flags |= B_CALL)

#ifdef KERNEL
#define osi_bio_wakeup(bp, cv) osi_Wakeup((opaque)(bp))
#define osi_bio_init(bp)

#define osi_bio_cleanup(bp, cv)         \
	MACRO_BEGIN(bp)->b_flags |= B_DONE; \
	if ((bp)->b_flags & B_ASYNC)        \
		osi_ReleaseBuf(bp);             \
	MACRO_END

#define osi_biodone(bp, cv)         \
	MACRO_BEGIN                     \
	if ((bp)->b_flags & B_WANTED)   \
	{                               \
		(bp)->b_flags &= ~B_WANTED; \
		osi_bio_wakeup(bp, cv);     \
	}                               \
	MACRO_END

#define osi_bio_wait(bp, cv, mutex) \
	MACRO_BEGIN                     \
	if (!((bp)->b_flags & B_DONE))  \
	{                               \
		(bp)->b_flags |= B_WANTED;  \
		osi_Sleep((opaque)(bp));    \
	}                               \
	MACRO_END

#endif /* KERNEL */

#endif /* TRANSARC_LINUX_DCEDFS_OSI_BUF_MACH_H */
