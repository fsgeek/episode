
/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994, 1996 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 *
 */

//
// (C) Copyright 2020 Tony Mason
//
//

//
// The purpose of this header file is to include the platform specific file.
//

#if !defined(__DCEDFS_OSI_LOCK_MACH_H__)
#define __DCEDFS_OSI_LOCK_MACH_H__

// These are common
#include <dcedfs_stds.h>
#ifndef _KERNEL
#include <dirent.h>
#endif
#include <sys/types.h>

// For now, there's no machine specific logic here

#endif // __DCEDFS_OSI_LOCK_MACH_H__
