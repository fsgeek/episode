/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 */
/*
 *      Copyright (C) 1996, 1990 Transarc Corporation
 *      All rights reserved.
 */

#ifndef TRANSARC_OSI_BUF_MACH_H
#define TRANSARC_OSI_BUF_MACH_H

//
// TODO: add other platforms, make this one conditional
//
#if defined(__linux__)
#include <linux/dcedfs_osi_buf_mach.h>
#else
#error "Unsupported platform"
#endif

#endif /* TRANSARC_OSI_BUF_MACH_H */
