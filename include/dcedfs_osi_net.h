/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 */
/* Copyright (C) 1992 Transarc Corporation - All rights reserved */

#ifndef TRANSARC_OSI_NET_H
#define TRANSARC_OSI_NET_H

#include <dcedfs_param.h>

#ifdef HAS_OSI_NET_MACH_H
#include <dcedfs_osi_net_mach.h>
#endif

#endif
