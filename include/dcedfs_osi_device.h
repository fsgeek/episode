/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 */

/*
 *      Copyright (C) 1990-1992 Transarc Corporation
 *      All rights reserved.
 */

#ifndef TRANSARC_OSI_DEVICE_H
#define TRANSARC_OSI_DEVICE_H
/*
 * definitions relating to device interfaces and data structures
 */
#include <dcedfs_param.h>

/*
 * Don't keep resource usage statistics at user level.
 */
#ifndef _KERNEL
#define osi_inc_ru_oublock(n)
#define osi_inc_ru_inblock(n)
#endif

#if defined(__linux__)
#include <linux/dcedfs_osi_device_mach.h>
#include <sys/sysmacros.h>
#else
#error "Unknown platform"
#endif

#endif /* TRANSARC_OSI_DEVICE_H */
