
/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994, 1996 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 *
 */

//
// (C) Copyright 2020 Tony Mason
//
//

//
// The purpose of this header file is to define only the things needed to build Episode
//

#if !defined(__EPISODE_H__)
#define __EPISODE_H__

#include <dce_error.h>
#include <dcedfs_stds.h>
#include <epi_thread.h>

#endif // __EPISODE_H__
