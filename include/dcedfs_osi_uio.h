/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 */

/* Copyright (C) 1992 Transarc Corporation - All rights reserved */

#ifndef TRANSARC_OSI_UIO_H
#define TRANSARC_OSI_UIO_H

#include <dcedfs_param.h>
#include <dcedfs_stds.h>

#if defined(__linux__)
#include <linux/osi_uio_mach.h>
#else
#error "Unknown platform"
#endif

struct uio;

/*
 * This file contains standard macros that are used to isolate OS
 * system dependencies for uio services
 */
extern void osi_uiomove_unit(
    u_long prevNum,
    u_long n,
    struct uio *uio,
    caddr_t *baseP,
    u_long *lenP);

#if !defined(KERNEL)
extern int osi_user_uiomove(
    caddr_t cp,
    u_long n,
    enum uio_rw rw,
    struct uio *uio);
#endif

extern int osi_uio_copy(
    struct uio *inuiop,
    struct uio *outuiop,
    struct iovec *outvecp);

extern int osi_uio_trim(struct uio *uiop, long size);
extern int osi_uio_skip(struct uio *uiop, long size);

#endif /* TRANSARC_OSI_UIO_H */
