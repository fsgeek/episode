/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 */

#ifndef _ICL_USERINT_H__ENV_
#define _ICL_USERINT_H__ENV_

// #include <dcedfs_cmd.h>
// #include <dcedfs_cmdAux.h>

/* define common argument indices */
#define COMMON_ARG_CDSENTRY_INDEX 4

/* indices for dump command */
#define DUMP_ARG_SET_INDEX 0
#define DUMP_ARG_FOLLOW_INDEX 1
#define DUMP_ARG_FILE_INDEX 2
#define DUMP_ARG_SLEEP_INDEX 3
#define DUMP_ARG_LOG_INDEX 5
#define DUMP_ARG_RAW_INDEX 6

/* define argument indices for Showlog */
#define SHOWL_ARG_SET_INDEX 0
#define SHOWL_ARG_LOG_INDEX 1
#define SHOWL_ARG_LONG_INDEX 2

/* define argument indices for Showset */
#define SHOWS_ARG_SET_INDEX 0

/* define argument indices for Clear */
#define CLR_ARG_SET_INDEX 0
#define CLR_ARG_LOG_INDEX 1

/* define argument indices for set */
#define SET_ARG_SET_INDEX 0
#define SET_ARG_ACT_INDEX 1
#define SET_ARG_DEACT_INDEX 2
#define SET_ARG_FREE_INDEX 3

/* define argument indices for resize */
#define SSIZE_ARG_LOG_INDEX 0
#define SSIZE_ARG_SIZE_INDEX 1

#endif /* _ICL_USERINT_H__ENV_ */
