/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 */
/*
 * Copyright (c) 1995 Transarc Corporation
 * All Rights Reserved
 */
/*
 * HISTORY
 * $Log: compat.h,v $
 * Revision 1.1.70.1  1996/10/02  17:54:12  damon
 * 	Newest DFS from Transarc
 * 	[1996/10/01  18:42:02  damon]
 *
 * $EndLog$
*/
#ifndef __COMPAT_H_INCL_ENV_
#define __COMPAT_H_INCL_ENV_ 1

#include <dcedfs_param.h>
#include <dcedfs_stds.h>
// #include <dcedfs_common_data.h>
#include <dcedfs_sysincludes.h>
#include <dcedfs_osi_net.h>

// #include <dce/rpc.h>
#include <dcedfs_osi_net_mach.h>

/*
 * from compat_osi.c:
 */
extern void afsos_panic _TAKES((char *a));

/* from compat_err.c: */
extern const char *dfs_dceErrTxt(unsigned long st);

extern void dfs_copyDceErrTxt _TAKES((
	unsigned long st,
	char *txtbuf,
	int bufsize));

#define NCSCOMPAT_ERR_BUF_SIZE 256

#endif /* __COMPAT_H_INCL_ENV_ */
