//
// (C) Copyright 2020 Tony Mason
//

void CalcCRC32Std(
    const char *buffer,
    int bufSize,
    unsigned long *crc);
