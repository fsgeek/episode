//
// (C) Copyright 2020 Tony Mason
//

#if !defined(__EPISODE_UIO_H__)
#define __EPISODE_UIO_H__

// This is based on the BSD version; there is no linux equivalent as far as I can tell.

#if defined(BSD_UIO)

typedef struct uio epi_uio_t;

#define epi_uio_iov uio_iov
#define epi_uio_iovcnt uio_iovcnt
#define epi_uio_io
#define epi_uio_offset uio_offset
#define epi_uio_resid uio_resid
#define epi_uio_segflg uio_segflg
#define epi_uio_rw uio_rw
#define epi_uio_td uio_td

#else

#include <sys/uio.h> // linux defines struct iovec here

enum epi_uio_rw
{
    EPI_UIO_READ = 17,
    EPI_UIO_WRITE
};

/* Segment flag values. */
enum epi_uio_seg
{
    EPI_UIO_USERSPACE = 33, /* from user data space */
    EPI_UIO_SYSSPACE,       /* from system space */
    EPIUIO_NOCOPY           /* don't copy, already in object */
};

typedef struct _epi_uio
{
    struct iovec *epi_uio_iov;       /*	scatter/gather list */
    int epi_uio_iovcnt;              /*	length of scatter/gather list */
    off_t epi_uio_offset;            /*	offset in target object	*/
    ssize_t epi_uio_resid;           /*	remaining bytes	to copy	*/
    enum epi_uio_seg epi_uio_segflg; /*	address	space */
    enum epi_uio_rw epi_uio_rw;      /*	operation */
    struct thread *uio_td;           /*	owner */
} epi_uio_t;
#endif               // BSD

#endif // __EPISODE_UIO_H__
