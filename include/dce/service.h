//
// (C) Copyright 2021 Tony Mason, All rights reserved
//
// Note: this is derived from OSF copyright code.

//
// This is a "hacked together" version of service.h since I still don't have a working IDL
// compiler.  I'm only trying to preserve the types for now.

#pragma once

#if !defined(__DCE_SERVICE_H__)
#define __DCE_SERVICE_H__

typedef char *dce_svc_string_t;
typedef struct dce_svc_stringarray_s_t
{
    long tab_size;
    dce_svc_string_t *table[1]; // variable sized array of tab_size
} dce_svc_stringarray_t;
typedef struct dce_svc_subcomp_s_t
{
    dce_svc_string_t sc_name;
    dce_svc_string_t sc_descr;
    unsigned long sc_descr_msgid;
    unsigned long sc_level;
} dce_svc_subcomp_t;

typedef struct dce_svc_subcomparray_s_t
{
    long tab_size;
    dce_svc_subcomp_t table[1]; // variable sized array of tab_size
} dce_svc_subcomparray_t;

const long dce_svc_stats_unknown = 0;

typedef struct dce_svc_stats_unknown_s_t
{
    long data_size;
    byte data[1]; // variable sized array of data_size
} dce_svc_stats_unknown_t;

/*
 **  A union of all possible statistics.
 */
typedef union
{
    dce_svc_stats_unknown_t stats_unknown;
} dce_svc_stats_t;

#endif // __DCE_SERVICE_H__
