/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1996 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE in the
 * src directory for the full copyright text.
 */
/*
 * HISTORY
 * $Log: nbase.h,v $
 * Revision 1.1.4.2  1996/02/18  23:46:05  marty
 * 	Update OSF copyright years
 * 	[1996/02/18  22:44:51  marty]
 *
 * Revision 1.1.4.1  1995/12/07  22:36:38  root
 * 	Submit OSF/DCE 1.2.1
 * 	[1995/12/07  21:18:17  root]
 *
 * Revision 1.1.2.3  1993/01/03  22:15:02  bbelch
 * 	Embedding copyright notice
 * 	[1993/01/03  19:48:10  bbelch]
 *
 * Revision 1.1.2.2  1992/12/23  19:15:55  zeliff
 * 	Embedding copyright notice
 * 	[1992/12/23  01:11:20  zeliff]
 *
 * Revision 1.1  1992/01/19  02:59:29  devrcs
 * 	Initial revision
 *
 * $EndLog$
 */
/*
*/
#ifndef nbase__included
#define nbase__included
#include <idl_base.h>
#include <idlbase.h>
typedef ndr_$short_int binteger;
typedef ndr_$short_int pinteger;
typedef ndr_$long_int linteger;
typedef struct status_$t status_$t;
struct status_$t
{
    ndr_$long_int all;
};
#define status_$ok 0
typedef struct uuid_$t uuid_$t;
struct uuid_$t
{
    ndr_$ulong_int time_high;
    ndr_$ushort_int time_low;
    ndr_$ushort_int reserved;
    ndr_$byte family;
    ndr_$byte host[7];
};
#ifdef __STDC__
handle_t uuid_$t_bind(uuid_$t h);
void uuid_$t_unbind(uuid_$t uh, handle_t h);
#else
handle_t uuid_$t_bind();
void uuid_$t_unbind();
#endif
#define socket_$unspec_port 0
#define socket_$unspec ((ndr_$ushort_int)0x0)
#define socket_$unix ((ndr_$ushort_int)0x1)
#define socket_$internet ((ndr_$ushort_int)0x2)
#define socket_$implink ((ndr_$ushort_int)0x3)
#define socket_$pup ((ndr_$ushort_int)0x4)
#define socket_$chaos ((ndr_$ushort_int)0x5)
#define socket_$ns ((ndr_$ushort_int)0x6)
#define socket_$nbs ((ndr_$ushort_int)0x7)
#define socket_$ecma ((ndr_$ushort_int)0x8)
#define socket_$datakit ((ndr_$ushort_int)0x9)
#define socket_$ccitt ((ndr_$ushort_int)0xa)
#define socket_$sna ((ndr_$ushort_int)0xb)
#define socket_$unspec2 ((ndr_$ushort_int)0xc)
#define socket_$dds ((ndr_$ushort_int)0xd)
typedef ndr_$ushort_int socket_$addr_family_t;
#define socket_$num_families 32
#define socket_$sizeof_family 2
#define socket_$sizeof_data 14
#define socket_$sizeof_ndata 12
#define socket_$sizeof_hdata 12
typedef struct socket_$addr_t socket_$addr_t;
struct socket_$addr_t
{
    socket_$addr_family_t family;
    ndr_$byte data[14];
};
typedef struct socket_$net_addr_t socket_$net_addr_t;
struct socket_$net_addr_t
{
    socket_$addr_family_t family;
    ndr_$byte data[12];
};
typedef struct socket_$host_id_t socket_$host_id_t;
struct socket_$host_id_t
{
    socket_$addr_family_t family;
    ndr_$byte data[12];
};

//
// I'm adding this from nbase.idl, since I think THOSE are the definitions I really need

/*
 * Type definitions for specific size integers.
 */
typedef unsigned char unsigned8;   /* positive 8 bit integer */
typedef unsigned short unsigned16; /* positive 16 bit integer */
typedef unsigned long unsigned32;  /* positive 32 bit integer */

typedef char signed8;   /* signed 8 bit integer */
typedef short signed16; /* signed 16 bit integer */
typedef long signed32;  /* signed 32 bit integer */

/*
 * Type definition for 32-bit wide booleans.
 */
typedef unsigned32 boolean32;

/*
 * Canonical types for expressing procedure return status.
 */
typedef unsigned long error_status_t;
const long error_status_ok = 0;

/*
 * Universal Unique Identifier (UUID) types.
 */
typedef struct
{
    unsigned32 time_low;
    unsigned16 time_mid;
    unsigned16 time_hi_and_version;
    unsigned8 clock_seq_hi_and_reserved;
    unsigned8 clock_seq_low;
    unsigned8 node[6];
} uuid_t, *uuid_p_t;

/*
 * Old UUID type.
 */

typedef struct
{
    unsigned long time_high;
    unsigned short time_low;
    unsigned short reserved;
    unsigned8 family;
    unsigned8 host[7];
} uuid_old_t;

#if 0
/*
 * Protocol Tower.  The network representation of network addressing information
 * (e.g., RPC bindings).
 */
typedef struct
{
    unsigned32 tower_length;
    [size_is(tower_length)] byte tower_octet_string[];
} twr_t, *twr_p_t;
#endif // 0

/*
 * NDR format flag type definition and values.
 */
const long ndr_c_int_big_endian = 0;
const long ndr_c_int_little_endian = 1;
const long ndr_c_float_ieee = 0;
const long ndr_c_float_vax = 1;
const long ndr_c_float_cray = 2;
const long ndr_c_float_ibm = 3;
const long ndr_c_char_ascii = 0;
const long ndr_c_char_ebcdic = 1;

typedef struct
{
    unsigned8 int_rep;
    unsigned8 char_rep;
    unsigned8 float_rep;
    unsigned8 reserved;
} ndr_format_t, *ndr_format_p_t;

/*
 * Network representation of a NIDL context handle.
 */
typedef struct ndr_context_handle
{
    unsigned32 context_handle_attributes;
    uuid_t context_handle_uuid;
} ndr_context_handle;

/*
 * International character types.
 */
typedef unsigned8 ISO_LATIN_1;

typedef struct
{
    unsigned8 row;
    unsigned8 column;
} ISO_MULTI_LINGUAL;

typedef struct
{
    unsigned8 group;
    unsigned8 plane;
    unsigned8 row;
    unsigned8 column;
} ISO_UCS;

/*
 * Authentication protocol IDs.  These are architectural values that
 * are carried in RPC protocol messages.
 */
const long dce_c_rpc_authn_protocol_none = 0;  /* No authentication */
const long dce_c_rpc_authn_protocol_krb5 = 1;  /* Kerberos v5 authentication */
const long dce_c_rpc_authn_protocol_dummy = 2; /* Non-crypto authentication */
const long dce_c_rpc_authn_protocol_dssa = 3;  /* DEC DSSA authentication */
typedef unsigned8 dce_rpc_authn_protocol_id_t;
#endif
