//
// Copyright 2021 Tony Mason, all rights reserved
//
//

// This stubs out exception handling for now.

#include <assert.h>

void exc_raise(void *exception)
{
    // not supported
    (void)exception;

    assert(0);
}

void exc_push_ctx(void *arg)
{
    // not supported
    (void)arg;

    assert(0);
}

void exc_pop_ctx(void *arg)
{
    // not supported
    (void)arg;

    assert(0);
}
