/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1996 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE in the
 * src directory for the full copyright text.
 */

/*  init_et.c V=2 10/23/91 //littl/prgy/krb5/comerr
**
** Copyright (c) Hewlett-Packard Company 1991
** Unpublished work. All Rights Reserved.
**
*/
/*
 * $Header: /u0/rcs_trees/dce/rcs/security/krb5/comerr/init_et.c,v 1.1.4.2 1996/02/18 00:08:33 marty Exp $
 * $Source: /u0/rcs_trees/dce/rcs/security/krb5/comerr/init_et.c,v $
 * $Locker:  $
 *
 * Copyright 1986, 1987, 1988 by MIT Information Systems and
 *      the MIT Student Information Processing Board.
 *
 * For copyright info, see mit-sipb-cr.h.
 */

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include "error_table.h"

#ifndef __STDC__
#define const
#endif

#ifndef lint
static const char rcsid_init_et_c[] =
    "$Header: /u0/rcs_trees/dce/rcs/security/krb5/comerr/init_et.c,v 1.1.4.2 1996/02/18 00:08:33 marty Exp $";
#endif

struct foobar
{
    struct et_list etl;
    struct error_table et;
};

extern struct et_list *_et_list;

// TODO: move to header
int init_error_table(const unsigned char *const *msgs, int base, int count);

int init_error_table(const unsigned char *const *msgs, int base, int count)
{
    struct foobar *new_et;

    (void)rcsid_init_et_c;

    if (!base || !count || !msgs)
        return 0;

    new_et = (struct foobar *)malloc(sizeof(struct foobar));
    if (!new_et)
        return errno; /* oops */
    new_et->etl.table = &new_et->et;
    new_et->et.msgs = msgs;
    new_et->et.base = base;
    new_et->et.n_msgs = count;

    new_et->etl.next = _et_list;
    _et_list = &new_et->etl;
    return 0;
}
