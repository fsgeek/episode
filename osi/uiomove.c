/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 */

#include <dcedfs_param.h>
#include <dcedfs_osi.h>
#include <dcedfs_osi_uio.h>
#include <sys/types.h>
#include <dcedfs_stds.h>

/* Define two macros to do the random uiomove book keeping. */
/* The first, FIND_NEXT_CHUNK is a macro that must be used in a block as it
 * expands into a sequence of statements.  The n and uio parameters are
 * expressions giving the total requested transfer size and the address of the
 * uio structure, repectively.  The last two parameters must be LValue
 * expressions which are set to the base and length, repectively, of the next
 * largest contiguous area of memory to be used for a transfer. */

#define FIND_NEXT_CHUNK(n, uio, base, len)              \
	while (base = (uio)->osi_uio_iov->iov_base,         \
		   len = (uio)->osi_uio_iov->iov_len,           \
		   ((len <= 0) && ((uio)->osi_uio_iovcnt > 1))) \
		(uio)->osi_uio_iov++, (uio)->osi_uio_iovcnt--;  \
	if (len > (n))                                      \
		len = (n);                                      \
	if (len > (uio)->osi_uio_resid)                     \
		len = (uio)->osi_uio_resid;

#define Update(uio, delta)                    \
	((uio)->osi_uio_iov->iov_base += (delta), \
	 (uio)->osi_uio_iov->iov_len -= (delta),  \
	 (uio)->osi_uio_offset += (delta),        \
	 (uio)->osi_uio_resid -= (delta))

#if !defined(KERNEL)

int osi_user_uiomove(
	IN caddr_t cp,
	IN u_long n,
	IN enum uio_rw rw,
	INOUT struct uio *uio)
{
	if ((uio->osi_uio_iovcnt <= 0) || (uio->osi_uio_resid <= 0))
		return 0;
	while ((n > 0) && (uio->osi_uio_resid > 0))
	{
		int code;
		caddr_t base;
		u_long num;

		FIND_NEXT_CHUNK(n, uio, base, num);
		if (num <= 0)
			return 0;

		/* do the transfer */
		code = 0;
		if (rw == UIO_READ)
		{
			if (uio->osi_uio_seg == OSI_UIOSYS)
			{
				bcopy(cp, base, num);
			}
			else if (uio->osi_uio_seg == OSI_UIOUSER)
			{
				code = osi_copyout(cp, base, num);
			}
			else
				panic("uiomove: bad osi_uio_seg");
		}
		else if (rw == UIO_WRITE)
		{
			if (uio->osi_uio_seg == OSI_UIOSYS)
			{
				bcopy(base, cp, num);
			}
			else if (uio->osi_uio_seg == OSI_UIOUSER)
			{
				code = osi_copyin(base, cp, num);
			}
			else
				panic("uiomove: bad osi_uio_seg");
		}
		else
			panic("uiomove: bad rw");
		if (code)
			return code;

		/* update data uio state */
		Update(uio, num);
		n -= num;
	}
	return 0;
}
#endif /* !defined(KERNEL) */

void osi_uiomove_unit(
	IN u_long prevNum,
	IN u_long n,
	INOUT struct uio *uio,
	OUT caddr_t *baseP,
	OUT u_long *lenP)
{
	if (prevNum > 0)
		Update(uio, prevNum);

	FIND_NEXT_CHUNK(n, uio, *baseP, *lenP);
}
/*
 *
 * Routines that deal with copying/trimming multi vector uio structs.
 *
 */
#define AFS_MAXIOVCNT 16

/*
 * routine to make copy of uio structure in ainuio, using aoutvec for space
 */
int osi_uio_copy(
	struct uio *inuiop,
	struct uio *outuiop,
	struct iovec *outvecp)
{
	register int i;
	register struct iovec *iovecp;

	if (inuiop->osi_uio_iovcnt > AFS_MAXIOVCNT)
		return EINVAL;
	*outuiop = *inuiop;
	iovecp = inuiop->osi_uio_iov;
	outuiop->osi_uio_iov = outvecp;
	for (i = 0; i < inuiop->osi_uio_iovcnt; i++)
	{
		*outvecp = *iovecp;
		iovecp++;
		outvecp++;
	}
	return 0;
}

/*
 * trim the uio structure to the specified size
 */
int osi_uio_trim(struct uio *uiop, long size)
{
	register int i;
	register struct iovec *iovecp;

	uiop->osi_uio_resid = size;
	iovecp = uiop->osi_uio_iov;
	/*
     * It isn't clear that multiple iovecs work ok (hasn't been tested!)
     */
	for (i = 0;; i++, iovecp++)
	{
		if (i >= uiop->osi_uio_iovcnt || size <= 0)
		{
			uiop->osi_uio_iovcnt = i; /* we're done */
			break;
		}
		if (iovecp->iov_len <= size)
			/*
	     * entire iovec is included
	     */
			size -= iovecp->iov_len; /* this many fewer bytes */
		else
		{
			/*
	     * this is the last one
	     */
			iovecp->iov_len = size;
			uiop->osi_uio_iovcnt = i + 1;
			break;
		}
	}
	return 0;
}

/*
 * skip size bytes in the current uio structure
 */
int osi_uio_skip(struct uio *uiop, long size)
{
	register struct iovec *iovecp; /* pointer to current iovec */
	register int cnt;

	/*
     * It isn't guaranteed that multiple iovecs work ok (hasn't been tested!)
     */
	while (size > 0 && uiop->osi_uio_resid)
	{
		iovecp = uiop->osi_uio_iov;
		cnt = iovecp->iov_len;
		if (cnt == 0)
		{
			uiop->osi_uio_iov++;
			uiop->osi_uio_iovcnt--;
			continue;
		}
		if (cnt > size)
			cnt = size;
		iovecp->iov_base += cnt;
		iovecp->iov_len -= cnt;
		uiop->uio_resid -= cnt;
		uiop->uio_offset += cnt;
		size -= cnt;
	}
	return 0;
}
