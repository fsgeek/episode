/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 */
#include <dcedfs_param.h>
#include <dcedfs_stds.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <memory.h>

IMPORT void icl_DumpUser _TAKES((int signal));
/******************************************************
 * Code to handle dumps in user space that are started
 * by a signal.
 */

/* define signal mask */
static sigset_t sigmask;

/* threaded routine to handle signal */
static void *SigHandle(void *arg)
{
	int signal_num = 0;

	(void)arg; // not used

	sigemptyset(&sigmask);
	if (sigaddset(&sigmask, SIGUSR1) < 0)
	{
		(void)fprintf(stderr, "unable to catch signal, errno = %d\n", errno);
		pthread_exit(0);
	}

	while (1)
	{
		if (0 != sigwait(&sigmask, &signal_num))
		{
			(void)fprintf(stderr, "icl_signal: sigwait() failed, errno = %d\n",
						  errno);
			break;
		}
		if (signal_num == SIGUSR1)
			icl_DumpUser(signal_num);
	}
	pthread_exit(0);

	return NULL;
}

// TODO: move declaration into public header file
int icl_StartCatcher(int waitInKernel);

/* Function to set up thread to handle signals */
int icl_StartCatcher(int waitInKernel) /* are we waiting in the kernel? */
{
	static int Initted = 0;
	static pthread_t sig_thread_id;
	int code;

	if (waitInKernel)
	{
#ifdef AFS_SIGACTION_ENV
		struct sigaction vec;
#else
		struct sigvec vec;
#endif

		/* If we're waiting in the kernel, the pthread isn't going to help us */
		memset(&vec, 0, sizeof(vec));
#ifdef AFS_SIGACTION_ENV
		vec.sa_handler = icl_DumpUser;
		code = sigaction(SIGUSR1, &vec, NULL);
#else  /* SIGACTION */
		vec.sv_handler = icl_DumpUser;
		code = sigvec(SIGUSR1, &vec, NULL);
#endif /* SIGACTION */
		return code;
	}

	if (Initted)
		return 0;
	Initted = 1;

	/* set up signal handler to dump the log */
	return (pthread_create(&sig_thread_id, NULL,
						   SigHandle,
						   NULL));
}
