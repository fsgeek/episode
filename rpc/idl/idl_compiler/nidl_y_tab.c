/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.5.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 47 "nidl.y"

/*
**  @OSF_COPYRIGHT@
**
**  Copyright (c) 1989 by
**      Hewlett-Packard Company, Palo Alto, Ca. &
**  Digital Equipment Corporation, Maynard, Mass.
**  All Rights Reserved.  Unpublished rights reserved
**  under the copyright laws of the United States.
**
**  The software contained on this media is proprietary
**  to and embodies the confidential technology of
**  Digital Equipment Corporation.  Possession, use,
**  duplication or dissemination of the software and
**  media is authorized only pursuant to a valid written
**  license from Digital Equipment Corporation.
**
**  RESTRICTED RIGHTS LEGEND   Use, duplication, or
**  disclosure by the U.S. Government is subject to
**  restrictions as set forth in Subparagraph (c)(1)(ii)
**  of DFARS 252.227-7013, or in FAR 52.227-19, as
**  applicable.
**
**
**  NAME:
**
**      IDL.Y 
**
**  FACILITY:
**
**      Interface Definition Language (IDL) Compiler
**
**  ABSTRACT:
**
**      This module defines the main IDL grammar accepted
**      by the IDL compiler.
**
**  VERSION: DCE 1.0
**
*/

#ifdef vms
#  include <types.h>
#else
#  include <sys/types.h>
#endif

#include <nidl.h>
#include <nametbl.h>
#include <errors.h>
#include <ast.h>
#include <astp.h>
#include <frontend.h>
#include <command.h>

extern boolean search_attributes_table ;

int yyparse (
#ifdef PROTO
    void
#endif
);

int yylex (
#ifdef PROTO
    void
#endif
);

/*
**  Local cells used for inter-production communication
*/
static ASTP_attr_k_t       ASTP_bound_type;    /* Array bound attribute */



#line 147 "nidl_y_tab.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_NIDL_Y_TAB_H_INCLUDED
# define YY_YY_NIDL_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    ALIGN_KW = 258,
    BYTE_KW = 259,
    CHAR_KW = 260,
    CONST_KW = 261,
    DEFAULT_KW = 262,
    ENUM_KW = 263,
    EXCEPTIONS_KW = 264,
    FLOAT_KW = 265,
    HYPER_KW = 266,
    INT_KW = 267,
    INTERFACE_KW = 268,
    IMPORT_KW = 269,
    LONG_KW = 270,
    PIPE_KW = 271,
    REF_KW = 272,
    SMALL_KW = 273,
    STRUCT_KW = 274,
    TYPEDEF_KW = 275,
    UNION_KW = 276,
    UNSIGNED_KW = 277,
    SHORT_KW = 278,
    VOID_KW = 279,
    DOUBLE_KW = 280,
    BOOLEAN_KW = 281,
    CASE_KW = 282,
    SWITCH_KW = 283,
    HANDLE_T_KW = 284,
    TRUE_KW = 285,
    FALSE_KW = 286,
    NULL_KW = 287,
    BROADCAST_KW = 288,
    COMM_STATUS_KW = 289,
    CONTEXT_HANDLE_KW = 290,
    FIRST_IS_KW = 291,
    HANDLE_KW = 292,
    IDEMPOTENT_KW = 293,
    IGNORE_KW = 294,
    IN_KW = 295,
    LAST_IS_KW = 296,
    LENGTH_IS_KW = 297,
    LOCAL_KW = 298,
    MAX_IS_KW = 299,
    MAYBE_KW = 300,
    CXX_LOOKUP_KW = 301,
    CLIENT_MEMORY_KW = 302,
    MIN_IS_KW = 303,
    MUTABLE_KW = 304,
    OUT_KW = 305,
    POINTER_DEFAULT_KW = 306,
    ENDPOINT_KW = 307,
    PTR_KW = 308,
    REFLECT_DELETIONS_KW = 309,
    REMOTE_KW = 310,
    SECURE_KW = 311,
    SHAPE_KW = 312,
    SIZE_IS_KW = 313,
    STRING_KW = 314,
    SWITCH_IS_KW = 315,
    SWITCH_TYPE_KW = 316,
    TRANSMIT_AS_KW = 317,
    UNIQUE_KW = 318,
    UUID_KW = 319,
    VERSION_KW = 320,
    V1_ARRAY_KW = 321,
    V1_STRING_KW = 322,
    V1_ENUM_KW = 323,
    V1_STRUCT_KW = 324,
    STATIC_KW = 325,
    UUID_REP = 326,
    COLON = 327,
    COMMA = 328,
    DOTDOT = 329,
    EQUAL = 330,
    LBRACE = 331,
    LBRACKET = 332,
    LPAREN = 333,
    RBRACE = 334,
    RBRACKET = 335,
    RPAREN = 336,
    SEMI = 337,
    STAR = 338,
    QUESTION = 339,
    BAR = 340,
    BARBAR = 341,
    LANGLE = 342,
    LANGLEANGLE = 343,
    RANGLE = 344,
    RANGLEANGLE = 345,
    AMP = 346,
    AMPAMP = 347,
    LESSEQUAL = 348,
    GREATEREQUAL = 349,
    EQUALEQUAL = 350,
    CARET = 351,
    PLUS = 352,
    MINUS = 353,
    NOT = 354,
    NOTEQUAL = 355,
    SLASH = 356,
    PERCENT = 357,
    TILDE = 358,
    POUND = 359,
    UNKNOWN = 360,
    IDENTIFIER = 361,
    STRING = 362,
    INTEGER_NUMERIC = 363,
    CHAR = 364,
    FLOAT_NUMERIC = 365
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 127 "nidl.y"

    NAMETABLE_id_t         y_id ;          /* Identifier           */
    STRTAB_str_t           y_string ;      /* String               */
    STRTAB_str_t           y_float ;       /* Float constant       */
    long                   y_ival ;        /* Integer constant     */
    AST_export_n_t*        y_export ;      /* an export node       */
    AST_import_n_t*        y_import ;      /* Import node          */
    AST_exception_n_t*     y_exception ;   /* Exception node       */
    AST_constant_n_t*      y_constant;     /* Constant node        */
    AST_parameter_n_t*     y_parameter ;   /* Parameter node       */
    AST_type_n_t*          y_type ;        /* Type node            */
    AST_type_p_n_t*        y_type_ptr ;    /* Type pointer node    */
    AST_field_n_t*         y_field ;       /* Field node           */
    AST_arm_n_t*           y_arm ;         /* Union variant arm    */
    AST_operation_n_t*     y_operation ;   /* Routine node         */
    AST_interface_n_t*     y_interface ;   /* Interface node       */
    AST_case_label_n_t*    y_label ;       /* Union tags           */
    ASTP_declarator_n_t*   y_declarator ;  /* Declarator info      */
    ASTP_array_index_n_t*  y_index ;       /* Array index info     */
    nidl_uuid_t            y_uuid ;        /* Universal UID        */
    char                   y_char;         /* character constant   */
    ASTP_attributes_t      y_attributes;   /* attributes flags     */
    struct {
        AST_type_k_t    int_size;
        int             int_signed;
        }                  y_int_info;     /* int size and signedness */
    ASTP_exp_n_t           y_exp;          /* constant expression info */

#line 339 "nidl_y_tab.c"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_NIDL_Y_TAB_H_INCLUDED  */



#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))

/* Stored state numbers (used for stacks). */
typedef yytype_int16 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  4
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   584

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  111
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  116
/* YYNRULES -- Number of rules.  */
#define YYNRULES  278
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  453

#define YYUNDEFTOK  2
#define YYMAXUTOK   365


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   297,   297,   305,   312,   325,   328,   335,   340,   356,
     358,   363,   371,   384,   386,   391,   392,   401,   405,   409,
     416,   417,   428,   443,   444,   454,   458,   463,   469,   476,
     490,   501,   508,   518,   522,   530,   532,   538,   539,   540,
     541,   542,   543,   544,   545,   550,   551,   552,   553,   558,
     565,   569,   575,   577,   581,   583,   588,   589,   593,   598,
     603,   608,   616,   618,   623,   631,   633,   635,   643,   648,
     653,   658,   663,   668,   675,   683,   693,   702,   711,   720,
     729,   738,   745,   751,   758,   759,   767,   768,   777,   783,
     790,   791,   799,   800,   809,   815,   819,   826,   831,   839,
     843,   864,   868,   872,   879,   880,   889,   902,   908,   908,
     911,   918,   919,   928,   937,   938,   943,   950,   954,   962,
     966,   970,   978,   987,   991,   995,   999,  1001,  1007,  1011,
    1039,  1044,  1049,  1054,  1059,  1064,  1069,  1078,  1093,  1102,
    1105,  1112,  1119,  1126,  1134,  1135,  1143,  1149,  1184,  1192,
    1195,  1210,  1217,  1224,  1231,  1243,  1263,  1264,  1269,  1273,
    1274,  1275,  1279,  1283,  1292,  1298,  1304,  1313,  1321,  1331,
    1332,  1333,  1337,  1343,  1358,  1359,  1363,  1367,  1376,  1383,
    1401,  1406,  1414,  1418,  1422,  1426,  1430,  1434,  1442,  1443,
    1452,  1456,  1460,  1465,  1471,  1478,  1482,  1492,  1496,  1504,
    1505,  1515,  1529,  1532,  1551,  1554,  1556,  1558,  1560,  1565,
    1567,  1569,  1573,  1575,  1581,  1583,  1585,  1587,  1589,  1591,
    1593,  1597,  1599,  1601,  1604,  1607,  1610,  1613,  1615,  1620,
    1627,  1632,  1635,  1651,  1656,  1658,  1669,  1671,  1681,  1683,
    1693,  1695,  1705,  1707,  1717,  1719,  1729,  1731,  1738,  1748,
    1750,  1757,  1764,  1771,  1781,  1783,  1790,  1800,  1802,  1812,
    1822,  1824,  1834,  1844,  1856,  1861,  1863,  1869,  1875,  1881,
    1890,  1892,  1897,  1902,  1907,  1912,  1918,  1924,  1929
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "ALIGN_KW", "BYTE_KW", "CHAR_KW",
  "CONST_KW", "DEFAULT_KW", "ENUM_KW", "EXCEPTIONS_KW", "FLOAT_KW",
  "HYPER_KW", "INT_KW", "INTERFACE_KW", "IMPORT_KW", "LONG_KW", "PIPE_KW",
  "REF_KW", "SMALL_KW", "STRUCT_KW", "TYPEDEF_KW", "UNION_KW",
  "UNSIGNED_KW", "SHORT_KW", "VOID_KW", "DOUBLE_KW", "BOOLEAN_KW",
  "CASE_KW", "SWITCH_KW", "HANDLE_T_KW", "TRUE_KW", "FALSE_KW", "NULL_KW",
  "BROADCAST_KW", "COMM_STATUS_KW", "CONTEXT_HANDLE_KW", "FIRST_IS_KW",
  "HANDLE_KW", "IDEMPOTENT_KW", "IGNORE_KW", "IN_KW", "LAST_IS_KW",
  "LENGTH_IS_KW", "LOCAL_KW", "MAX_IS_KW", "MAYBE_KW", "CXX_LOOKUP_KW",
  "CLIENT_MEMORY_KW", "MIN_IS_KW", "MUTABLE_KW", "OUT_KW",
  "POINTER_DEFAULT_KW", "ENDPOINT_KW", "PTR_KW", "REFLECT_DELETIONS_KW",
  "REMOTE_KW", "SECURE_KW", "SHAPE_KW", "SIZE_IS_KW", "STRING_KW",
  "SWITCH_IS_KW", "SWITCH_TYPE_KW", "TRANSMIT_AS_KW", "UNIQUE_KW",
  "UUID_KW", "VERSION_KW", "V1_ARRAY_KW", "V1_STRING_KW", "V1_ENUM_KW",
  "V1_STRUCT_KW", "STATIC_KW", "UUID_REP", "COLON", "COMMA", "DOTDOT",
  "EQUAL", "LBRACE", "LBRACKET", "LPAREN", "RBRACE", "RBRACKET", "RPAREN",
  "SEMI", "STAR", "QUESTION", "BAR", "BARBAR", "LANGLE", "LANGLEANGLE",
  "RANGLE", "RANGLEANGLE", "AMP", "AMPAMP", "LESSEQUAL", "GREATEREQUAL",
  "EQUALEQUAL", "CARET", "PLUS", "MINUS", "NOT", "NOTEQUAL", "SLASH",
  "PERCENT", "TILDE", "POUND", "UNKNOWN", "IDENTIFIER", "STRING",
  "INTEGER_NUMERIC", "CHAR", "FLOAT_NUMERIC", "$accept", "grammer_start",
  "interface", "interface_start", "interface_derivation",
  "interface_derivation_list", "interface_init", "interface_tail",
  "interface_body", "optional_imports", "imports", "import",
  "import_files", "import_file", "exports", "export", "const_dcl",
  "const_exp", "type_dcl", "type_declarator", "type_spec",
  "optional_const_qual", "simple_type_spec", "constructed_type_spec",
  "named_type_spec", "floating_point_type_spec", "extraneous_comma",
  "extraneous_semi", "optional_unsigned_kw", "integer_size_spec",
  "integer_modifiers", "integer_type_spec", "char_type_spec",
  "boolean_type_spec", "byte_type_spec", "void_type_spec",
  "handle_type_spec", "push_name_space", "pop_name_space",
  "union_type_spec", "ne_union_body", "union_body", "ne_union_cases",
  "union_cases", "ne_union_case", "union_case", "ne_union_case_list",
  "union_case_list", "ne_union_case_label", "union_case_label",
  "ne_union_member", "union_member", "struct_type_spec", "member_list",
  "member", "enum_type_spec", "optional_tag", "enum_body", "enum_ids",
  "enum_id", "optional_value", "pipe_type_spec", "declarators",
  "declarator", "declarator1", "pointer", "reference", "direct_declarator",
  "array_bounds", "operation_dcl", "optional_static", "parameter_dcls",
  "param_names", "end_param_names", "param_list", "param_dcl",
  "declarator_or_null", "attribute_opener", "attribute_closer",
  "bounds_opener", "bounds_closer", "old_attribute_syntax",
  "interface_attributes", "interface_attr_list", "interface_attr",
  "pointer_class", "version_number", "port_list", "excep_list",
  "port_spec", "excep_spec", "fp_attribute", "array_bound_type",
  "array_bound_id_list", "array_bound_id", "neu_switch_type",
  "neu_switch_id", "attributes", "rest_of_attribute_list",
  "attribute_list", "attribute", "expression", "conditional_expression",
  "logical_OR_expression", "logical_AND_expression",
  "inclusive_OR_expression", "exclusive_OR_expression", "AND_expression",
  "equality_expression", "relational_expression", "shift_expression",
  "additive_expression", "multiplicative_expression", "cast_expression",
  "unary_expression", "primary_expression", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365
};
# endif

#define YYPACT_NINF (-290)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-199)

#define yytable_value_is_error(Yyn) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    -290,    59,  -290,   -59,  -290,  -290,    12,   118,    92,    22,
      47,   107,    61,  -290,   155,   184,    15,   196,    87,  -290,
     141,  -290,  -290,   199,   269,  -290,  -290,  -290,   200,    10,
     205,  -290,  -290,    66,   242,  -290,  -290,  -290,    14,   238,
      88,   269,  -290,  -290,   108,  -290,  -290,  -290,  -290,   241,
    -290,   115,  -290,  -290,  -290,   245,  -290,   171,   237,  -290,
      65,  -290,  -290,   250,   329,   -59,   254,  -290,   258,   261,
     268,   231,   281,  -290,   242,  -290,  -290,   242,  -290,  -290,
    -290,  -290,   255,  -290,  -290,  -290,    58,   323,  -290,   329,
    -290,    18,  -290,  -290,  -290,   119,   285,  -290,  -290,   286,
    -290,  -290,  -290,  -290,  -290,  -290,   289,  -290,  -290,  -290,
    -290,  -290,   292,  -290,  -290,  -290,  -290,  -290,   293,   297,
    -290,  -290,  -290,  -290,  -290,  -290,  -290,   299,   299,  -290,
     146,  -290,  -290,   395,   200,   205,  -290,    58,   329,   329,
    -290,   306,  -290,   -61,   -61,   -46,  -290,   276,  -290,  -290,
    -290,   329,  -290,   -56,    26,   148,  -290,  -290,  -290,  -290,
    -290,  -290,  -290,  -290,  -290,  -290,    78,   363,   376,  -290,
    -290,  -290,  -290,  -290,  -290,  -290,  -290,  -290,  -290,    58,
    -290,  -290,  -290,   290,   474,   341,   347,   182,   182,  -290,
      51,   -48,   242,  -290,  -290,    58,  -290,  -290,   327,   326,
    -290,   474,   -46,   -46,   147,  -290,  -290,  -290,    56,  -290,
     336,  -290,  -290,   339,   -59,   345,    -7,   -59,  -290,  -290,
    -290,  -290,  -290,  -290,   352,  -290,   346,   354,   366,   367,
    -290,  -290,  -290,   474,   324,   324,   324,   324,  -290,  -290,
    -290,  -290,  -290,  -290,   129,  -290,  -290,  -290,   156,   357,
     343,   348,   359,   120,   227,   219,   154,    52,  -290,  -290,
    -290,   370,   372,   381,   384,   360,   362,  -290,   140,  -290,
     364,  -290,   390,   419,   352,  -290,  -290,  -290,  -290,    71,
     169,   -59,   402,  -290,   329,   377,  -290,   -59,   181,  -290,
     329,   182,   398,  -290,   181,  -290,  -290,   231,   242,  -290,
    -290,  -290,  -290,   403,  -290,  -290,  -290,  -290,   474,  -290,
     474,   474,   474,   474,   474,   474,   474,   474,   474,   474,
     474,   474,   474,   474,   474,   474,   474,   474,   474,  -290,
    -290,  -290,  -290,  -290,  -290,    51,  -290,  -290,  -290,  -290,
    -290,   411,  -290,   460,  -290,    58,  -290,   143,   412,  -290,
     -59,   420,   186,  -290,   181,  -290,   -59,  -290,   -59,   391,
     182,   -59,  -290,    43,    58,  -290,  -290,   424,   357,   343,
     348,   359,   120,   227,   227,   219,   219,   219,   219,   154,
     154,    52,    52,  -290,  -290,  -290,  -290,   418,   422,   423,
     427,  -290,  -290,   368,  -290,    58,   474,  -290,   242,  -290,
    -290,  -290,    58,   430,   393,  -290,  -290,    58,  -290,   474,
    -290,  -290,  -290,  -290,  -290,  -290,  -290,   377,    70,   -30,
     431,   433,  -290,  -290,  -290,   339,   123,  -290,    25,  -290,
    -290,   428,   474,   181,  -290,   113,  -290,   339,  -290,  -290,
     441,   123,  -290,  -290,  -290,  -290,   329,  -290,  -290,  -290,
      58,   434,  -290
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_int16 yydefact[] =
{
       8,     0,     2,   158,     1,   151,     5,     0,     0,     0,
       0,     0,     0,   167,     0,     0,     0,     0,     0,   159,
       0,     7,     6,    10,    14,     3,   152,   157,     0,     0,
       0,   162,   163,     0,    52,   156,     4,    11,     0,     0,
       0,    13,    15,   179,     0,   176,   169,   170,   171,     0,
     178,     0,   174,   172,   173,     0,    53,     0,    17,    22,
       0,    20,     9,     0,    35,   198,    54,    23,     0,     0,
       0,     0,   139,    16,    52,   165,   168,    52,   164,   166,
     160,    18,     0,    19,    28,    36,     0,    57,    31,    35,
      55,     0,    26,    25,    27,     0,     0,   231,   218,     0,
     205,   220,   182,   227,   207,   219,   210,   183,   184,   185,
     206,   186,   212,   209,   208,   187,   215,   194,     0,     0,
     217,   214,   216,   222,   221,   232,   204,     0,     0,   197,
       0,   202,   140,     0,     0,     0,    21,     0,    35,    35,
     126,     0,   119,     0,     0,   120,    70,   109,    50,    61,
      60,    35,    58,     0,     0,    56,    59,    71,    51,    69,
      72,    49,    33,    34,    43,    37,     0,    62,    65,    38,
      39,    40,    41,    42,    44,    46,    45,    47,    48,     0,
      24,   201,   200,     0,     0,     0,     0,    57,    57,   153,
     193,     0,    52,   199,   138,     0,   177,   175,     0,   123,
     125,     0,   121,   122,     0,   142,   127,   129,     0,   108,
       0,   116,    73,   103,   198,     0,    81,     0,    75,    63,
      68,    67,    64,    66,    32,   117,     0,     0,     0,     0,
     276,   277,   275,     0,     0,     0,     0,     0,   273,   274,
     271,   272,   278,    94,     0,    90,    30,   233,   234,   236,
     238,   240,   242,   244,   246,   249,   254,   257,   260,   264,
     265,     0,     0,     0,     0,     0,     0,   190,     0,   188,
       0,   195,     0,     0,   137,   128,   124,    29,   130,     0,
       0,   198,    52,   144,    35,     0,   107,   198,    54,   104,
      35,    57,     0,    77,    54,    84,    88,     0,    52,   226,
     225,   223,   224,     0,   266,   267,   269,   268,     0,   230,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   211,
     213,   229,   228,   191,   192,   193,   154,   180,   196,   181,
     203,     0,   131,     0,   132,   150,   155,    52,     0,   141,
     198,   114,     0,   111,    54,    74,   198,   101,   198,     0,
      57,     0,    82,    35,     0,   270,    91,     0,   237,   239,
     241,   243,   245,   247,   248,   250,   251,   252,   253,   255,
     256,   258,   259,   261,   262,   263,   189,     0,     0,     0,
       0,   149,   148,     0,   143,   150,     0,   113,    52,   110,
     102,   105,     0,     0,     0,    85,    97,     0,   118,     0,
     133,   134,   135,   136,   145,   147,   115,     0,     0,     0,
       0,     0,   235,   112,   106,     0,     0,    76,     0,    98,
      78,     0,     0,    54,    86,   198,    92,     0,    79,    96,
       0,     0,    83,    99,    93,    89,    35,    80,    95,    87,
       0,     0,   100
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -290,  -290,  -290,  -290,  -290,  -290,  -290,  -290,  -290,  -290,
    -290,   481,  -290,   442,  -290,   432,  -290,  -195,  -290,  -290,
     -81,    17,  -175,  -290,  -290,  -290,   -70,   -65,  -290,   371,
    -290,  -290,  -290,  -290,  -290,  -290,  -290,   -98,  -289,  -290,
     311,  -253,  -290,  -290,   167,    89,  -290,  -290,   221,    96,
    -290,  -290,  -290,   246,   176,  -290,  -290,  -290,  -290,   117,
    -290,  -290,  -184,   -84,  -290,   337,  -290,   152,  -290,  -290,
    -290,  -290,  -290,  -290,  -290,   142,   144,     0,    -4,   409,
     270,  -119,  -290,  -290,   483,  -290,  -290,  -290,  -290,   406,
     410,  -290,  -290,  -290,   210,  -290,  -290,   -40,   249,  -290,
     274,  -223,   139,  -290,   239,   243,   236,   240,   247,     8,
      39,   -36,     5,   -24,  -290,   203
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     2,     6,    10,    22,     3,    25,    39,    40,
      41,    42,    60,    61,    66,    67,    68,   243,    69,    88,
      86,    87,   162,   163,   164,   165,    57,   356,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   426,   357,   175,
     218,   427,   294,   433,   295,   434,   244,   435,   245,   436,
     296,   445,   176,   288,   289,   177,   210,   286,   352,   353,
     397,   178,   224,   225,   142,   143,   144,   145,   206,    70,
     133,   207,   208,   349,   282,   283,   392,    71,    27,   190,
     337,   345,     8,    18,    19,    49,    55,    51,    44,    52,
      45,   126,   127,   268,   269,   128,   272,   290,   129,   130,
     131,   246,   247,   248,   249,   250,   251,   252,   253,   254,
     255,   256,   257,   258,   259,   260
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      72,    91,   141,     7,   134,   362,   277,   135,   179,   280,
     303,   274,   263,   264,    35,    58,    31,   137,     5,    63,
     212,   292,  -198,  -198,    64,    89,  -198,    46,  -198,  -198,
    -198,   204,   205,  -198,  -198,   270,  -198,  -198,    65,  -198,
    -198,  -198,  -198,  -198,  -198,   140,   212,  -198,    23,    85,
     213,    72,   195,   198,   215,   214,   217,   281,   271,     4,
    -198,  -198,  -198,    47,  -198,   400,  -198,  -198,  -198,   212,
     211,  -198,  -198,    48,  -198,  -198,   425,  -198,  -198,  -198,
    -198,  -198,  -198,   220,     9,  -198,    32,   367,  -198,    63,
     221,   182,  -198,  -198,    64,     5,  -198,   -12,  -198,  -198,
    -198,   212,   212,  -198,  -198,    20,  -198,  -198,    65,  -198,
    -198,  -198,  -198,  -198,  -198,   287,   359,  -198,   217,    11,
     431,    59,   273,    24,  -198,   406,   193,    12,    21,  -146,
     431,   437,   216,     5,   265,   326,   137,  -146,    82,    28,
     432,   138,   266,   298,   442,   341,   388,    83,   390,   139,
     432,   342,   424,   327,   328,   199,   200,   267,  -198,   149,
      34,    13,  -198,   150,   140,     5,   152,    26,   284,    14,
      15,   156,   430,   416,    53,   438,    54,   230,   231,   232,
      12,    74,    16,    17,   447,   404,   146,    26,    77,    75,
       5,  -161,   148,   149,  -198,   443,    78,   150,  -161,    26,
     152,   181,   308,   350,   155,   156,   157,   158,   159,   358,
     309,   160,   348,   335,    13,   316,    56,   297,   418,   192,
     317,   336,    14,    15,   -53,   233,    26,   278,   364,   361,
     279,   395,    95,    29,    96,    16,    17,   440,    97,   402,
     310,   346,   311,   343,   234,   235,   236,    36,    98,   344,
     237,   324,   325,   238,   239,   240,   241,   242,    99,   398,
     355,   391,    30,    90,   100,   399,   101,   102,   103,   104,
     105,   106,   107,   108,    33,   109,   110,   393,    37,   111,
     408,   112,   407,    38,   113,   114,   379,   380,   161,   115,
     116,   117,   118,   119,   120,   202,   203,   121,   122,   123,
     124,   226,   383,   384,   385,   227,    43,   322,   228,   323,
     346,   391,    50,   229,   318,    56,   319,    62,   346,    81,
     320,   321,    76,   421,   373,   374,    79,   146,   417,   381,
     382,   147,    84,   148,   149,    85,    90,   125,   150,   151,
      92,   152,   153,    93,   154,   155,   156,   157,   158,   159,
      94,   132,   160,   284,   230,   231,   232,   375,   376,   377,
     378,   297,    59,   183,   184,   450,   451,   185,   441,   281,
     186,   187,  -198,  -198,  -198,   188,  -198,   189,  -198,  -198,
    -198,   201,   209,  -198,  -198,   222,  -198,  -198,   223,  -198,
    -198,  -198,  -198,  -198,  -198,   446,   194,  -198,   261,   -35,
     -35,    85,   233,   -35,   262,   -35,   -35,   -35,   275,   138,
     -35,   -35,   285,   -35,   -35,   212,   -35,   -35,   -35,   -35,
     -35,   -35,    96,   291,   -35,   298,    97,   299,   313,   161,
     238,   239,   240,   241,   242,   300,    98,   304,   305,   306,
     307,   230,   231,   232,   314,     5,    99,   301,   302,   312,
     315,   329,   100,   330,   101,   102,   103,   104,   105,   106,
     107,   108,   331,   109,   110,   332,   333,   111,   334,   112,
     338,   336,   113,   114,  -198,   347,   360,   115,   116,   117,
     118,   119,   120,   351,   365,   121,   122,   123,   124,   233,
     230,   231,   232,   394,   387,   396,   409,   403,   410,   420,
     439,   -35,   411,   412,   230,   231,   232,   413,   234,   235,
     236,   419,   428,   448,   237,   429,   452,   238,   239,   240,
     241,   242,    73,   180,   136,   125,   219,   293,   405,   366,
     449,   444,   401,   354,   423,   414,   276,   191,   233,   415,
      80,   197,   339,   389,   196,   386,   363,   340,   422,   370,
     368,     0,   233,     0,   371,   369,     0,   234,   235,   236,
       0,     0,   372,   237,     0,     0,   238,   239,   240,   241,
     242,   234,   235,   236,     0,     0,     0,   237,     0,     0,
     238,   239,   240,   241,   242
};

static const yytype_int16 yycheck[] =
{
      40,    66,    86,     3,    74,   294,   201,    77,    89,   204,
     233,   195,   187,   188,    18,     1,     1,    78,    77,     1,
      76,    28,     4,     5,     6,    65,     8,    17,    10,    11,
      12,    77,    78,    15,    16,    83,    18,    19,    20,    21,
      22,    23,    24,    25,    26,   106,    76,    29,     1,     6,
     106,    91,   133,   137,    28,   153,   154,     1,   106,     0,
       4,     5,     6,    53,     8,   354,    10,    11,    12,    76,
     151,    15,    16,    63,    18,    19,   106,    21,    22,    23,
      24,    25,    26,     5,    72,    29,    71,   310,    70,     1,
      12,    95,     4,     5,     6,    77,     8,    79,    10,    11,
      12,    76,    76,    15,    16,    13,    18,    19,    20,    21,
      22,    23,    24,    25,    26,   213,   291,    29,   216,     1,
       7,   107,   192,    76,   106,    82,   130,     9,   106,    73,
       7,   106,   106,    77,    83,    83,    78,    81,    73,    78,
      27,    83,    91,    73,   433,    74,   341,    82,   343,    91,
      27,    80,    82,   101,   102,   138,   139,   106,    70,    11,
      73,    43,   106,    15,   106,    77,    18,    80,   208,    51,
      52,    23,   425,   396,   108,   428,   110,    30,    31,    32,
       9,    73,    64,    65,   437,   360,     4,    80,    73,    81,
      77,    73,    10,    11,   106,    82,    81,    15,    80,    80,
      18,    82,    73,   284,    22,    23,    24,    25,    26,   290,
      81,    29,   282,    73,    43,    95,    73,   217,   402,    73,
     100,    81,    51,    52,    81,    78,    80,    80,   298,   294,
      83,   350,     1,    78,     3,    64,    65,   432,     7,   358,
      84,   281,    86,    74,    97,    98,    99,   106,    17,    80,
     103,    97,    98,   106,   107,   108,   109,   110,    27,    73,
      79,   345,    78,    82,    33,    79,    35,    36,    37,    38,
      39,    40,    41,    42,    78,    44,    45,   347,    79,    48,
     364,    50,   363,    14,    53,    54,   322,   323,   106,    58,
      59,    60,    61,    62,    63,   143,   144,    66,    67,    68,
      69,    11,   326,   327,   328,    15,   106,    88,    18,    90,
     350,   395,   107,    23,    87,    73,    89,    79,   358,    82,
      93,    94,    81,   407,   316,   317,    81,     4,   398,   324,
     325,     8,    82,    10,    11,     6,    82,   106,    15,    16,
      82,    18,    19,    82,    21,    22,    23,    24,    25,    26,
      82,    70,    29,   393,    30,    31,    32,   318,   319,   320,
     321,   361,   107,    78,    78,   446,   450,    78,   433,     1,
      78,    78,     4,     5,     6,    78,     8,    78,    10,    11,
      12,    75,   106,    15,    16,    22,    18,    19,    12,    21,
      22,    23,    24,    25,    26,   435,     1,    29,    57,     4,
       5,     6,    78,     8,    57,    10,    11,    12,    81,    83,
      15,    16,    76,    18,    19,    76,    21,    22,    23,    24,
      25,    26,     3,    78,    29,    73,     7,    81,    85,   106,
     106,   107,   108,   109,   110,    81,    17,   234,   235,   236,
     237,    30,    31,    32,    96,    77,    27,    81,    81,    92,
      91,    81,    33,    81,    35,    36,    37,    38,    39,    40,
      41,    42,    81,    44,    45,    81,   106,    48,   106,    50,
     106,    81,    53,    54,   106,    73,    78,    58,    59,    60,
      61,    62,    63,   106,    81,    66,    67,    68,    69,    78,
      30,    31,    32,    81,    83,    75,    72,   106,    80,   106,
      72,   106,    80,    80,    30,    31,    32,    80,    97,    98,
      99,    81,    81,    72,   103,    82,    82,   106,   107,   108,
     109,   110,    41,    91,    82,   106,   155,   216,   361,   308,
     441,   435,   356,   287,   417,   393,   199,   128,    78,   395,
      57,   135,   272,    83,   134,   335,   297,   273,   409,   313,
     311,    -1,    78,    -1,   314,   312,    -1,    97,    98,    99,
      -1,    -1,   315,   103,    -1,    -1,   106,   107,   108,   109,
     110,    97,    98,    99,    -1,    -1,    -1,   103,    -1,    -1,
     106,   107,   108,   109,   110
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,   112,   113,   117,     0,    77,   114,   188,   193,    72,
     115,     1,     9,    43,    51,    52,    64,    65,   194,   195,
      13,   106,   116,     1,    76,   118,    80,   189,    78,    78,
      78,     1,    71,    78,    73,   189,   106,    79,    14,   119,
     120,   121,   122,   106,   199,   201,    17,    53,    63,   196,
     107,   198,   200,   108,   110,   197,    73,   137,     1,   107,
     123,   124,    79,     1,     6,    20,   125,   126,   127,   129,
     180,   188,   208,   122,    73,    81,    81,    73,    81,    81,
     195,    82,    73,    82,    82,     6,   131,   132,   130,   208,
      82,   138,    82,    82,    82,     1,     3,     7,    17,    27,
      33,    35,    36,    37,    38,    39,    40,    41,    42,    44,
      45,    48,    50,    53,    54,    58,    59,    60,    61,    62,
      63,    66,    67,    68,    69,   106,   202,   203,   206,   209,
     210,   211,    70,   181,   137,   137,   124,    78,    83,    91,
     106,   174,   175,   176,   177,   178,     4,     8,    10,    11,
      15,    16,    18,    19,    21,    22,    23,    24,    25,    26,
      29,   106,   133,   134,   135,   136,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   150,   163,   166,   172,   131,
     126,    82,   189,    78,    78,    78,    78,    78,    78,    78,
     190,   190,    73,   189,     1,   131,   201,   200,   174,   132,
     132,    75,   178,   178,    77,    78,   179,   182,   183,   106,
     167,   131,    76,   106,   148,    28,   106,   148,   151,   140,
       5,    12,    22,    12,   173,   174,    11,    15,    18,    23,
      30,    31,    32,    78,    97,    98,    99,   103,   106,   107,
     108,   109,   110,   128,   157,   159,   212,   213,   214,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,   225,
     226,    57,    57,   133,   133,    83,    91,   106,   204,   205,
      83,   106,   207,   137,   173,    81,   176,   128,    80,    83,
     128,     1,   185,   186,   208,    76,   168,   148,   164,   165,
     208,    78,    28,   151,   153,   155,   161,   188,    73,    81,
      81,    81,    81,   212,   226,   226,   226,   226,    73,    81,
      84,    86,    92,    85,    96,    91,    95,   100,    87,    89,
      93,    94,    88,    90,    97,    98,    83,   101,   102,    81,
      81,    81,    81,   106,   106,    73,    81,   191,   106,   191,
     211,    74,    80,    74,    80,   192,   208,    73,   137,   184,
     131,   106,   169,   170,   164,    79,   138,   149,   131,   133,
      78,   138,   149,   209,   137,    81,   159,   212,   215,   216,
     217,   218,   219,   220,   220,   221,   221,   221,   221,   222,
     222,   223,   223,   224,   224,   224,   205,    83,   128,    83,
     128,   174,   187,   137,    81,   192,    75,   171,    73,    79,
     149,   165,   192,   106,   133,   155,    82,   131,   174,    72,
      80,    80,    80,    80,   186,   187,   212,   137,   173,    81,
     106,   174,   213,   170,    82,   106,   148,   152,    81,    82,
     152,     7,    27,   154,   156,   158,   160,   106,   152,    72,
     128,   138,   149,    82,   160,   162,   208,   152,    72,   156,
     131,   174,    82
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,   111,   112,   113,   114,   115,   115,   116,   117,   118,
     118,   118,   119,   120,   120,   121,   121,   122,   122,   122,
     123,   123,   124,   125,   125,   126,   126,   126,   126,   127,
     128,   129,   130,   131,   131,   132,   132,   133,   133,   133,
     133,   133,   133,   133,   133,   134,   134,   134,   134,   135,
     136,   136,   137,   137,   138,   138,   139,   139,   140,   140,
     140,   140,   141,   141,   141,   142,   142,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   150,   150,   150,   150,
     150,   150,   151,   152,   153,   153,   154,   154,   155,   156,
     157,   157,   158,   158,   159,   160,   160,   161,   161,   162,
     162,   163,   163,   163,   164,   164,   165,   166,   167,   167,
     168,   169,   169,   170,   171,   171,   172,   173,   173,   174,
     175,   175,   175,   176,   176,   177,   178,   178,   178,   178,
     179,   179,   179,   179,   179,   179,   179,   180,   180,   181,
     181,   182,   183,   184,   185,   185,   185,   186,   186,   187,
     187,   188,   189,   190,   191,   192,   193,   193,   193,   194,
     194,   194,   195,   195,   195,   195,   195,   195,   195,   196,
     196,   196,   197,   197,   198,   198,   199,   199,   200,   201,
     202,   202,   203,   203,   203,   203,   203,   203,   204,   204,
     205,   205,   205,   205,   206,   207,   207,   208,   208,   209,
     209,   209,   210,   210,   211,   211,   211,   211,   211,   211,
     211,   211,   211,   211,   211,   211,   211,   211,   211,   211,
     211,   211,   211,   211,   211,   211,   211,   211,   211,   211,
     211,   211,   211,   212,   213,   213,   214,   214,   215,   215,
     216,   216,   217,   217,   218,   218,   219,   219,   219,   220,
     220,   220,   220,   220,   221,   221,   221,   222,   222,   222,
     223,   223,   223,   223,   224,   225,   225,   225,   225,   225,
     226,   226,   226,   226,   226,   226,   226,   226,   226
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     1,     4,     3,     0,     2,     1,     0,     3,
       1,     2,     3,     1,     0,     1,     2,     2,     3,     3,
       1,     3,     1,     1,     3,     2,     2,     2,     2,     5,
       1,     2,     3,     2,     2,     0,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     0,     1,     0,     1,     1,     0,     1,     1,
       1,     1,     1,     2,     2,     1,     2,     2,     2,     1,
       1,     1,     1,     1,     1,     2,     7,     3,     8,     8,
       9,     2,     3,     3,     1,     3,     1,     3,     1,     2,
       1,     3,     1,     2,     1,     3,     2,     3,     5,     1,
       4,     4,     5,     2,     1,     3,     5,     3,     1,     0,
       3,     1,     4,     2,     0,     2,     2,     1,     4,     1,
       1,     2,     2,     2,     3,     2,     1,     2,     3,     2,
       2,     3,     3,     5,     5,     5,     5,     4,     3,     0,
       1,     3,     1,     2,     1,     4,     0,     4,     3,     1,
       0,     1,     1,     1,     1,     1,     3,     3,     0,     1,
       4,     0,     2,     2,     4,     4,     4,     1,     4,     1,
       1,     1,     1,     1,     1,     4,     1,     4,     1,     1,
       4,     4,     1,     1,     1,     1,     1,     1,     1,     3,
       1,     2,     2,     0,     1,     1,     2,     2,     0,     2,
       2,     2,     1,     4,     1,     1,     1,     1,     1,     1,
       1,     4,     1,     4,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     4,     4,     4,     4,     1,     4,     4,
       4,     1,     1,     1,     1,     5,     1,     3,     1,     3,
       1,     3,     1,     3,     1,     3,     1,     3,     3,     1,
       3,     3,     3,     3,     1,     3,     3,     1,     3,     3,
       1,     3,     3,     3,     1,     1,     2,     2,     2,     2,
       3,     1,     1,     1,     1,     1,     1,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yytype], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyo, yytype, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[+yyssp[yyi + 1 - yynrhs]],
                       &yyvsp[(yyi + 1) - (yynrhs)]
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
#  else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
yystrlen (const char *yystr)
{
  YYPTRDIFF_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYPTRDIFF_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYPTRDIFF_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (yyres)
    return yystpcpy (yyres, yystr) - yyres;
  else
    return yystrlen (yystr);
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYPTRDIFF_T *yymsg_alloc, char **yymsg,
                yy_state_t *yyssp, int yytoken)
{
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat: reported tokens (one for the "unexpected",
     one per "expected"). */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Actual size of YYARG. */
  int yycount = 0;
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T yysize = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[+*yyssp];
      YYPTRDIFF_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
      yysize = yysize0;
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYPTRDIFF_T yysize1
                    = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
                    yysize = yysize1;
                  else
                    return 2;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    /* Don't count the "%s"s in the final size, but reserve room for
       the terminator.  */
    YYPTRDIFF_T yysize1 = yysize + (yystrlen (yyformat) - 2 * yycount) + 1;
    if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
      yysize = yysize1;
    else
      return 2;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          ++yyp;
          ++yyformat;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss;
    yy_state_t *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYPTRDIFF_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYPTRDIFF_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
# undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2:
#line 298 "nidl.y"
        {
            /* Support for multiple interfaces...  */
        }
#line 1931 "nidl_y_tab.c"
    break;

  case 3:
#line 306 "nidl.y"
        {
            AST_finish_interface_node(the_interface);
        }
#line 1939 "nidl_y_tab.c"
    break;

  case 4:
#line 313 "nidl.y"
        {
	    AST_type_n_t *interface_type = AST_type_node(AST_interface_k);
	    interface_type->type_structure.interface = the_interface;
	    interface_type->name = (yyvsp[0].y_id); 
            the_interface->name = (yyvsp[0].y_id);
            ASTP_add_name_binding(the_interface->name, (char *)interface_type);

        }
#line 1952 "nidl_y_tab.c"
    break;

  case 5:
#line 325 "nidl.y"
        {
	    the_interface->inherited_interface_name = NAMETABLE_NIL_ID;
	}
#line 1960 "nidl_y_tab.c"
    break;

  case 6:
#line 329 "nidl.y"
        {
	    the_interface->inherited_interface_name = (yyvsp[0].y_id);
	}
#line 1968 "nidl_y_tab.c"
    break;

  case 8:
#line 340 "nidl.y"
        {
            STRTAB_str_t nidl_idl_str;
            nidl_idl_str = STRTAB_add_string (AUTO_IMPORT_FILE);
            the_interface = AST_interface_node();
            the_interface->exports = NULL;
            the_interface->imports = AST_import_node(nidl_idl_str);
            the_interface->imports->interface = FE_parse_import (nidl_idl_str);
            if (the_interface->imports->interface != NULL)
            {
                AST_CLR_OUT_OF_LINE(the_interface->imports->interface);
                AST_SET_IN_LINE(the_interface->imports->interface);
            }
        }
#line 1986 "nidl_y_tab.c"
    break;

  case 9:
#line 357 "nidl.y"
        { (yyval.y_interface) = (yyvsp[-1].y_interface); }
#line 1992 "nidl_y_tab.c"
    break;

  case 10:
#line 359 "nidl.y"
        {
            (yyval.y_interface) = NULL;
            log_error(yylineno,NIDL_MISSONINTER);
        }
#line 2001 "nidl_y_tab.c"
    break;

  case 11:
#line 364 "nidl.y"
        {
            (yyval.y_interface) = NULL;
        }
#line 2009 "nidl_y_tab.c"
    break;

  case 12:
#line 372 "nidl.y"
        {
            /* May already be an import of nbase, so concat */
            the_interface->imports = (AST_import_n_t *) AST_concat_element(
                                        (ASTP_node_t *) the_interface->imports,
                                        (ASTP_node_t *) (yyvsp[-2].y_import));
            the_interface->exports = (AST_export_n_t *) AST_concat_element(
                                            (ASTP_node_t *) the_interface->exports,
                                            (ASTP_node_t *) (yyvsp[-1].y_export)) ;
        }
#line 2023 "nidl_y_tab.c"
    break;

  case 14:
#line 386 "nidl.y"
        {
            (yyval.y_import) = (AST_import_n_t *)NULL;
        }
#line 2031 "nidl_y_tab.c"
    break;

  case 16:
#line 393 "nidl.y"
        {
                (yyval.y_import) = (AST_import_n_t *) AST_concat_element(
                                                (ASTP_node_t *) (yyvsp[-1].y_import),
                                                (ASTP_node_t *) (yyvsp[0].y_import));
        }
#line 2041 "nidl_y_tab.c"
    break;

  case 17:
#line 402 "nidl.y"
        {
            (yyval.y_import) = (AST_import_n_t *)NULL;
        }
#line 2049 "nidl_y_tab.c"
    break;

  case 18:
#line 406 "nidl.y"
        {
            (yyval.y_import) = (AST_import_n_t *)NULL;
        }
#line 2057 "nidl_y_tab.c"
    break;

  case 19:
#line 410 "nidl.y"
        {
            (yyval.y_import) = (yyvsp[-1].y_import);
        }
#line 2065 "nidl_y_tab.c"
    break;

  case 21:
#line 418 "nidl.y"
        {
                (yyval.y_import) = (AST_import_n_t *) AST_concat_element(
                                                (ASTP_node_t *) (yyvsp[-2].y_import),
                                                (ASTP_node_t *) (yyvsp[0].y_import));
        }
#line 2075 "nidl_y_tab.c"
    break;

  case 22:
#line 429 "nidl.y"
        {
            AST_interface_n_t  *int_p;
            int_p = FE_parse_import ((yyvsp[0].y_string));
            if (int_p != (AST_interface_n_t *)NULL)
            {
                (yyval.y_import) = AST_import_node((yyvsp[0].y_string));
                (yyval.y_import)->interface = int_p;
            }
            else
                (yyval.y_import) = (AST_import_n_t *)NULL;
        }
#line 2091 "nidl_y_tab.c"
    break;

  case 24:
#line 445 "nidl.y"
        {
                (yyval.y_export) = (AST_export_n_t *) AST_concat_element(
                                            (ASTP_node_t *) (yyvsp[-2].y_export),
                                            (ASTP_node_t *) (yyvsp[0].y_export)) ;
        }
#line 2101 "nidl_y_tab.c"
    break;

  case 25:
#line 455 "nidl.y"
        {
                (yyval.y_export) = AST_types_to_exports ((yyvsp[-1].y_type_ptr));
        }
#line 2109 "nidl_y_tab.c"
    break;

  case 26:
#line 459 "nidl.y"
        {
                (yyval.y_export) = AST_export_node (
                        (ASTP_node_t *) (yyvsp[-1].y_constant), AST_constant_k);
        }
#line 2118 "nidl_y_tab.c"
    break;

  case 27:
#line 464 "nidl.y"
        {
            if (ASTP_parsing_main_idl)
                (yyval.y_export) = AST_export_node (
                    (ASTP_node_t *) (yyvsp[-1].y_operation), AST_operation_k);
        }
#line 2128 "nidl_y_tab.c"
    break;

  case 28:
#line 470 "nidl.y"
        {
            (yyval.y_export) = (AST_export_n_t *)NULL;
        }
#line 2136 "nidl_y_tab.c"
    break;

  case 29:
#line 478 "nidl.y"
        {
           (yyval.y_constant) = AST_finish_constant_node ((yyvsp[0].y_constant),
                                        (yyvsp[-2].y_declarator), (yyvsp[-3].y_type));
        }
#line 2145 "nidl_y_tab.c"
    break;

  case 30:
#line 491 "nidl.y"
        {
            if ((yyvsp[0].y_exp).type == AST_int_const_k)
                (yyval.y_constant) = AST_integer_constant ((yyvsp[0].y_exp).val.integer) ;
            else
                (yyval.y_constant) = (yyvsp[0].y_exp).val.other;
        }
#line 2156 "nidl_y_tab.c"
    break;

  case 31:
#line 502 "nidl.y"
        {
            (yyval.y_type_ptr) = (yyvsp[0].y_type_ptr);
        }
#line 2164 "nidl_y_tab.c"
    break;

  case 32:
#line 509 "nidl.y"
        {
            (yyval.y_type_ptr)  = AST_declarators_to_types((yyvsp[-1].y_type),
                        (yyvsp[0].y_declarator), &(yyvsp[-2].y_attributes)) ;
            ASTP_free_simple_list((ASTP_node_t *)(yyvsp[-2].y_attributes).bounds);
        }
#line 2174 "nidl_y_tab.c"
    break;

  case 33:
#line 519 "nidl.y"
        {
		(yyval.y_type) = (yyvsp[0].y_type);
	}
#line 2182 "nidl_y_tab.c"
    break;

  case 34:
#line 523 "nidl.y"
        {
		(yyval.y_type) = (yyvsp[0].y_type);
	}
#line 2190 "nidl_y_tab.c"
    break;

  case 35:
#line 530 "nidl.y"
        {
        }
#line 2197 "nidl_y_tab.c"
    break;

  case 36:
#line 533 "nidl.y"
        {
        }
#line 2204 "nidl_y_tab.c"
    break;

  case 49:
#line 559 "nidl.y"
        {
            (yyval.y_type) = AST_lookup_named_type((yyvsp[0].y_id));
        }
#line 2212 "nidl_y_tab.c"
    break;

  case 50:
#line 566 "nidl.y"
        {
            (yyval.y_type) = AST_lookup_type_node(AST_short_float_k);
        }
#line 2220 "nidl_y_tab.c"
    break;

  case 51:
#line 570 "nidl.y"
        {
            (yyval.y_type) = AST_lookup_type_node(AST_long_float_k);
        }
#line 2228 "nidl_y_tab.c"
    break;

  case 53:
#line 578 "nidl.y"
        { log_warning(yylineno, NIDL_EXTRAPUNCT, ",");}
#line 2234 "nidl_y_tab.c"
    break;

  case 55:
#line 584 "nidl.y"
        { log_warning(yylineno, NIDL_EXTRAPUNCT, ";");}
#line 2240 "nidl_y_tab.c"
    break;

  case 56:
#line 588 "nidl.y"
                        { (yyval.y_ival) = false; }
#line 2246 "nidl_y_tab.c"
    break;

  case 57:
#line 589 "nidl.y"
                        { (yyval.y_ival) = true; }
#line 2252 "nidl_y_tab.c"
    break;

  case 58:
#line 594 "nidl.y"
        {
            (yyval.y_int_info).int_size = AST_small_integer_k;
            (yyval.y_int_info).int_signed = true;
        }
#line 2261 "nidl_y_tab.c"
    break;

  case 59:
#line 599 "nidl.y"
        {
            (yyval.y_int_info).int_size = AST_short_integer_k;
            (yyval.y_int_info).int_signed = true;
        }
#line 2270 "nidl_y_tab.c"
    break;

  case 60:
#line 604 "nidl.y"
        {
            (yyval.y_int_info).int_size = AST_long_integer_k;
            (yyval.y_int_info).int_signed = true;
        }
#line 2279 "nidl_y_tab.c"
    break;

  case 61:
#line 609 "nidl.y"
        {
            (yyval.y_int_info).int_size = AST_hyper_integer_k;
            (yyval.y_int_info).int_signed = true;
        }
#line 2288 "nidl_y_tab.c"
    break;

  case 62:
#line 617 "nidl.y"
        { (yyval.y_int_info) = (yyvsp[0].y_int_info); }
#line 2294 "nidl_y_tab.c"
    break;

  case 63:
#line 619 "nidl.y"
        {
            (yyval.y_int_info).int_size = (yyvsp[0].y_int_info).int_size;
            (yyval.y_int_info).int_signed = false;
        }
#line 2303 "nidl_y_tab.c"
    break;

  case 64:
#line 624 "nidl.y"
        {
            (yyval.y_int_info).int_size = (yyvsp[-1].y_int_info).int_size;
            (yyval.y_int_info).int_signed = false;
        }
#line 2312 "nidl_y_tab.c"
    break;

  case 65:
#line 632 "nidl.y"
        { (yyval.y_type) = AST_lookup_integer_type_node((yyvsp[0].y_int_info).int_size,(yyvsp[0].y_int_info).int_signed); }
#line 2318 "nidl_y_tab.c"
    break;

  case 66:
#line 634 "nidl.y"
        { (yyval.y_type) = AST_lookup_integer_type_node((yyvsp[-1].y_int_info).int_size,(yyvsp[-1].y_int_info).int_signed); }
#line 2324 "nidl_y_tab.c"
    break;

  case 67:
#line 636 "nidl.y"
        {
            log_warning(yylineno,NIDL_INTSIZEREQ);
            (yyval.y_type) = AST_lookup_integer_type_node(AST_long_integer_k,(yyvsp[-1].y_ival));
        }
#line 2333 "nidl_y_tab.c"
    break;

  case 68:
#line 644 "nidl.y"
        { (yyval.y_type) = AST_lookup_type_node(AST_character_k); }
#line 2339 "nidl_y_tab.c"
    break;

  case 69:
#line 649 "nidl.y"
        { (yyval.y_type) = AST_lookup_type_node(AST_boolean_k); }
#line 2345 "nidl_y_tab.c"
    break;

  case 70:
#line 654 "nidl.y"
        { (yyval.y_type) = AST_lookup_type_node(AST_byte_k); }
#line 2351 "nidl_y_tab.c"
    break;

  case 71:
#line 659 "nidl.y"
        { (yyval.y_type) = AST_lookup_type_node(AST_void_k); }
#line 2357 "nidl_y_tab.c"
    break;

  case 72:
#line 664 "nidl.y"
        { (yyval.y_type) = AST_lookup_type_node(AST_handle_k); }
#line 2363 "nidl_y_tab.c"
    break;

  case 73:
#line 669 "nidl.y"
        {
            NAMETABLE_push_level ();
        }
#line 2371 "nidl_y_tab.c"
    break;

  case 74:
#line 676 "nidl.y"
        {
            ASTP_patch_field_reference ();
            NAMETABLE_pop_level ();
        }
#line 2380 "nidl_y_tab.c"
    break;

  case 75:
#line 684 "nidl.y"
        {
        (yyval.y_type) = AST_disc_union_node(
                         NAMETABLE_NIL_ID,      /* tag name          */
                         NAMETABLE_NIL_ID,      /* union name        */
                         NAMETABLE_NIL_ID,      /* discriminant name */
                         NULL,                  /* discriminant type */
                         (yyvsp[0].y_arm) );           /* the arm list      */
        }
#line 2393 "nidl_y_tab.c"
    break;

  case 76:
#line 694 "nidl.y"
        {
        (yyval.y_type) = AST_disc_union_node(
                         NAMETABLE_NIL_ID,      /* tag name          */
                         ASTP_tagged_union_id,  /* union name        */
                         (yyvsp[-2].y_id),              /* discriminant name */
                         (yyvsp[-3].y_type),            /* discriminant type */
                         (yyvsp[0].y_arm) );           /* the arm list      */
        }
#line 2406 "nidl_y_tab.c"
    break;

  case 77:
#line 703 "nidl.y"
        {
        (yyval.y_type) = AST_disc_union_node(
                         (yyvsp[-1].y_id),              /* tag name          */
                         NAMETABLE_NIL_ID,      /* union name        */
                         NAMETABLE_NIL_ID,      /* discriminant name */
                         NULL,                  /* discriminant type */
                         (yyvsp[0].y_arm) );           /* the arm list      */
        }
#line 2419 "nidl_y_tab.c"
    break;

  case 78:
#line 712 "nidl.y"
        {
        (yyval.y_type) = AST_disc_union_node(
                         NAMETABLE_NIL_ID,      /* tag name          */
                         (yyvsp[-1].y_id),              /* union name        */
                         (yyvsp[-3].y_id),              /* discriminant name */
                         (yyvsp[-4].y_type),            /* discriminant type */
                         (yyvsp[0].y_arm) );           /* the arm list      */
        }
#line 2432 "nidl_y_tab.c"
    break;

  case 79:
#line 721 "nidl.y"
        {
        (yyval.y_type) = AST_disc_union_node(
                         (yyvsp[-6].y_id),              /* tag name          */
                         ASTP_tagged_union_id,  /* union name        */
                         (yyvsp[-2].y_id),              /* discriminant name */
                         (yyvsp[-3].y_type),            /* discriminant type */
                         (yyvsp[0].y_arm) );           /* the arm list      */
        }
#line 2445 "nidl_y_tab.c"
    break;

  case 80:
#line 730 "nidl.y"
        {
        (yyval.y_type) = AST_disc_union_node(
                         (yyvsp[-7].y_id),              /* tag name          */
                         (yyvsp[-1].y_id),              /* union name        */
                         (yyvsp[-3].y_id),              /* discriminant name */
                         (yyvsp[-4].y_type),            /* discriminant type */
                         (yyvsp[0].y_arm) );           /* the arm list      */
        }
#line 2458 "nidl_y_tab.c"
    break;

  case 81:
#line 739 "nidl.y"
        {
            (yyval.y_type) = AST_type_from_tag (AST_disc_union_k, (yyvsp[0].y_id));
        }
#line 2466 "nidl_y_tab.c"
    break;

  case 82:
#line 746 "nidl.y"
        {
                (yyval.y_arm) = (yyvsp[-1].y_arm);
        }
#line 2474 "nidl_y_tab.c"
    break;

  case 83:
#line 752 "nidl.y"
        {
                (yyval.y_arm) = (yyvsp[-1].y_arm);
        }
#line 2482 "nidl_y_tab.c"
    break;

  case 85:
#line 760 "nidl.y"
        {
            (yyval.y_arm) = (AST_arm_n_t *) AST_concat_element(
                                        (ASTP_node_t *) (yyvsp[-2].y_arm),
                                        (ASTP_node_t *) (yyvsp[0].y_arm));
        }
#line 2492 "nidl_y_tab.c"
    break;

  case 87:
#line 769 "nidl.y"
        {
            (yyval.y_arm) = (AST_arm_n_t *) AST_concat_element(
                                        (ASTP_node_t *) (yyvsp[-2].y_arm),
                                        (ASTP_node_t *) (yyvsp[0].y_arm));
        }
#line 2502 "nidl_y_tab.c"
    break;

  case 88:
#line 778 "nidl.y"
        {
            (yyval.y_arm) = (yyvsp[0].y_arm);
        }
#line 2510 "nidl_y_tab.c"
    break;

  case 89:
#line 784 "nidl.y"
        {
            (yyval.y_arm) = AST_label_arm((yyvsp[0].y_arm), (yyvsp[-1].y_label)) ;
        }
#line 2518 "nidl_y_tab.c"
    break;

  case 91:
#line 792 "nidl.y"
        {
            (yyval.y_label) = (AST_case_label_n_t *) AST_concat_element(
                                        (ASTP_node_t *) (yyvsp[-2].y_label),
                                        (ASTP_node_t *) (yyvsp[0].y_label));
        }
#line 2528 "nidl_y_tab.c"
    break;

  case 93:
#line 801 "nidl.y"
        {
            (yyval.y_label) = (AST_case_label_n_t *) AST_concat_element(
                                        (ASTP_node_t *) (yyvsp[-1].y_label),
                                        (ASTP_node_t *) (yyvsp[0].y_label));
        }
#line 2538 "nidl_y_tab.c"
    break;

  case 94:
#line 810 "nidl.y"
        {
            (yyval.y_label) = AST_case_label_node((yyvsp[0].y_constant));
        }
#line 2546 "nidl_y_tab.c"
    break;

  case 95:
#line 816 "nidl.y"
        {
            (yyval.y_label) = AST_case_label_node((yyvsp[-1].y_constant));
        }
#line 2554 "nidl_y_tab.c"
    break;

  case 96:
#line 820 "nidl.y"
        {
            (yyval.y_label) = AST_default_case_label_node();
        }
#line 2562 "nidl_y_tab.c"
    break;

  case 97:
#line 827 "nidl.y"
        {
            (yyval.y_arm) = AST_declarator_to_arm(NULL, NULL, &(yyvsp[-1].y_attributes));
            ASTP_free_simple_list((ASTP_node_t *)(yyvsp[-1].y_attributes).bounds);
        }
#line 2571 "nidl_y_tab.c"
    break;

  case 98:
#line 832 "nidl.y"
        {
            (yyval.y_arm) = AST_declarator_to_arm((yyvsp[-2].y_type),
                                (yyvsp[-1].y_declarator), &(yyvsp[-3].y_attributes));
            ASTP_free_simple_list((ASTP_node_t *)(yyvsp[-3].y_attributes).bounds);
        }
#line 2581 "nidl_y_tab.c"
    break;

  case 99:
#line 840 "nidl.y"
        {
            (yyval.y_arm) = AST_arm_node(NAMETABLE_NIL_ID,NULL,NULL);
        }
#line 2589 "nidl_y_tab.c"
    break;

  case 100:
#line 844 "nidl.y"
        {
            if (ASTP_TEST_ATTR(&(yyvsp[-3].y_attributes), ASTP_CASE))
            {
                ASTP_attr_flag_t attr1 = ASTP_CASE;
                log_error(yylineno, NIDL_EUMEMATTR,
                      KEYWORDS_lookup_text(AST_attribute_to_token(&attr1)));
            }
            if (ASTP_TEST_ATTR(&(yyvsp[-3].y_attributes), ASTP_DEFAULT))
            {
                ASTP_attr_flag_t attr1 = ASTP_DEFAULT;
                log_error(yylineno, NIDL_EUMEMATTR,
                      KEYWORDS_lookup_text(AST_attribute_to_token(&attr1)));
            }
            (yyval.y_arm) = AST_declarator_to_arm((yyvsp[-2].y_type),
                                (yyvsp[-1].y_declarator), &(yyvsp[-3].y_attributes));
            ASTP_free_simple_list((ASTP_node_t *)(yyvsp[-3].y_attributes).bounds);
        }
#line 2611 "nidl_y_tab.c"
    break;

  case 101:
#line 865 "nidl.y"
        {
            (yyval.y_type) = AST_structure_node((yyvsp[-1].y_field), NAMETABLE_NIL_ID) ;
        }
#line 2619 "nidl_y_tab.c"
    break;

  case 102:
#line 869 "nidl.y"
        {
            (yyval.y_type) = AST_structure_node((yyvsp[-1].y_field), (yyvsp[-3].y_id)) ;
        }
#line 2627 "nidl_y_tab.c"
    break;

  case 103:
#line 873 "nidl.y"
        {
            (yyval.y_type) = AST_type_from_tag (AST_structure_k, (yyvsp[0].y_id));
        }
#line 2635 "nidl_y_tab.c"
    break;

  case 105:
#line 881 "nidl.y"
        {
            (yyval.y_field) = (AST_field_n_t *)AST_concat_element(
                                    (ASTP_node_t *) (yyvsp[-2].y_field),
                                    (ASTP_node_t *) (yyvsp[0].y_field)) ;
        }
#line 2645 "nidl_y_tab.c"
    break;

  case 106:
#line 890 "nidl.y"
        {
            (yyval.y_field) = AST_declarators_to_fields((yyvsp[-1].y_declarator),
                                                    (yyvsp[-3].y_type),
                                                    &(yyvsp[-4].y_attributes));
            ASTP_free_simple_list((ASTP_node_t *)(yyvsp[-4].y_attributes).bounds);
        }
#line 2656 "nidl_y_tab.c"
    break;

  case 107:
#line 903 "nidl.y"
        {
             (yyval.y_type) = AST_enumerator_node((yyvsp[0].y_constant), AST_short_integer_k);
        }
#line 2664 "nidl_y_tab.c"
    break;

  case 110:
#line 912 "nidl.y"
        {
            (yyval.y_constant) = (yyvsp[-1].y_constant) ;
        }
#line 2672 "nidl_y_tab.c"
    break;

  case 112:
#line 920 "nidl.y"
        {
            (yyval.y_constant) = (AST_constant_n_t *) AST_concat_element(
                                    (ASTP_node_t *) (yyvsp[-3].y_constant),
                                    (ASTP_node_t *) (yyvsp[0].y_constant)) ;
        }
#line 2682 "nidl_y_tab.c"
    break;

  case 113:
#line 929 "nidl.y"
        {
            (yyval.y_constant)  = AST_enum_constant((yyvsp[-1].y_id)) ;
	    (yyval.y_constant)->value.int_val = (yyvsp[0].y_exp).val.integer;
        }
#line 2691 "nidl_y_tab.c"
    break;

  case 114:
#line 937 "nidl.y"
        { (yyval.y_exp).type = AST_int_const_k; (yyval.y_exp).val.integer = 0; }
#line 2697 "nidl_y_tab.c"
    break;

  case 115:
#line 939 "nidl.y"
        { (yyval.y_exp) = (yyvsp[0].y_exp); }
#line 2703 "nidl_y_tab.c"
    break;

  case 116:
#line 944 "nidl.y"
        {
            (yyval.y_type) = AST_pipe_node ((yyvsp[0].y_type));
        }
#line 2711 "nidl_y_tab.c"
    break;

  case 117:
#line 951 "nidl.y"
        {
	    (yyval.y_declarator) =  (yyvsp[0].y_declarator);
	}
#line 2719 "nidl_y_tab.c"
    break;

  case 118:
#line 955 "nidl.y"
        {
            (yyval.y_declarator) = (ASTP_declarator_n_t *) AST_concat_element(
                                            (ASTP_node_t *) (yyvsp[-3].y_declarator),
                                            (ASTP_node_t *) (yyvsp[0].y_declarator)) ;
        }
#line 2729 "nidl_y_tab.c"
    break;

  case 119:
#line 963 "nidl.y"
            { (yyval.y_declarator) = (yyvsp[0].y_declarator); }
#line 2735 "nidl_y_tab.c"
    break;

  case 120:
#line 967 "nidl.y"
            { 
	        (yyval.y_declarator) = (yyvsp[0].y_declarator); 
	    }
#line 2743 "nidl_y_tab.c"
    break;

  case 121:
#line 971 "nidl.y"
            {
                (yyval.y_declarator) = (yyvsp[0].y_declarator);
                AST_declarator_operation((yyval.y_declarator), AST_pointer_k,
                        (ASTP_node_t *)NULL, (yyvsp[-1].y_ival),
                        0);

	    }
#line 2755 "nidl_y_tab.c"
    break;

  case 122:
#line 979 "nidl.y"
            {
                (yyval.y_declarator) = (yyvsp[0].y_declarator);
                AST_declarator_operation((yyval.y_declarator), AST_pointer_k,
                        (ASTP_node_t *)NULL, (yyvsp[-1].y_ival),0);
            }
#line 2765 "nidl_y_tab.c"
    break;

  case 123:
#line 988 "nidl.y"
            { 
	      (yyval.y_ival) = 1;
	    }
#line 2773 "nidl_y_tab.c"
    break;

  case 124:
#line 992 "nidl.y"
            { (yyval.y_ival) = (yyvsp[0].y_ival) + 1; }
#line 2779 "nidl_y_tab.c"
    break;

  case 125:
#line 996 "nidl.y"
            { (yyval.y_ival) = 0;}
#line 2785 "nidl_y_tab.c"
    break;

  case 126:
#line 1000 "nidl.y"
            { (yyval.y_declarator) = AST_declarator_node ( (yyvsp[0].y_id) ); }
#line 2791 "nidl_y_tab.c"
    break;

  case 127:
#line 1002 "nidl.y"
            {
                (yyval.y_declarator) = (yyval.y_declarator);
                AST_declarator_operation((yyval.y_declarator), AST_array_k,
                        (ASTP_node_t *) (yyvsp[0].y_index), 0, 0 );
            }
#line 2801 "nidl_y_tab.c"
    break;

  case 128:
#line 1008 "nidl.y"
            {
            (yyval.y_declarator) = (yyvsp[-1].y_declarator);
            }
#line 2809 "nidl_y_tab.c"
    break;

  case 129:
#line 1012 "nidl.y"
            {
                (yyval.y_declarator) = (yyval.y_declarator);
                AST_declarator_operation((yyval.y_declarator), AST_function_k,
                        (ASTP_node_t *) (yyvsp[0].y_parameter), 0, 0 );
            }
#line 2819 "nidl_y_tab.c"
    break;

  case 130:
#line 1040 "nidl.y"
        {
            (yyval.y_index) = ASTP_array_index_node ( NULL, ASTP_default_bound,
                                                 NULL, ASTP_open_bound);
        }
#line 2828 "nidl_y_tab.c"
    break;

  case 131:
#line 1045 "nidl.y"
        {
            (yyval.y_index) = ASTP_array_index_node  ( NULL, ASTP_default_bound,
                                                 NULL, ASTP_open_bound);
        }
#line 2837 "nidl_y_tab.c"
    break;

  case 132:
#line 1050 "nidl.y"
        {
            (yyval.y_index) = ASTP_array_index_node  ( NULL, ASTP_default_bound,
                                                 (yyvsp[-1].y_constant), ASTP_constant_bound);
        }
#line 2846 "nidl_y_tab.c"
    break;

  case 133:
#line 1055 "nidl.y"
        {
            (yyval.y_index) = ASTP_array_index_node  ( NULL, ASTP_open_bound,
                                                 NULL, ASTP_open_bound);
        }
#line 2855 "nidl_y_tab.c"
    break;

  case 134:
#line 1060 "nidl.y"
        {
            (yyval.y_index) = ASTP_array_index_node  ( NULL, ASTP_open_bound,
                                                 (yyvsp[-1].y_constant), ASTP_constant_bound);
        }
#line 2864 "nidl_y_tab.c"
    break;

  case 135:
#line 1065 "nidl.y"
        {
            (yyval.y_index) = ASTP_array_index_node  ( (yyvsp[-3].y_constant), ASTP_constant_bound,
                                                 NULL, ASTP_open_bound);
        }
#line 2873 "nidl_y_tab.c"
    break;

  case 136:
#line 1070 "nidl.y"
        {
            (yyval.y_index) = ASTP_array_index_node  ( (yyvsp[-3].y_constant), ASTP_constant_bound,
                                                 (yyvsp[-1].y_constant), ASTP_constant_bound);
        }
#line 2882 "nidl_y_tab.c"
    break;

  case 137:
#line 1079 "nidl.y"
        {
            if (ASTP_parsing_main_idl) {
                (yyval.y_operation) = AST_operation_node (
                                    (yyvsp[-1].y_type),         /*The type node*/
                                    (yyvsp[0].y_declarator),   /* Declarator list */
                                   &(yyvsp[-3].y_attributes));  /* attributes */
              if ((yyvsp[-2].y_ival))
	      {
		AST_SET_STATIC((yyval.y_operation));
		(yyval.y_operation)->cxx_static_name = NULL;
	      }
	    }
            ASTP_free_simple_list((ASTP_node_t *)(yyvsp[-3].y_attributes).bounds);
        }
#line 2901 "nidl_y_tab.c"
    break;

  case 138:
#line 1094 "nidl.y"
        {
        log_error(yylineno,NIDL_MISSONOP);
        (yyval.y_operation) = NULL;
        }
#line 2910 "nidl_y_tab.c"
    break;

  case 139:
#line 1102 "nidl.y"
        {
            (yyval.y_ival) = FALSE;
        }
#line 2918 "nidl_y_tab.c"
    break;

  case 140:
#line 1106 "nidl.y"
        {
            (yyval.y_ival) = TRUE;
        }
#line 2926 "nidl_y_tab.c"
    break;

  case 141:
#line 1113 "nidl.y"
        {
            (yyval.y_parameter) = (yyvsp[-1].y_parameter);
        }
#line 2934 "nidl_y_tab.c"
    break;

  case 142:
#line 1120 "nidl.y"
        {
        NAMETABLE_push_level ();
        }
#line 2942 "nidl_y_tab.c"
    break;

  case 143:
#line 1127 "nidl.y"
        {
        ASTP_patch_field_reference ();
        NAMETABLE_pop_level ();
        }
#line 2951 "nidl_y_tab.c"
    break;

  case 145:
#line 1136 "nidl.y"
        {
            if (ASTP_parsing_main_idl)
                (yyval.y_parameter) = (AST_parameter_n_t *) AST_concat_element(
                                    (ASTP_node_t *) (yyvsp[-3].y_parameter),
                                    (ASTP_node_t *) (yyvsp[0].y_parameter));
        }
#line 2962 "nidl_y_tab.c"
    break;

  case 146:
#line 1143 "nidl.y"
        {
            (yyval.y_parameter) = (AST_parameter_n_t *)NULL;
        }
#line 2970 "nidl_y_tab.c"
    break;

  case 147:
#line 1150 "nidl.y"
        {
            /*
             * We have to use special code here to allow (void) as a parameter
             * specification.  If there are no declarators, then we need to
             * make sure that the type is void and that there are no attributes .
             */
            if ((yyvsp[0].y_declarator) == NULL)
            {
                /*
                 * If the type is not void or some attribute is specified,
                 * there is a syntax error.  Force a yacc error, so that
                 * we can safely recover from the lack of a declarator.
                 */
                if (((yyvsp[-2].y_type)->kind != AST_void_k) ||
                   ((yyvsp[-3].y_attributes).bounds != NULL) ||
                   ((yyvsp[-3].y_attributes).attr_flags != 0))
                {
                    yywhere();  /* Issue a syntax error for this line */
                    YYERROR;    /* Allow natural error recovery */
                }

                (yyval.y_parameter) = (AST_parameter_n_t *)NULL;
            }
            else
            {
                if (ASTP_parsing_main_idl) {
                    (yyval.y_parameter) = AST_declarator_to_param(
                                            &(yyvsp[-3].y_attributes),
                                            (yyvsp[-2].y_type),
                                            (yyvsp[0].y_declarator));
                }
            }
            ASTP_free_simple_list((ASTP_node_t *)(yyvsp[-3].y_attributes).bounds);
        }
#line 3009 "nidl_y_tab.c"
    break;

  case 148:
#line 1185 "nidl.y"
        {
            log_error(yylineno, NIDL_MISSONPARAM);
            (yyval.y_parameter) = (AST_parameter_n_t *)NULL;
        }
#line 3018 "nidl_y_tab.c"
    break;

  case 149:
#line 1193 "nidl.y"
        { (yyval.y_declarator) = (yyvsp[0].y_declarator); }
#line 3024 "nidl_y_tab.c"
    break;

  case 150:
#line 1195 "nidl.y"
        { (yyval.y_declarator) = NULL; }
#line 3030 "nidl_y_tab.c"
    break;

  case 151:
#line 1211 "nidl.y"
        {
            search_attributes_table = true;
        }
#line 3038 "nidl_y_tab.c"
    break;

  case 152:
#line 1218 "nidl.y"
        {
            search_attributes_table = false;
        }
#line 3046 "nidl_y_tab.c"
    break;

  case 153:
#line 1225 "nidl.y"
        {
            search_attributes_table = false;
        }
#line 3054 "nidl_y_tab.c"
    break;

  case 154:
#line 1232 "nidl.y"
        {
            search_attributes_table = true;
        }
#line 3062 "nidl_y_tab.c"
    break;

  case 155:
#line 1244 "nidl.y"
        {
            /* Give an error on notranslated sources */
            if (((yyvsp[0].y_attributes).bounds != NULL) ||
               ((yyvsp[0].y_attributes).attr_flags != 0))
            {
                log_error(yylineno,NIDL_ATTRTRANS);
                ASTP_free_simple_list((ASTP_node_t *)(yyvsp[0].y_attributes).bounds);
            }
        }
#line 3076 "nidl_y_tab.c"
    break;

  case 157:
#line 1265 "nidl.y"
        {
            log_error(yylineno,NIDL_ERRINATTR);
        }
#line 3084 "nidl_y_tab.c"
    break;

  case 162:
#line 1280 "nidl.y"
        {
            log_error(yylineno,NIDL_SYNTAXUUID);
        }
#line 3092 "nidl_y_tab.c"
    break;

  case 163:
#line 1284 "nidl.y"
        {
            {
                if (ASTP_IF_AF_SET(the_interface,ASTP_IF_UUID))
                        log_error(yylineno, NIDL_ATTRUSEMULT);
                ASTP_SET_IF_AF(the_interface,ASTP_IF_UUID);
                the_interface->uuid = (yyvsp[0].y_uuid);
            }
        }
#line 3105 "nidl_y_tab.c"
    break;

  case 164:
#line 1293 "nidl.y"
        {
            if (ASTP_IF_AF_SET(the_interface,ASTP_IF_PORT))
                    log_error(yylineno, NIDL_ATTRUSEMULT);
            ASTP_SET_IF_AF(the_interface,ASTP_IF_PORT);
        }
#line 3115 "nidl_y_tab.c"
    break;

  case 165:
#line 1299 "nidl.y"
        {
            if (ASTP_IF_AF_SET(the_interface, ASTP_IF_EXCEPTIONS))
                log_error(yylineno, NIDL_ATTRUSEMULT);
            ASTP_SET_IF_AF(the_interface, ASTP_IF_EXCEPTIONS);
        }
#line 3125 "nidl_y_tab.c"
    break;

  case 166:
#line 1305 "nidl.y"
        {
            {
                if (ASTP_IF_AF_SET(the_interface,ASTP_IF_VERSION))
                        log_error(yylineno, NIDL_ATTRUSEMULT);
                ASTP_SET_IF_AF(the_interface,ASTP_IF_VERSION);
            }

        }
#line 3138 "nidl_y_tab.c"
    break;

  case 167:
#line 1314 "nidl.y"
        {
            {
                if (AST_LOCAL_SET(the_interface))
                        log_warning(yylineno, NIDL_MULATTRDEF);
                AST_SET_LOCAL(the_interface);
            }
        }
#line 3150 "nidl_y_tab.c"
    break;

  case 168:
#line 1322 "nidl.y"
        {
            if (interface_pointer_class != 0)
                    log_error(yylineno, NIDL_ATTRUSEMULT);
            interface_pointer_class = (yyvsp[-1].y_ival);
        }
#line 3160 "nidl_y_tab.c"
    break;

  case 169:
#line 1331 "nidl.y"
               { (yyval.y_ival) = ASTP_REF; }
#line 3166 "nidl_y_tab.c"
    break;

  case 170:
#line 1332 "nidl.y"
               { (yyval.y_ival) = ASTP_PTR; }
#line 3172 "nidl_y_tab.c"
    break;

  case 171:
#line 1333 "nidl.y"
                  { (yyval.y_ival) = ASTP_UNIQUE; }
#line 3178 "nidl_y_tab.c"
    break;

  case 172:
#line 1338 "nidl.y"
        {
            the_interface->version = (yyvsp[0].y_ival);
            if ((yyvsp[0].y_ival) > (unsigned int)ASTP_C_USHORT_MAX)
                log_error(yylineno, NIDL_MAJORTOOLARGE, ASTP_C_USHORT_MAX);
        }
#line 3188 "nidl_y_tab.c"
    break;

  case 173:
#line 1344 "nidl.y"
        {
            char    *float_text;
            unsigned int            major_version,minor_version;
            STRTAB_str_to_string((yyvsp[0].y_string), &float_text);
            sscanf(float_text,"%d.%d",&major_version,&minor_version);
            if (major_version > (unsigned int)ASTP_C_USHORT_MAX)
                log_error(yylineno, NIDL_MAJORTOOLARGE, ASTP_C_USHORT_MAX);
            if (minor_version > (unsigned int)ASTP_C_USHORT_MAX)
                log_error(yylineno, NIDL_MINORTOOLARGE, ASTP_C_USHORT_MAX);
            the_interface->version = (minor_version * 65536) + major_version;
        }
#line 3204 "nidl_y_tab.c"
    break;

  case 176:
#line 1364 "nidl.y"
        {
            the_interface->exceptions = (yyvsp[0].y_exception);
        }
#line 3212 "nidl_y_tab.c"
    break;

  case 177:
#line 1368 "nidl.y"
        {
            (yyval.y_exception) = (AST_exception_n_t *) AST_concat_element(
                                (ASTP_node_t *) the_interface->exceptions,
                                (ASTP_node_t *) (yyvsp[0].y_exception) );
        }
#line 3222 "nidl_y_tab.c"
    break;

  case 178:
#line 1377 "nidl.y"
        {
            ASTP_parse_port(the_interface,(yyvsp[0].y_string));
        }
#line 3230 "nidl_y_tab.c"
    break;

  case 179:
#line 1384 "nidl.y"
        {
            if (ASTP_parsing_main_idl)
                (yyval.y_exception) = AST_exception_node((yyvsp[0].y_id));
            else
                (yyval.y_exception) = NULL;
        }
#line 3241 "nidl_y_tab.c"
    break;

  case 180:
#line 1402 "nidl.y"
        {
            (yyval.y_attributes).bounds = (yyvsp[-1].y_attributes).bounds;
            (yyval.y_attributes).attr_flags = 0;
        }
#line 3250 "nidl_y_tab.c"
    break;

  case 181:
#line 1407 "nidl.y"
        {
            (yyval.y_attributes).bounds = (yyvsp[-1].y_attributes).bounds;
            (yyval.y_attributes).attr_flags = 0;
        }
#line 3259 "nidl_y_tab.c"
    break;

  case 182:
#line 1415 "nidl.y"
        {
            ASTP_bound_type = first_is_k;
        }
#line 3267 "nidl_y_tab.c"
    break;

  case 183:
#line 1419 "nidl.y"
        {
            ASTP_bound_type = last_is_k;
        }
#line 3275 "nidl_y_tab.c"
    break;

  case 184:
#line 1423 "nidl.y"
        {
            ASTP_bound_type = length_is_k;
        }
#line 3283 "nidl_y_tab.c"
    break;

  case 185:
#line 1427 "nidl.y"
        {
            ASTP_bound_type = max_is_k;
        }
#line 3291 "nidl_y_tab.c"
    break;

  case 186:
#line 1431 "nidl.y"
        {
            ASTP_bound_type = min_is_k;
        }
#line 3299 "nidl_y_tab.c"
    break;

  case 187:
#line 1435 "nidl.y"
        {
            ASTP_bound_type = size_is_k;
        }
#line 3307 "nidl_y_tab.c"
    break;

  case 189:
#line 1444 "nidl.y"
        {
        (yyval.y_attributes).bounds = (ASTP_type_attr_n_t *) AST_concat_element (
                                (ASTP_node_t*) (yyvsp[-2].y_attributes).bounds,
                                (ASTP_node_t*) (yyvsp[0].y_attributes).bounds);
        }
#line 3317 "nidl_y_tab.c"
    break;

  case 190:
#line 1453 "nidl.y"
        {
        (yyval.y_attributes).bounds = AST_array_bound_info ((yyvsp[0].y_id), ASTP_bound_type, FALSE);
        }
#line 3325 "nidl_y_tab.c"
    break;

  case 191:
#line 1457 "nidl.y"
        {
        (yyval.y_attributes).bounds = AST_array_bound_info ((yyvsp[0].y_id), ASTP_bound_type, TRUE);
        }
#line 3333 "nidl_y_tab.c"
    break;

  case 192:
#line 1461 "nidl.y"
        {
        (yyval.y_attributes).bounds = AST_array_bound_info ((yyvsp[0].y_id), ASTP_bound_type, TRUE);
        }
#line 3341 "nidl_y_tab.c"
    break;

  case 193:
#line 1465 "nidl.y"
        {
        (yyval.y_attributes).bounds = AST_array_bound_info (NAMETABLE_NIL_ID, ASTP_bound_type, FALSE);
        }
#line 3349 "nidl_y_tab.c"
    break;

  case 194:
#line 1472 "nidl.y"
        {
            ASTP_bound_type = switch_is_k;
        }
#line 3357 "nidl_y_tab.c"
    break;

  case 195:
#line 1479 "nidl.y"
        {
        (yyval.y_attributes).bounds = AST_array_bound_info ((yyvsp[0].y_id), ASTP_bound_type, FALSE);
        }
#line 3365 "nidl_y_tab.c"
    break;

  case 196:
#line 1483 "nidl.y"
        {
        (yyval.y_attributes).bounds = AST_array_bound_info ((yyvsp[0].y_id), ASTP_bound_type, TRUE);
        }
#line 3373 "nidl_y_tab.c"
    break;

  case 197:
#line 1493 "nidl.y"
        { (yyval.y_attributes) = (yyvsp[0].y_attributes); }
#line 3379 "nidl_y_tab.c"
    break;

  case 198:
#line 1496 "nidl.y"
        {
        (yyval.y_attributes).bounds = NULL;
        (yyval.y_attributes).attr_flags = 0;
        }
#line 3388 "nidl_y_tab.c"
    break;

  case 200:
#line 1506 "nidl.y"
        {
        /*
         * Can't tell if we had any valid attributes in the list, so return
         * none.
         */
        (yyval.y_attributes).bounds = NULL;
        (yyval.y_attributes).attr_flags = 0;
        log_error(yylineno, NIDL_ERRINATTR);
        }
#line 3402 "nidl_y_tab.c"
    break;

  case 201:
#line 1516 "nidl.y"
        {
        /*
         * No closer to the attribute, so give a different message.
         */
        (yyval.y_attributes).bounds = NULL;
        (yyval.y_attributes).attr_flags = 0;
        log_error(yylineno, NIDL_MISSONATTR);
        search_attributes_table = false;
        }
#line 3416 "nidl_y_tab.c"
    break;

  case 202:
#line 1530 "nidl.y"
        { (yyval.y_attributes) = (yyvsp[0].y_attributes); }
#line 3422 "nidl_y_tab.c"
    break;

  case 203:
#line 1533 "nidl.y"
        {
          /*
           * If the same bit has been specified more than once, then issue
           * a message.
           */
          if (((yyvsp[-3].y_attributes).attr_flags & (yyvsp[0].y_attributes).attr_flags) != 0)
                log_warning(yylineno, NIDL_MULATTRDEF);
          (yyval.y_attributes).attr_flags = (yyvsp[-3].y_attributes).attr_flags |
                                        (yyvsp[0].y_attributes).attr_flags;
          (yyval.y_attributes).bounds = (ASTP_type_attr_n_t *) AST_concat_element (
                                (ASTP_node_t*) (yyvsp[-3].y_attributes).bounds,
                                (ASTP_node_t*) (yyvsp[0].y_attributes).bounds);
        }
#line 3440 "nidl_y_tab.c"
    break;

  case 204:
#line 1551 "nidl.y"
                                { (yyval.y_attributes) = (yyvsp[0].y_attributes); }
#line 3446 "nidl_y_tab.c"
    break;

  case 205:
#line 1554 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_BROADCAST;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3453 "nidl_y_tab.c"
    break;

  case 206:
#line 1556 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_MAYBE;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3460 "nidl_y_tab.c"
    break;

  case 207:
#line 1558 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_IDEMPOTENT;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3467 "nidl_y_tab.c"
    break;

  case 208:
#line 1560 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_REFLECT_DELETIONS;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3474 "nidl_y_tab.c"
    break;

  case 209:
#line 1565 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_PTR;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3481 "nidl_y_tab.c"
    break;

  case 210:
#line 1567 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_IN;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3488 "nidl_y_tab.c"
    break;

  case 211:
#line 1570 "nidl.y"
                                { (yyval.y_attributes).attr_flags =
                                        ASTP_IN | ASTP_IN_SHAPE;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3496 "nidl_y_tab.c"
    break;

  case 212:
#line 1573 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_OUT;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3503 "nidl_y_tab.c"
    break;

  case 213:
#line 1576 "nidl.y"
                                { (yyval.y_attributes).attr_flags =
                                        ASTP_OUT | ASTP_OUT_SHAPE;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3511 "nidl_y_tab.c"
    break;

  case 214:
#line 1581 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_SMALL;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3518 "nidl_y_tab.c"
    break;

  case 215:
#line 1583 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_STRING;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3525 "nidl_y_tab.c"
    break;

  case 216:
#line 1585 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_STRING0;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3532 "nidl_y_tab.c"
    break;

  case 217:
#line 1587 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_UNIQUE;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3539 "nidl_y_tab.c"
    break;

  case 218:
#line 1589 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_REF;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3546 "nidl_y_tab.c"
    break;

  case 219:
#line 1591 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_IGNORE;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3553 "nidl_y_tab.c"
    break;

  case 220:
#line 1593 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_CONTEXT;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3560 "nidl_y_tab.c"
    break;

  case 221:
#line 1597 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_UNALIGN;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3567 "nidl_y_tab.c"
    break;

  case 222:
#line 1599 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_V1_ENUM;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3574 "nidl_y_tab.c"
    break;

  case 223:
#line 1602 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_ALIGN_SMALL;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3581 "nidl_y_tab.c"
    break;

  case 224:
#line 1605 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_ALIGN_SHORT;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3588 "nidl_y_tab.c"
    break;

  case 225:
#line 1608 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_ALIGN_LONG;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3595 "nidl_y_tab.c"
    break;

  case 226:
#line 1611 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_ALIGN_HYPER;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3602 "nidl_y_tab.c"
    break;

  case 227:
#line 1613 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_HANDLE;
                                  (yyval.y_attributes).bounds = NULL;       }
#line 3609 "nidl_y_tab.c"
    break;

  case 228:
#line 1616 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_TRANSMIT_AS;
                                  (yyval.y_attributes).bounds = NULL;
                                  ASTP_transmit_as_type = (yyvsp[-1].y_type);
                                }
#line 3618 "nidl_y_tab.c"
    break;

  case 229:
#line 1621 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_SWITCH_TYPE;
                                  (yyval.y_attributes).bounds = NULL;
                                  ASTP_switch_type = (yyvsp[-1].y_type);
                                }
#line 3627 "nidl_y_tab.c"
    break;

  case 230:
#line 1628 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_CASE;
                                  (yyval.y_attributes).bounds = NULL;
                                  ASTP_case = (yyvsp[-1].y_label);
                                }
#line 3636 "nidl_y_tab.c"
    break;

  case 231:
#line 1632 "nidl.y"
                                { (yyval.y_attributes).attr_flags = ASTP_DEFAULT;
                                  (yyval.y_attributes).bounds = NULL;       
                                }
#line 3644 "nidl_y_tab.c"
    break;

  case 232:
#line 1636 "nidl.y"
        {
                char *identifier;       /* place to receive the identifier text */
                NAMETABLE_id_to_string ((yyvsp[0].y_id), &identifier);
                log_error (yylineno, NIDL_UNKNOWNATTR, identifier);
                (yyval.y_attributes).attr_flags = 0;
                (yyval.y_attributes).bounds = NULL;
        }
#line 3656 "nidl_y_tab.c"
    break;

  case 233:
#line 1652 "nidl.y"
        {(yyval.y_exp) = (yyvsp[0].y_exp);}
#line 3662 "nidl_y_tab.c"
    break;

  case 234:
#line 1657 "nidl.y"
        {(yyval.y_exp) = (yyvsp[0].y_exp);}
#line 3668 "nidl_y_tab.c"
    break;

  case 235:
#line 1659 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-4].y_exp));
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-4].y_exp).val.integer ? (yyvsp[-2].y_exp).val.integer : (yyvsp[0].y_exp).val.integer;
        }
#line 3680 "nidl_y_tab.c"
    break;

  case 236:
#line 1670 "nidl.y"
        {(yyval.y_exp) = (yyvsp[0].y_exp);}
#line 3686 "nidl_y_tab.c"
    break;

  case 237:
#line 1672 "nidl.y"
        {
           ASTP_validate_integer(&(yyvsp[-2].y_exp));
           ASTP_validate_integer(&(yyvsp[0].y_exp));
           (yyval.y_exp).type = AST_int_const_k;
           (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer || (yyvsp[0].y_exp).val.integer;
        }
#line 3697 "nidl_y_tab.c"
    break;

  case 238:
#line 1682 "nidl.y"
        {(yyval.y_exp) = (yyvsp[0].y_exp);}
#line 3703 "nidl_y_tab.c"
    break;

  case 239:
#line 1684 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-1].y_exp).val.integer && (yyvsp[0].y_exp).val.integer;
        }
#line 3714 "nidl_y_tab.c"
    break;

  case 240:
#line 1694 "nidl.y"
        {(yyval.y_exp) = (yyvsp[0].y_exp);}
#line 3720 "nidl_y_tab.c"
    break;

  case 241:
#line 1696 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer | (yyvsp[0].y_exp).val.integer;
        }
#line 3731 "nidl_y_tab.c"
    break;

  case 242:
#line 1706 "nidl.y"
        {(yyval.y_exp) = (yyvsp[0].y_exp);}
#line 3737 "nidl_y_tab.c"
    break;

  case 243:
#line 1708 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer ^ (yyvsp[0].y_exp).val.integer;
        }
#line 3748 "nidl_y_tab.c"
    break;

  case 244:
#line 1718 "nidl.y"
        {(yyval.y_exp) = (yyvsp[0].y_exp);}
#line 3754 "nidl_y_tab.c"
    break;

  case 245:
#line 1720 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer & (yyvsp[0].y_exp).val.integer;
        }
#line 3765 "nidl_y_tab.c"
    break;

  case 246:
#line 1730 "nidl.y"
        {(yyval.y_exp) = (yyvsp[0].y_exp);}
#line 3771 "nidl_y_tab.c"
    break;

  case 247:
#line 1732 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer == (yyvsp[0].y_exp).val.integer;
        }
#line 3782 "nidl_y_tab.c"
    break;

  case 248:
#line 1739 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer != (yyvsp[0].y_exp).val.integer;
        }
#line 3793 "nidl_y_tab.c"
    break;

  case 249:
#line 1749 "nidl.y"
        {(yyval.y_exp) = (yyvsp[0].y_exp);}
#line 3799 "nidl_y_tab.c"
    break;

  case 250:
#line 1751 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer < (yyvsp[0].y_exp).val.integer;
        }
#line 3810 "nidl_y_tab.c"
    break;

  case 251:
#line 1758 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer > (yyvsp[0].y_exp).val.integer;
        }
#line 3821 "nidl_y_tab.c"
    break;

  case 252:
#line 1765 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer <= (yyvsp[0].y_exp).val.integer;
        }
#line 3832 "nidl_y_tab.c"
    break;

  case 253:
#line 1772 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer >= (yyvsp[0].y_exp).val.integer;
        }
#line 3843 "nidl_y_tab.c"
    break;

  case 254:
#line 1782 "nidl.y"
        {(yyval.y_exp) = (yyvsp[0].y_exp);}
#line 3849 "nidl_y_tab.c"
    break;

  case 255:
#line 1784 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer << (yyvsp[0].y_exp).val.integer;
        }
#line 3860 "nidl_y_tab.c"
    break;

  case 256:
#line 1791 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer >> (yyvsp[0].y_exp).val.integer;
        }
#line 3871 "nidl_y_tab.c"
    break;

  case 257:
#line 1801 "nidl.y"
        {(yyval.y_exp) = (yyvsp[0].y_exp);}
#line 3877 "nidl_y_tab.c"
    break;

  case 258:
#line 1803 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer + (yyvsp[0].y_exp).val.integer;
            if (((yyval.y_exp).val.integer < (yyvsp[-2].y_exp).val.integer) &&
                ((yyval.y_exp).val.integer < (yyvsp[0].y_exp).val.integer))
                log_error (yylineno, NIDL_INTOVERFLOW, KEYWORDS_lookup_text(LONG_KW));
        }
#line 3891 "nidl_y_tab.c"
    break;

  case 259:
#line 1813 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer - (yyvsp[0].y_exp).val.integer;
        }
#line 3902 "nidl_y_tab.c"
    break;

  case 260:
#line 1823 "nidl.y"
        {(yyval.y_exp) = (yyvsp[0].y_exp);}
#line 3908 "nidl_y_tab.c"
    break;

  case 261:
#line 1825 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer * (yyvsp[0].y_exp).val.integer;
            if (((yyval.y_exp).val.integer < (yyvsp[-2].y_exp).val.integer) &&
                ((yyval.y_exp).val.integer < (yyvsp[0].y_exp).val.integer))
                log_error (yylineno, NIDL_INTOVERFLOW, KEYWORDS_lookup_text(LONG_KW));
        }
#line 3922 "nidl_y_tab.c"
    break;

  case 262:
#line 1835 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            if ((yyvsp[0].y_exp).val.integer != 0)
                (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer / (yyvsp[0].y_exp).val.integer;
            else
                log_error (yylineno, NIDL_INTDIVBY0);
        }
#line 3936 "nidl_y_tab.c"
    break;

  case 263:
#line 1845 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[-2].y_exp));
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            if ((yyvsp[0].y_exp).val.integer != 0)
                (yyval.y_exp).val.integer = (yyvsp[-2].y_exp).val.integer % (yyvsp[0].y_exp).val.integer;
            else
                log_error (yylineno, NIDL_INTDIVBY0);
        }
#line 3950 "nidl_y_tab.c"
    break;

  case 264:
#line 1857 "nidl.y"
        {(yyval.y_exp) = (yyvsp[0].y_exp);}
#line 3956 "nidl_y_tab.c"
    break;

  case 265:
#line 1862 "nidl.y"
        {(yyval.y_exp) = (yyvsp[0].y_exp);}
#line 3962 "nidl_y_tab.c"
    break;

  case 266:
#line 1864 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[0].y_exp).val.integer;
        }
#line 3972 "nidl_y_tab.c"
    break;

  case 267:
#line 1870 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = -(yyvsp[0].y_exp).val.integer;
        }
#line 3982 "nidl_y_tab.c"
    break;

  case 268:
#line 1876 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = ~(yyvsp[0].y_exp).val.integer;
        }
#line 3992 "nidl_y_tab.c"
    break;

  case 269:
#line 1882 "nidl.y"
        {
            ASTP_validate_integer(&(yyvsp[0].y_exp));
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = !(yyvsp[0].y_exp).val.integer;
        }
#line 4002 "nidl_y_tab.c"
    break;

  case 270:
#line 1891 "nidl.y"
        { (yyval.y_exp) = (yyvsp[-1].y_exp); }
#line 4008 "nidl_y_tab.c"
    break;

  case 271:
#line 1893 "nidl.y"
        {
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = (yyvsp[0].y_ival);
        }
#line 4017 "nidl_y_tab.c"
    break;

  case 272:
#line 1898 "nidl.y"
        {
            (yyval.y_exp).type = AST_nil_const_k;
            (yyval.y_exp).val.other = AST_char_constant ((yyvsp[0].y_char)) ;
        }
#line 4026 "nidl_y_tab.c"
    break;

  case 273:
#line 1903 "nidl.y"
        {
            (yyval.y_exp).type = AST_nil_const_k;
            (yyval.y_exp).val.other  = AST_named_constant((yyvsp[0].y_id)) ;
        }
#line 4035 "nidl_y_tab.c"
    break;

  case 274:
#line 1908 "nidl.y"
        {
            (yyval.y_exp).type = AST_nil_const_k;
            (yyval.y_exp).val.other = AST_string_constant ((yyvsp[0].y_string)) ;
        }
#line 4044 "nidl_y_tab.c"
    break;

  case 275:
#line 1913 "nidl.y"
        {
            (yyval.y_exp).type = AST_nil_const_k;
            (yyval.y_exp).val.other = AST_null_constant() ;
        }
#line 4053 "nidl_y_tab.c"
    break;

  case 276:
#line 1919 "nidl.y"
        {
            (yyval.y_exp).type = AST_nil_const_k;
            (yyval.y_exp).val.other = AST_boolean_constant(true) ;
        }
#line 4062 "nidl_y_tab.c"
    break;

  case 277:
#line 1925 "nidl.y"
        {
            (yyval.y_exp).type = AST_nil_const_k;
            (yyval.y_exp).val.other = AST_boolean_constant(false) ;
        }
#line 4071 "nidl_y_tab.c"
    break;

  case 278:
#line 1930 "nidl.y"
        {
            (yyval.y_exp).type = AST_int_const_k;
            (yyval.y_exp).val.integer = 0;
            log_error(yylineno, NIDL_FLOATCONSTNOSUP);
        }
#line 4081 "nidl_y_tab.c"
    break;


#line 4085 "nidl_y_tab.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = YY_CAST (char *, YYSTACK_ALLOC (YY_CAST (YYSIZE_T, yymsg_alloc)));
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[+*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 1937 "nidl.y"

