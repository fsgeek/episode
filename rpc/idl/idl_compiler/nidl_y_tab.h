/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

#ifndef YY_YY_NIDL_Y_TAB_H_INCLUDED
# define YY_YY_NIDL_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    ALIGN_KW = 258,
    BYTE_KW = 259,
    CHAR_KW = 260,
    CONST_KW = 261,
    DEFAULT_KW = 262,
    ENUM_KW = 263,
    EXCEPTIONS_KW = 264,
    FLOAT_KW = 265,
    HYPER_KW = 266,
    INT_KW = 267,
    INTERFACE_KW = 268,
    IMPORT_KW = 269,
    LONG_KW = 270,
    PIPE_KW = 271,
    REF_KW = 272,
    SMALL_KW = 273,
    STRUCT_KW = 274,
    TYPEDEF_KW = 275,
    UNION_KW = 276,
    UNSIGNED_KW = 277,
    SHORT_KW = 278,
    VOID_KW = 279,
    DOUBLE_KW = 280,
    BOOLEAN_KW = 281,
    CASE_KW = 282,
    SWITCH_KW = 283,
    HANDLE_T_KW = 284,
    TRUE_KW = 285,
    FALSE_KW = 286,
    NULL_KW = 287,
    BROADCAST_KW = 288,
    COMM_STATUS_KW = 289,
    CONTEXT_HANDLE_KW = 290,
    FIRST_IS_KW = 291,
    HANDLE_KW = 292,
    IDEMPOTENT_KW = 293,
    IGNORE_KW = 294,
    IN_KW = 295,
    LAST_IS_KW = 296,
    LENGTH_IS_KW = 297,
    LOCAL_KW = 298,
    MAX_IS_KW = 299,
    MAYBE_KW = 300,
    CXX_LOOKUP_KW = 301,
    CLIENT_MEMORY_KW = 302,
    MIN_IS_KW = 303,
    MUTABLE_KW = 304,
    OUT_KW = 305,
    POINTER_DEFAULT_KW = 306,
    ENDPOINT_KW = 307,
    PTR_KW = 308,
    REFLECT_DELETIONS_KW = 309,
    REMOTE_KW = 310,
    SECURE_KW = 311,
    SHAPE_KW = 312,
    SIZE_IS_KW = 313,
    STRING_KW = 314,
    SWITCH_IS_KW = 315,
    SWITCH_TYPE_KW = 316,
    TRANSMIT_AS_KW = 317,
    UNIQUE_KW = 318,
    UUID_KW = 319,
    VERSION_KW = 320,
    V1_ARRAY_KW = 321,
    V1_STRING_KW = 322,
    V1_ENUM_KW = 323,
    V1_STRUCT_KW = 324,
    STATIC_KW = 325,
    UUID_REP = 326,
    COLON = 327,
    COMMA = 328,
    DOTDOT = 329,
    EQUAL = 330,
    LBRACE = 331,
    LBRACKET = 332,
    LPAREN = 333,
    RBRACE = 334,
    RBRACKET = 335,
    RPAREN = 336,
    SEMI = 337,
    STAR = 338,
    QUESTION = 339,
    BAR = 340,
    BARBAR = 341,
    LANGLE = 342,
    LANGLEANGLE = 343,
    RANGLE = 344,
    RANGLEANGLE = 345,
    AMP = 346,
    AMPAMP = 347,
    LESSEQUAL = 348,
    GREATEREQUAL = 349,
    EQUALEQUAL = 350,
    CARET = 351,
    PLUS = 352,
    MINUS = 353,
    NOT = 354,
    NOTEQUAL = 355,
    SLASH = 356,
    PERCENT = 357,
    TILDE = 358,
    POUND = 359,
    UNKNOWN = 360,
    IDENTIFIER = 361,
    STRING = 362,
    INTEGER_NUMERIC = 363,
    CHAR = 364,
    FLOAT_NUMERIC = 365
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 127 "nidl.y"

    NAMETABLE_id_t         y_id ;          /* Identifier           */
    STRTAB_str_t           y_string ;      /* String               */
    STRTAB_str_t           y_float ;       /* Float constant       */
    long                   y_ival ;        /* Integer constant     */
    AST_export_n_t*        y_export ;      /* an export node       */
    AST_import_n_t*        y_import ;      /* Import node          */
    AST_exception_n_t*     y_exception ;   /* Exception node       */
    AST_constant_n_t*      y_constant;     /* Constant node        */
    AST_parameter_n_t*     y_parameter ;   /* Parameter node       */
    AST_type_n_t*          y_type ;        /* Type node            */
    AST_type_p_n_t*        y_type_ptr ;    /* Type pointer node    */
    AST_field_n_t*         y_field ;       /* Field node           */
    AST_arm_n_t*           y_arm ;         /* Union variant arm    */
    AST_operation_n_t*     y_operation ;   /* Routine node         */
    AST_interface_n_t*     y_interface ;   /* Interface node       */
    AST_case_label_n_t*    y_label ;       /* Union tags           */
    ASTP_declarator_n_t*   y_declarator ;  /* Declarator info      */
    ASTP_array_index_n_t*  y_index ;       /* Array index info     */
    nidl_uuid_t            y_uuid ;        /* Universal UID        */
    char                   y_char;         /* character constant   */
    ASTP_attributes_t      y_attributes;   /* attributes flags     */
    struct {
        AST_type_k_t    int_size;
        int             int_signed;
        }                  y_int_info;     /* int size and signedness */
    ASTP_exp_n_t           y_exp;          /* constant expression info */

#line 197 "nidl_y_tab.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_NIDL_Y_TAB_H_INCLUDED  */
