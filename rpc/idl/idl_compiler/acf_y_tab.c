/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.5.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 133 "acf.y"

/*----------------------*
 *  yacc included code  *
 *----------------------*/

/* Declarations in this section are copied from yacc source to y_tab.c. */

#include <acf.h>                /* ACF include file - keep first! */
#include <nidl.h>               /* IDL compiler-wide defs */

#include <ast.h>                /* Abstract Syntax Tree defs */
#include <astp.h>               /* Import AST processing routine defs */
#include <command.h>            /* Command line defs */
#include <message.h>            /* Error message defs */
#include <nidlmsg.h>            /* Error message IDs */
#include <files.h>
#include <propagat.h>
#include <checker.h>

extern AST_interface_n_t *the_interface;    /* Ptr to AST interface node */
extern boolean ASTP_parsing_main_idl;       /* True when parsing main IDL */

typedef union                   /* Attributes bitmask */
{
    struct
    {
        unsigned auto_handle    : 1;
        unsigned binding_callout: 1;
        unsigned code           : 1;
        unsigned comm_status    : 1;
        unsigned cs_char        : 1;
        unsigned cs_drtag       : 1;
        unsigned cs_rtag        : 1;
        unsigned cs_stag        : 1;
        unsigned cs_tag_rtn     : 1;
        unsigned decode         : 1;
        unsigned enable_allocate: 1;
        unsigned encode         : 1;
        unsigned explicit_handle: 1;
        unsigned extern_exceps  : 1;
        unsigned fault_status   : 1;
        unsigned heap           : 1;
        unsigned implicit_handle: 1;
        unsigned in_line        : 1;
        unsigned nocode         : 1;
        unsigned out_of_line    : 1;
        unsigned represent_as   : 1;
        unsigned cxx_new	  : 1;
        unsigned cxx_delegate	  : 1;
        unsigned cxx_static	  : 1;
        unsigned lookup_callout	  : 1;
        unsigned malloc_callout_name : 1;
        unsigned free_callout_name   : 1;
    }   bit;
    long    mask;
}   acf_attrib_t;

typedef struct acf_param_t      /* ACF parameter info structure */
{
    struct acf_param_t *next;                   /* Forward link */
    acf_attrib_t    parameter_attr;             /* Parameter attributes */
    NAMETABLE_id_t  param_id;                   /* Parameter name */
}   acf_param_t;


static acf_attrib_t interface_attr,     /* Interface attributes */
                    type_attr,          /* Type attributes */
                    operation_attr,     /* Operation attributes */
                    parameter_attr;     /* Parameter attributes */

static char     *interface_name,        /* Interface name */
                *impl_name,             /* Implicit handle name */
                *type_name,             /* Current type name */
                *repr_type_name,        /* Current represent_as type */
                *cs_char_type_name,     /* Current cs_char type */
                *operation_name,        /* Current operation name */
                *cs_tag_rtn_name,       /* Current cs_tag_rtn name */
                *binding_callout_name,  /* Current binding_callout name */
                *lookup_callout_name,   /* Current cxx_lookup name */
                *malloc_callout_name,   /* Current malloc name */
                *free_callout_name,     /* Current free name */
                *cxx_static_name,       /* Current cxx_static name */
                *cxx_delegate_name,     /* Current cxx_delegate name */
                *cxx_new_name;          /* Current cxx_new name */


static boolean  named_type;             /* True if parsed type is named type */

static AST_include_n_t  *include_list,  /* List of AST include nodes */
                        *include_p;     /* Ptr to a created include node */


static acf_param_t  *parameter_list,        /* Param list for curr. operation */
                    *parameter_free_list;   /* List of available acf_param_t */
static boolean      parameter_attr_list;    /* True if param attrs specified */

static boolean      *cmd_opt;       /* Array of command option flags */
static void         **cmd_val;      /* Array of command option values */
static NAMETABLE_id_t cxx_inherit;

#line 171 "acf_y_tab.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_ACF_Y_TAB_H_INCLUDED
# define YY_YY_ACF_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    LONG_KW = 258,
    AUTO_HANDLE_KW = 259,
    BINDING_CALLOUT_KW = 260,
    CODE_KW = 261,
    COMM_STATUS_KW = 262,
    CS_CHAR_KW = 263,
    CS_TAG_RTN_KW = 264,
    CXX_STATIC_KW = 265,
    ENABLE_ALLOCATE_KW = 266,
    EXPLICIT_HANDLE_KW = 267,
    EXTERN_EXCEPS_KW = 268,
    FAULT_STATUS_KW = 269,
    HANDLE_T_KW = 270,
    HEAP_KW = 271,
    IMPLICIT_HANDLE_KW = 272,
    INCLUDE_KW = 273,
    SSTUB_KW = 274,
    CSTUB_KW = 275,
    INTERFACE_KW = 276,
    IN_LINE_KW = 277,
    NOCODE_KW = 278,
    OUT_OF_LINE_KW = 279,
    REPRESENT_AS_KW = 280,
    TYPEDEF_KW = 281,
    CXX_LOOKUP_KW = 282,
    CLIENT_MEMORY_KW = 283,
    CXX_DELEGATE_KW = 284,
    CXX_NEW_KW = 285,
    COMMA = 286,
    LBRACE = 287,
    LBRACKET = 288,
    LPAREN = 289,
    RBRACE = 290,
    RBRACKET = 291,
    RPAREN = 292,
    SEMI = 293,
    TILDE = 294,
    UNKNOWN = 295,
    IDENTIFIER = 296,
    CXX_IDENTIFIER = 297,
    STRING = 298,
    INTEGER_NUMERIC = 299
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 244 "acf.y"

    NAMETABLE_id_t  y_id;       /* Identifier */
    STRTAB_str_t    y_string;   /* Text string */
    long      	    y_ival ;        /* Integer constant     */
#ifdef SNI_SVR4
    STRTAB_str_t           y_float ;       /* Float constant       */
    AST_export_n_t*        y_export ;      /* an export node       */
    AST_import_n_t*        y_import ;      /* Import node          */
    AST_constant_n_t*      y_constant;     /* Constant node        */
    AST_parameter_n_t*     y_parameter ;   /* Parameter node       */
    AST_type_n_t*          y_type ;        /* Type node            */
    AST_type_p_n_t*        y_type_ptr ;    /* Type pointer node    */
    AST_field_n_t*         y_field ;       /* Field node           */
    AST_arm_n_t*           y_arm ;         /* Union variant arm    */
    AST_operation_n_t*     y_operation ;   /* Routine node         */
    AST_interface_n_t*     y_interface ;   /* Interface node       */
    AST_case_label_n_t*    y_label ;       /* Union tags           */
    ASTP_declarator_n_t*   y_declarator ;  /* Declarator info      */
    ASTP_array_index_n_t*  y_index ;       /* Array index info     */
    nidl_uuid_t            y_uuid ;        /* Universal UID        */
    char                   y_char;         /* character constant   */
    ASTP_attributes_t      y_attributes;   /* attributes flags     */
    struct {
        AST_type_k_t    int_size;
        int             int_signed;
        }                  y_int_info;     /* int size and signedness */
    ASTP_exp_n_t           y_exp;          /* constant expression info */
#endif /*SNI_SVR4*/

#line 298 "acf_y_tab.c"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_ACF_Y_TAB_H_INCLUDED  */



#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))

/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  34
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   190

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  45
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  68
/* YYNRULES -- Number of rules.  */
#define YYNRULES  131
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  211

#define YYUNDEFTOK  2
#define YYMAXUTOK   299


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   342,   342,   346,   479,   480,   484,   485,   489,   495,
     501,   507,   513,   519,   525,   531,   537,   543,   549,   555,
     564,   570,   590,   594,   598,   602,   610,   614,   621,   622,
     638,   639,   643,   653,   660,   661,   662,   664,   669,   670,
     674,   675,   676,   677,   689,   697,   705,   713,   725,   737,
     742,   743,   747,   790,   792,   801,   805,   812,   862,   863,
     867,   868,   872,   879,   880,   884,   890,   896,   902,   908,
     917,   921,   928,   932,   939,   950,   951,   955,  1123,  1124,
    1128,  1129,  1133,  1139,  1145,  1151,  1157,  1163,  1169,  1175,
    1181,  1187,  1207,  1211,  1218,  1222,  1229,  1233,  1240,  1244,
    1251,  1259,  1263,  1270,  1274,  1281,  1285,  1292,  1293,  1297,
    1298,  1302,  1335,  1340,  1346,  1347,  1351,  1357,  1363,  1369,
    1375,  1381,  1406,  1407,  1408,  1409,  1410,  1411,  1412,  1413,
    1414,  1415
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "LONG_KW", "AUTO_HANDLE_KW",
  "BINDING_CALLOUT_KW", "CODE_KW", "COMM_STATUS_KW", "CS_CHAR_KW",
  "CS_TAG_RTN_KW", "CXX_STATIC_KW", "ENABLE_ALLOCATE_KW",
  "EXPLICIT_HANDLE_KW", "EXTERN_EXCEPS_KW", "FAULT_STATUS_KW",
  "HANDLE_T_KW", "HEAP_KW", "IMPLICIT_HANDLE_KW", "INCLUDE_KW", "SSTUB_KW",
  "CSTUB_KW", "INTERFACE_KW", "IN_LINE_KW", "NOCODE_KW", "OUT_OF_LINE_KW",
  "REPRESENT_AS_KW", "TYPEDEF_KW", "CXX_LOOKUP_KW", "CLIENT_MEMORY_KW",
  "CXX_DELEGATE_KW", "CXX_NEW_KW", "COMMA", "LBRACE", "LBRACKET", "LPAREN",
  "RBRACE", "RBRACKET", "RPAREN", "SEMI", "TILDE", "UNKNOWN", "IDENTIFIER",
  "CXX_IDENTIFIER", "STRING", "INTEGER_NUMERIC", "$accept",
  "acf_interface", "acf_interface_header", "acf_interface_attr_list",
  "acf_interface_attrs", "acf_interface_attr", "acf_implicit_handle_attr",
  "acf_implicit_handle", "acf_impl_type", "acf_handle_type",
  "acf_impl_name", "acf_extern_exceps_attr", "acf_ext_excep_list",
  "acf_ext_excep", "acf_interface_name", "acf_interface_body",
  "acf_body_elements", "acf_body_element", "acf_include",
  "acf_include_list", "acf_include_name", "acf_type_declaration",
  "acf_named_type_list", "acf_named_type", "acf_type_attr_list",
  "acf_rest_of_attr_list", "acf_type_attrs", "acf_type_attr",
  "acf_represent_attr", "acf_repr_type", "acf_cs_char_attr",
  "acf_cs_char_type", "acf_operation_declaration", "acf_operations",
  "acf_operation", "acf_op_attr_list", "acf_op_attrs", "acf_op_attr",
  "acf_cxx_new_attr", "acf_cxx_new_name", "acf_binding_callout_attr",
  "acf_binding_callout_name", "acf_lookup_callout_attr",
  "acf_lookup_callout_name", "acf_client_memory_callout_attr",
  "acf_client_memory_malloc_callout_name",
  "acf_client_memory_free_callout_name", "acf_delegate_attr",
  "acf_delegate_name", "acf_cs_tag_rtn_attr", "acf_cs_tag_rtn_name",
  "acf_cxx_static_attr", "acf_parameter_list", "acf_parameters",
  "acf_parameter", "acf_param_attr_list", "acf_param_attrs",
  "acf_param_attr", "acf_auto_handle_attr", "acf_code_attr",
  "acf_nocode_attr", "acf_enable_allocate_attr",
  "acf_explicit_handle_attr", "acf_heap_attr", "acf_inline_attr",
  "acf_outofline_attr", "acf_commstat_attr", "acf_faultstat_attr", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299
};
# endif

#define YYPACT_NINF (-160)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-114)

#define yytable_value_is_error(Yyn) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      -4,    53,    20,     8,    12,  -160,     3,  -160,    11,  -160,
      26,    27,  -160,  -160,  -160,    37,    58,    63,  -160,   -13,
    -160,  -160,  -160,  -160,  -160,  -160,  -160,  -160,  -160,  -160,
    -160,  -160,  -160,  -160,  -160,    19,     6,  -160,    48,    61,
      64,    69,    -7,    72,    73,    74,    53,  -160,  -160,    29,
       5,    10,    97,  -160,     9,  -160,    83,    84,    85,    91,
    -160,  -160,  -160,    75,  -160,    87,  -160,   -18,  -160,  -160,
    -160,    98,    93,  -160,  -160,   100,  -160,   108,  -160,   103,
    -160,  -160,  -160,  -160,   110,  -160,  -160,    71,   102,  -160,
     111,  -160,  -160,    -5,    32,   112,  -160,    33,  -160,  -160,
    -160,  -160,  -160,  -160,  -160,  -160,  -160,  -160,  -160,  -160,
    -160,  -160,  -160,   113,   117,  -160,  -160,  -160,    69,  -160,
    -160,  -160,  -160,  -160,   114,  -160,   115,    52,   120,  -160,
     122,  -160,    47,  -160,  -160,  -160,  -160,  -160,  -160,  -160,
     126,  -160,   118,   131,   143,   144,   146,   121,   119,  -160,
     -16,    91,  -160,  -160,   129,  -160,  -160,  -160,   124,   127,
     128,  -160,   102,   130,   133,   115,   134,   115,  -160,   135,
    -160,    14,   136,   140,  -160,   137,  -160,  -160,  -160,   138,
    -160,   139,  -160,  -160,  -160,   156,   110,   159,   110,  -160,
    -160,    55,  -160,  -160,  -160,  -160,  -160,  -160,  -160,   147,
    -160,  -160,  -160,   115,   115,    14,  -160,  -160,   110,   110,
    -160
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       5,     0,     0,     0,     0,   122,     0,   123,     0,   126,
      29,     0,   128,   124,   129,     0,     0,     0,    21,     0,
       6,    15,    17,    10,    18,    19,    20,    11,    16,     8,
       9,    12,    13,    14,     1,    36,     0,     2,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     4,    37,     0,
       0,     0,     0,    35,     0,    38,     0,     0,     0,     0,
      33,     3,    95,     0,   104,     0,    32,     0,    30,    26,
      25,     0,     0,    24,    97,     0,    99,     0,   102,     0,
       7,    43,    49,    52,    44,    50,    53,     0,     0,   130,
     106,   125,   131,     0,     0,     0,    91,     0,    80,    90,
      85,    86,    83,    84,    87,    88,    82,    89,    34,    39,
      40,    41,    42,     0,    74,    75,    94,   103,     0,    28,
      22,    27,    23,    96,     0,   101,     0,     0,     0,   127,
       0,    58,     0,    63,    65,    66,    67,    68,    69,    57,
      54,    55,     0,     0,     0,     0,     0,     0,     0,    78,
     108,     0,    31,   100,     0,    51,    62,    61,     0,     0,
       0,    60,     0,     0,     0,     0,     0,     0,    93,     0,
      81,     0,     0,   107,   109,     0,    76,    98,    73,     0,
      71,     0,    64,    56,   105,     0,    45,     0,    46,    92,
     121,     0,   114,   118,   119,   120,   116,   117,    77,   113,
     111,    72,    70,     0,     0,     0,   112,   110,    48,    47,
     115
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -160,  -160,  -160,  -160,  -160,   141,  -160,  -160,  -160,  -160,
    -160,  -160,  -160,    65,  -160,  -160,  -160,   125,  -160,  -151,
      56,  -160,  -160,    22,  -160,  -160,  -160,    21,  -160,  -160,
    -160,  -160,  -160,  -160,    34,  -160,  -160,    38,  -160,  -160,
    -160,  -160,  -160,  -160,  -160,  -160,  -160,  -160,  -160,   -50,
    -160,  -160,  -160,  -160,   -11,  -160,  -160,   -15,  -160,   -49,
     -48,  -160,   -47,  -159,   -87,   -86,  -156,  -149
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     3,     4,    19,    20,    21,    71,    72,    73,
     122,    22,    67,    68,    61,    37,    54,    55,    56,    84,
      85,    57,   140,   141,    88,   131,   132,   133,   134,   181,
     135,   179,    58,   114,   115,    59,    97,    98,    99,   169,
      23,    63,    24,    75,    25,    77,   154,    26,    79,    27,
      65,   101,   172,   173,   174,   175,   191,   192,    28,    29,
      30,   104,    31,   136,    32,    33,   106,   107
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     137,   138,   100,   102,   103,   105,    82,    49,    69,    35,
      49,    86,   193,   118,   186,   196,   188,   171,    46,   119,
      34,    89,   197,    47,    50,  -113,   143,    50,    92,     1,
     129,   144,    51,    38,    70,    51,    12,    39,    14,    52,
      36,    53,    52,    87,   108,    40,   193,   -79,    83,   196,
     -79,   -59,   208,   209,    48,   190,   197,     5,     6,     7,
      41,    42,     8,   145,   148,     9,    10,    81,   146,   149,
      11,    43,   127,   137,   138,    12,    13,    14,   160,   128,
      15,    16,    17,   161,   194,   195,   205,   129,   156,    60,
     157,   206,    44,    12,    18,    14,   130,    45,   100,   102,
     103,   105,    62,     7,    89,    64,     8,    90,    91,     9,
      66,    92,   116,    74,    76,    78,    93,    94,   194,   195,
      13,   110,   111,   112,   117,     7,    89,    95,     8,    90,
      91,     9,   113,    92,   121,   120,   128,   123,    96,   124,
     125,   126,    13,   139,   129,   142,   147,   150,   151,    95,
      12,   164,    14,   130,   158,   153,   159,   162,    83,   163,
      96,   165,   168,   166,   167,   178,   177,   184,   180,   185,
     187,   199,   189,   198,   203,   201,   202,   204,   200,   109,
     171,   182,   155,   152,   183,   176,   170,    80,   207,     0,
     210
};

static const yytype_int16 yycheck[] =
{
      87,    87,    52,    52,    52,    52,     1,     1,    15,     1,
       1,     1,   171,    31,   165,   171,   167,    33,    31,    37,
       0,     7,   171,    36,    18,    41,    31,    18,    14,    33,
      16,    36,    26,    21,    41,    26,    22,    34,    24,    33,
      32,    35,    33,    33,    35,    34,   205,    41,    43,   205,
      41,    41,   203,   204,    35,    41,   205,     4,     5,     6,
      34,    34,     9,    31,    31,    12,    13,    38,    36,    36,
      17,    34,     1,   160,   160,    22,    23,    24,    31,     8,
      27,    28,    29,    36,   171,   171,    31,    16,    36,    41,
      38,    36,    34,    22,    41,    24,    25,    34,   148,   148,
     148,   148,    41,     6,     7,    41,     9,    10,    11,    12,
      41,    14,    37,    41,    41,    41,    19,    20,   205,   205,
      23,    38,    38,    38,    37,     6,     7,    30,     9,    10,
      11,    12,    41,    14,    41,    37,     8,    37,    41,    31,
      37,    31,    23,    41,    16,    34,    34,    34,    31,    30,
      22,    20,    24,    25,    34,    41,    34,    31,    43,    41,
      41,    18,    41,    19,    18,    41,    37,    37,    41,    36,
      36,    31,    37,    37,    18,    37,    37,    18,    41,    54,
      33,   160,   126,   118,   162,   151,   148,    46,   199,    -1,
     205
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,    33,    46,    47,    48,     4,     5,     6,     9,    12,
      13,    17,    22,    23,    24,    27,    28,    29,    41,    49,
      50,    51,    56,    85,    87,    89,    92,    94,   103,   104,
     105,   107,   109,   110,     0,     1,    32,    60,    21,    34,
      34,    34,    34,    34,    34,    34,    31,    36,    35,     1,
      18,    26,    33,    35,    61,    62,    63,    66,    77,    80,
      41,    59,    41,    86,    41,    95,    41,    57,    58,    15,
      41,    52,    53,    54,    41,    88,    41,    90,    41,    93,
      50,    38,     1,    43,    64,    65,     1,    33,    69,     7,
      10,    11,    14,    19,    20,    30,    41,    81,    82,    83,
      94,    96,   104,   105,   106,   107,   111,   112,    35,    62,
      38,    38,    38,    41,    78,    79,    37,    37,    31,    37,
      37,    41,    55,    37,    31,    37,    31,     1,     8,    16,
      25,    70,    71,    72,    73,    75,   108,   109,   110,    41,
      67,    68,    34,    31,    36,    31,    36,    34,    31,    36,
      34,    31,    58,    41,    91,    65,    36,    38,    34,    34,
      31,    36,    31,    41,    20,    18,    19,    18,    41,    84,
      82,    33,    97,    98,    99,   100,    79,    37,    41,    76,
      41,    74,    72,    68,    37,    36,    64,    36,    64,    37,
      41,   101,   102,   108,   109,   110,   111,   112,    37,    31,
      41,    37,    37,    18,    18,    31,    36,    99,    64,    64,
     102
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_int8 yyr1[] =
{
       0,    45,    46,    47,    48,    48,    49,    49,    50,    50,
      50,    50,    50,    50,    50,    50,    50,    50,    50,    50,
      50,    50,    51,    52,    53,    53,    54,    55,    56,    56,
      57,    57,    58,    59,    60,    60,    60,    60,    61,    61,
      62,    62,    62,    62,    63,    63,    63,    63,    63,    63,
      64,    64,    65,    66,    66,    67,    67,    68,    69,    69,
      70,    70,    70,    71,    71,    72,    72,    72,    72,    72,
      73,    74,    75,    76,    77,    78,    78,    79,    80,    80,
      81,    81,    82,    82,    82,    82,    82,    82,    82,    82,
      82,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    96,    97,    97,    98,
      98,    99,   100,   100,   101,   101,   102,   102,   102,   102,
     102,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     2,     3,     3,     0,     1,     3,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     4,     2,     1,     1,     1,     1,     4,     1,
       1,     3,     1,     1,     3,     2,     1,     2,     1,     2,
       2,     2,     2,     2,     2,     5,     5,     7,     7,     2,
       1,     3,     1,     2,     3,     1,     3,     1,     2,     0,
       2,     2,     2,     1,     3,     1,     1,     1,     1,     1,
       4,     1,     4,     1,     2,     1,     3,     4,     3,     0,
       1,     3,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     4,     1,     4,     1,     4,     1,     6,     1,
       1,     4,     1,     4,     1,     4,     1,     1,     0,     1,
       3,     2,     3,     0,     1,     3,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yytype], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyo, yytype, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[+yyssp[yyi + 1 - yynrhs]],
                       &yyvsp[(yyi + 1) - (yynrhs)]
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
#  else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
yystrlen (const char *yystr)
{
  YYPTRDIFF_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYPTRDIFF_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYPTRDIFF_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (yyres)
    return yystpcpy (yyres, yystr) - yyres;
  else
    return yystrlen (yystr);
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYPTRDIFF_T *yymsg_alloc, char **yymsg,
                yy_state_t *yyssp, int yytoken)
{
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat: reported tokens (one for the "unexpected",
     one per "expected"). */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Actual size of YYARG. */
  int yycount = 0;
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T yysize = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[+*yyssp];
      YYPTRDIFF_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
      yysize = yysize0;
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYPTRDIFF_T yysize1
                    = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
                    yysize = yysize1;
                  else
                    return 2;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    /* Don't count the "%s"s in the final size, but reserve room for
       the terminator.  */
    YYPTRDIFF_T yysize1 = yysize + (yystrlen (yyformat) - 2 * yycount) + 1;
    if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
      yysize = yysize1;
    else
      return 2;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          ++yyp;
          ++yyformat;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss;
    yy_state_t *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYPTRDIFF_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYPTRDIFF_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
# undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 3:
#line 347 "acf.y"
    {
        char            *ast_int_name;  /* Interface name in AST node */
        NAMETABLE_id_t  type_id;        /* Nametable id of impl_handle type */
        NAMETABLE_id_t  impl_name_id;   /* Nametable id of impl_handle var */
        AST_type_n_t    *type_p;        /* Ptr to implicit_handle type node */

#ifdef DUMPERS
        if (cmd_opt[opt_dump_acf])
            dump_attributes("ACF interface", interface_name, &interface_attr);
#endif

        /* Store source information. */
        if (the_interface->fe_info != NULL)
        {
            the_interface->fe_info->acf_file = error_file_name_id;
            the_interface->fe_info->acf_source_line = acf_yylineno;
        }

        /*
         *  Interface attributes are saved for main and imported interfaces.
         *  the_interface = pointer to main or imported interface node
         *
         *  Make sure that the interface name in the ACF agrees with the
         *  interface name in the main IDL file.  Then set the parsed
         *  attributes in the interface node.
         *
         *  interface_attr = bitmask of interface attributes parsed.
         *  interface_name = ACF interface name parsed.
         */

        NAMETABLE_id_to_string(the_interface->name, &ast_int_name);

        if (strcmp(interface_name, ast_int_name) != 0)
        {
            char    *acf_int_name;      /* Ptr to permanent copy */
            NAMETABLE_id_t name_id;     /* Handle on permanent copy */
            char    *file_name;         /* Related file name */

            name_id = NAMETABLE_add_id(interface_name);
            NAMETABLE_id_to_string(name_id, &acf_int_name);

            STRTAB_str_to_string(the_interface->fe_info->file, &file_name);

            acf_error(NIDL_INTNAMDIF, acf_int_name, ast_int_name);
            acf_error(NIDL_NAMEDECLAT, ast_int_name, file_name,
                      the_interface->fe_info->source_line);
        }
        else
        {
            if (interface_attr.bit.code)
                AST_SET_CODE(the_interface);
            if (interface_attr.bit.nocode)
                AST_SET_NO_CODE(the_interface);
            if (interface_attr.bit.decode)
                AST_SET_DECODE(the_interface);
            if (interface_attr.bit.encode)
                AST_SET_ENCODE(the_interface);
            if (interface_attr.bit.explicit_handle)
                AST_SET_EXPLICIT_HANDLE(the_interface);
            if (interface_attr.bit.in_line)
                AST_SET_IN_LINE(the_interface);
            if (interface_attr.bit.out_of_line)
                AST_SET_OUT_OF_LINE(the_interface);
            if (interface_attr.bit.auto_handle)
                AST_SET_AUTO_HANDLE(the_interface);
            if (interface_attr.bit.cs_tag_rtn)
                the_interface->cs_tag_rtn_name = NAMETABLE_add_id(cs_tag_rtn_name);
            if (interface_attr.bit.binding_callout)
                the_interface->binding_callout_name = NAMETABLE_add_id(binding_callout_name);
            if (interface_attr.bit.lookup_callout)
                the_interface->lookup_callout_name = NAMETABLE_add_id(lookup_callout_name);
            if (interface_attr.bit.malloc_callout_name)
                the_interface->malloc_callout_name = NAMETABLE_add_id(malloc_callout_name);
            if (interface_attr.bit.free_callout_name)
                the_interface->free_callout_name = NAMETABLE_add_id(free_callout_name);
            if (interface_attr.bit.cxx_delegate)
                the_interface->cxx_delegate_name = NAMETABLE_add_id(cxx_delegate_name);

            if (interface_attr.bit.implicit_handle)
            {
                /* Store the [implicit_handle] variable name in nametbl. */
                impl_name_id = NAMETABLE_add_id(impl_name);

                /*
                 * Store the type and name information in the AST.  If a
                 * named type, determine whether it is an IDL-defined type
                 * and process accordingly.  Otherwise the type is handle_t.
                 */
                if (named_type)
                {
                    if (lookup_type(type_name, FALSE, &type_id, &type_p))
                    {
                        the_interface->implicit_handle_name = impl_name_id;
                        the_interface->implicit_handle_type = type_p;
                        the_interface->implicit_handle_type_name = type_p->name;
                        if (AST_HANDLE_SET(type_p))
                            AST_SET_IMPLICIT_HANDLE_G(the_interface);
                    }
                    else    /* A user-defined type, not defined in IDL */
                    {
                        /* Store the user type name in nametbl. */
                        the_interface->implicit_handle_type_name
                            = NAMETABLE_add_id(type_name);

                        the_interface->implicit_handle_name = impl_name_id;
                        the_interface->implicit_handle_type = NULL;
                        AST_SET_IMPLICIT_HANDLE_G(the_interface);
                    }
                }
                else
                {
                    the_interface->implicit_handle_name = impl_name_id;
                    the_interface->implicit_handle_type = ASTP_handle_ptr;
                }
            }
        }

        interface_name = NULL;
        type_name = NULL;
        impl_name = NULL;
        binding_callout_name = NULL;
        lookup_callout_name = NULL;
        malloc_callout_name = NULL;
        free_callout_name = NULL;
        cxx_delegate_name = NULL;
        cs_tag_rtn_name = NULL;
        cxx_static_name = NULL;
        interface_attr.mask = 0;        /* Reset attribute mask */
    }
#line 1780 "acf_y_tab.c"
    break;

  case 8:
#line 490 "acf.y"
    {
        if (interface_attr.bit.code)
            log_warning(yylineno, NIDL_MULATTRDEF);
        interface_attr.bit.code = TRUE;
    }
#line 1790 "acf_y_tab.c"
    break;

  case 9:
#line 496 "acf.y"
    {
        if (interface_attr.bit.nocode)
            log_warning(yylineno, NIDL_MULATTRDEF);
        interface_attr.bit.nocode = TRUE;
    }
#line 1800 "acf_y_tab.c"
    break;

  case 10:
#line 502 "acf.y"
    {
        if (interface_attr.bit.binding_callout)
            log_error(yylineno, NIDL_ATTRUSEMULT);
        interface_attr.bit.binding_callout = TRUE;
    }
#line 1810 "acf_y_tab.c"
    break;

  case 11:
#line 508 "acf.y"
    {
        if (interface_attr.bit.cs_tag_rtn)
            log_error(yylineno, NIDL_ATTRUSEMULT);
        interface_attr.bit.cs_tag_rtn = TRUE;
    }
#line 1820 "acf_y_tab.c"
    break;

  case 12:
#line 514 "acf.y"
    {
        if (interface_attr.bit.explicit_handle)
            log_warning(yylineno, NIDL_MULATTRDEF);
        interface_attr.bit.explicit_handle = TRUE;
    }
#line 1830 "acf_y_tab.c"
    break;

  case 13:
#line 520 "acf.y"
    {
        if (interface_attr.bit.in_line)
            log_warning(yylineno, NIDL_MULATTRDEF);
        interface_attr.bit.in_line = TRUE;
    }
#line 1840 "acf_y_tab.c"
    break;

  case 14:
#line 526 "acf.y"
    {
        if (interface_attr.bit.out_of_line)
            log_warning(yylineno, NIDL_MULATTRDEF);
        interface_attr.bit.out_of_line = TRUE;
    }
#line 1850 "acf_y_tab.c"
    break;

  case 15:
#line 532 "acf.y"
    {
        if (interface_attr.bit.implicit_handle)
            log_error(yylineno, NIDL_ATTRUSEMULT);
        interface_attr.bit.implicit_handle = TRUE;
    }
#line 1860 "acf_y_tab.c"
    break;

  case 16:
#line 538 "acf.y"
    {
        if (interface_attr.bit.auto_handle)
            log_warning(yylineno, NIDL_MULATTRDEF);
        interface_attr.bit.auto_handle = TRUE;
    }
#line 1870 "acf_y_tab.c"
    break;

  case 17:
#line 544 "acf.y"
    {
        if (interface_attr.bit.extern_exceps)
            log_warning(yylineno, NIDL_MULATTRDEF);
        interface_attr.bit.extern_exceps = TRUE;
    }
#line 1880 "acf_y_tab.c"
    break;

  case 18:
#line 550 "acf.y"
    {
        if (interface_attr.bit.lookup_callout)
            log_warning(yylineno, NIDL_ATTRUSEMULT);
	interface_attr.bit.lookup_callout = TRUE;
    }
#line 1890 "acf_y_tab.c"
    break;

  case 19:
#line 556 "acf.y"
    {
        if (interface_attr.bit.malloc_callout_name)
            log_warning(yylineno, NIDL_ATTRUSEMULT);
	interface_attr.bit.malloc_callout_name = TRUE;
        if (interface_attr.bit.free_callout_name)
            log_warning(yylineno, NIDL_ATTRUSEMULT);
	interface_attr.bit.free_callout_name = TRUE;
    }
#line 1903 "acf_y_tab.c"
    break;

  case 20:
#line 565 "acf.y"
    {
        if (interface_attr.bit.cxx_delegate)
            log_warning(yylineno, NIDL_ATTRUSEMULT);
	interface_attr.bit.cxx_delegate = TRUE;
    }
#line 1913 "acf_y_tab.c"
    break;

  case 21:
#line 571 "acf.y"
    {
        if (NAMETABLE_add_id("decode") == (yyvsp[0].y_id))
        {
            if (interface_attr.bit.decode)
                log_warning(yylineno, NIDL_MULATTRDEF);
            interface_attr.bit.decode = TRUE;
        }
        else if (NAMETABLE_add_id("encode") == (yyvsp[0].y_id))
        {
            if (interface_attr.bit.encode)
                log_warning(yylineno, NIDL_MULATTRDEF);
            interface_attr.bit.encode = TRUE;
        }
        else
            log_error(yylineno, NIDL_ERRINATTR);
    }
#line 1934 "acf_y_tab.c"
    break;

  case 24:
#line 599 "acf.y"
    {
        named_type = FALSE;
    }
#line 1942 "acf_y_tab.c"
    break;

  case 25:
#line 603 "acf.y"
    {
        NAMETABLE_id_to_string((yyvsp[0].y_id), &type_name);
        named_type = TRUE;
    }
#line 1951 "acf_y_tab.c"
    break;

  case 27:
#line 615 "acf.y"
    {
        NAMETABLE_id_to_string((yyvsp[0].y_id), &impl_name);
    }
#line 1959 "acf_y_tab.c"
    break;

  case 29:
#line 623 "acf.y"
    {
        if (ASTP_parsing_main_idl)
        {
            AST_exception_n_t *excep_p;
            for (excep_p = the_interface->exceptions;
                 excep_p != NULL;
                 excep_p = excep_p->next)
            {
                AST_SET_EXTERN(excep_p);
            }
        }
    }
#line 1976 "acf_y_tab.c"
    break;

  case 32:
#line 644 "acf.y"
    {
        AST_exception_n_t *excep_p;
        if (ASTP_parsing_main_idl)
            if (lookup_exception((yyvsp[0].y_id), TRUE, &excep_p))
                AST_SET_EXTERN(excep_p);
    }
#line 1987 "acf_y_tab.c"
    break;

  case 33:
#line 654 "acf.y"
    {
        NAMETABLE_id_to_string((yyvsp[0].y_id), &interface_name);
    }
#line 1995 "acf_y_tab.c"
    break;

  case 36:
#line 663 "acf.y"
        { log_error(yylineno, NIDL_SYNTAXERR); }
#line 2001 "acf_y_tab.c"
    break;

  case 37:
#line 665 "acf.y"
        { log_error(yylineno, NIDL_SYNTAXERR); }
#line 2007 "acf_y_tab.c"
    break;

  case 43:
#line 678 "acf.y"
        {
            log_error(yylineno, NIDL_SYNTAXERR);
            /* Re-initialize attr masks to avoid sticky attributes */
            interface_attr.mask = 0;
            type_attr.mask      = 0;
            operation_attr.mask = 0;
            parameter_attr.mask = 0;
        }
#line 2020 "acf_y_tab.c"
    break;

  case 44:
#line 690 "acf.y"
        {
        if (ASTP_parsing_main_idl)
            the_interface->includes = (AST_include_n_t *)
                AST_concat_element((ASTP_node_t *)the_interface->includes,
                                   (ASTP_node_t *)include_list);
        include_list = NULL;
        }
#line 2032 "acf_y_tab.c"
    break;

  case 45:
#line 698 "acf.y"
        {
        if (ASTP_parsing_main_idl)
            the_interface->sincludes = (AST_include_n_t *)
                AST_concat_element((ASTP_node_t *)the_interface->sincludes,
                                   (ASTP_node_t *)include_list);
        include_list = NULL;
        }
#line 2044 "acf_y_tab.c"
    break;

  case 46:
#line 706 "acf.y"
        {
        if (ASTP_parsing_main_idl)
            the_interface->cincludes = (AST_include_n_t *)
                AST_concat_element((ASTP_node_t *)the_interface->cincludes,
                                   (ASTP_node_t *)include_list);
        include_list = NULL;
        }
#line 2056 "acf_y_tab.c"
    break;

  case 47:
#line 714 "acf.y"
        {
        if (ASTP_parsing_main_idl) {
            the_interface->cincludes = (AST_include_n_t *)
                AST_concat_element((ASTP_node_t *)the_interface->cincludes,
                                   (ASTP_node_t *)include_list);
            the_interface->sincludes = (AST_include_n_t *)
                AST_concat_element((ASTP_node_t *)the_interface->sincludes,
                                   (ASTP_node_t *)include_list);
	}
        include_list = NULL;
        }
#line 2072 "acf_y_tab.c"
    break;

  case 48:
#line 726 "acf.y"
        {
        if (ASTP_parsing_main_idl) {
            the_interface->sincludes = (AST_include_n_t *)
                AST_concat_element((ASTP_node_t *)the_interface->sincludes,
                                   (ASTP_node_t *)include_list);
            the_interface->cincludes = (AST_include_n_t *)
                AST_concat_element((ASTP_node_t *)the_interface->cincludes,
                                   (ASTP_node_t *)include_list);
	}
        include_list = NULL;
        }
#line 2088 "acf_y_tab.c"
    break;

  case 49:
#line 738 "acf.y"
        { log_error(yylineno, NIDL_SYNTAXERR); }
#line 2094 "acf_y_tab.c"
    break;

  case 52:
#line 748 "acf.y"
    {
        if (ASTP_parsing_main_idl)
        {
            char            *parsed_include_file;
            char            include_type[PATH_MAX];
            char            include_file[PATH_MAX];
            STRTAB_str_t    include_file_id;

            STRTAB_str_to_string((yyvsp[0].y_string), &parsed_include_file);

            /*
             * Log warning if include name contains a file extension.
             * Tack on the correct extension based on the -lang option.
             */
            FILE_parse(parsed_include_file, (char *)NULL, (char *)NULL,
                       include_type);
            if (include_type[0] != '\0')
                acf_warning(NIDL_INCLUDEXT);

            FILE_form_filespec(parsed_include_file, (char *)NULL,
			       ".h",
                               (char *)NULL, include_file);

            /* Create an include node. */
            include_file_id = STRTAB_add_string(include_file);
            include_p = AST_include_node(include_file_id, (yyvsp[0].y_string));

            /* Store source information. */
            if (include_p->fe_info != NULL)
            {
                include_p->fe_info->acf_file = error_file_name_id;
                include_p->fe_info->acf_source_line = acf_yylineno;
            }

            include_list = (AST_include_n_t *)
                AST_concat_element((ASTP_node_t *)include_list,
                                   (ASTP_node_t *)include_p);
        }
    }
#line 2138 "acf_y_tab.c"
    break;

  case 53:
#line 791 "acf.y"
        { log_error(yylineno, NIDL_SYNTAXERR); }
#line 2144 "acf_y_tab.c"
    break;

  case 54:
#line 793 "acf.y"
    {
        type_attr.mask = 0;             /* Reset attribute mask */
        repr_type_name = NULL;          /* Reset represent_as type name */
        cs_char_type_name = NULL;       /* Reset cs_char type name */
    }
#line 2154 "acf_y_tab.c"
    break;

  case 55:
#line 802 "acf.y"
    {
        type_name = NULL;               /* Reset type name */
    }
#line 2162 "acf_y_tab.c"
    break;

  case 56:
#line 806 "acf.y"
    {
        type_name = NULL;               /* Reset type name */
    }
#line 2170 "acf_y_tab.c"
    break;

  case 57:
#line 813 "acf.y"
    {
        NAMETABLE_id_t  type_id;        /* Nametable id of type name */
        AST_type_n_t    *type_p;        /* Ptr to AST type node */

        NAMETABLE_id_to_string((yyvsp[0].y_id), &type_name);

#ifdef DUMPERS
        if (cmd_opt[opt_dump_acf])
            dump_attributes("ACF type", type_name, &type_attr);
#endif

        /*
         *  Lookup the type_name parsed and verify that it is a valid type
         *  node.  Then set the parsed attributes in the type node.
         *
         *  type_attr = bitmask of type attributes parsed.
         *  type_name = name of type_t node to look up.
         *  [repr_type_name] = name of represent_as type.
         *  [cs_char_type_name] = name of cs_char type.
         */

        if (lookup_type(type_name, TRUE, &type_id, &type_p))
        {
            /* Store source information. */
            if (type_p->fe_info != NULL)
            {
                type_p->fe_info->acf_file = error_file_name_id;
                type_p->fe_info->acf_source_line = acf_yylineno;
            }

            if (type_attr.bit.heap
                && type_p->kind != AST_pipe_k
                && !AST_CONTEXT_RD_SET(type_p))
                PROP_set_type_attr(type_p,AST_HEAP);
            if (type_attr.bit.in_line)
                PROP_set_type_attr(type_p,AST_IN_LINE);
            if ((type_attr.bit.out_of_line) &&
                (type_p->kind != AST_pointer_k) &&
                (type_p->xmit_as_type == NULL))
                PROP_set_type_attr(type_p,AST_OUT_OF_LINE);
            if (type_attr.bit.represent_as)
                process_rep_as_type(the_interface, type_p, repr_type_name);
            if (type_attr.bit.cs_char)
                process_cs_char_type(the_interface, type_p, cs_char_type_name);
        }
    }
#line 2221 "acf_y_tab.c"
    break;

  case 61:
#line 869 "acf.y"
        {
        log_error(yylineno, NIDL_MISSONATTR);
        }
#line 2229 "acf_y_tab.c"
    break;

  case 62:
#line 873 "acf.y"
        {
        log_error(yylineno, NIDL_ERRINATTR);
        }
#line 2237 "acf_y_tab.c"
    break;

  case 65:
#line 885 "acf.y"
    {
        if (type_attr.bit.represent_as)
            log_error(yylineno, NIDL_ATTRUSEMULT);
        type_attr.bit.represent_as = TRUE;
    }
#line 2247 "acf_y_tab.c"
    break;

  case 66:
#line 891 "acf.y"
    {
        if (type_attr.bit.cs_char)
            log_error(yylineno, NIDL_ATTRUSEMULT);
        type_attr.bit.cs_char = TRUE;
    }
#line 2257 "acf_y_tab.c"
    break;

  case 67:
#line 897 "acf.y"
    {
        if (type_attr.bit.heap)
            log_warning(yylineno, NIDL_MULATTRDEF);
        type_attr.bit.heap = TRUE;
    }
#line 2267 "acf_y_tab.c"
    break;

  case 68:
#line 903 "acf.y"
    {
        if (type_attr.bit.in_line)
            log_warning(yylineno, NIDL_MULATTRDEF);
        type_attr.bit.in_line = TRUE;
    }
#line 2277 "acf_y_tab.c"
    break;

  case 69:
#line 909 "acf.y"
    {
        if (type_attr.bit.out_of_line)
            log_warning(yylineno, NIDL_MULATTRDEF);
        type_attr.bit.out_of_line = TRUE;
    }
#line 2287 "acf_y_tab.c"
    break;

  case 71:
#line 922 "acf.y"
    {
        NAMETABLE_id_to_string((yyvsp[0].y_id), &repr_type_name);
    }
#line 2295 "acf_y_tab.c"
    break;

  case 73:
#line 933 "acf.y"
    {
        NAMETABLE_id_to_string((yyvsp[0].y_id), &cs_char_type_name);
    }
#line 2303 "acf_y_tab.c"
    break;

  case 74:
#line 940 "acf.y"
    {
        operation_attr.mask = 0;        /* Reset attribute mask */
        cs_tag_rtn_name     = NULL;     /* Reset cs_tag_rtn name */
        cxx_static_name     = NULL;
        cxx_new_name        = NULL;
        cxx_delegate_name   = NULL;
    }
#line 2315 "acf_y_tab.c"
    break;

  case 77:
#line 956 "acf.y"
    {
        acf_param_t         *p;         /* Ptr to local parameter structure */
        NAMETABLE_id_t      op_id;      /* Nametable id of operation name */
        NAMETABLE_id_t      param_id;   /* Nametable id of parameter name */
        AST_operation_n_t   *op_p;      /* Ptr to AST operation node */
        AST_parameter_n_t   *param_p;   /* Ptr to AST parameter node */
        boolean             log_error;  /* TRUE => error if name not found */
        char                *param_name;/* character string of param id */

        NAMETABLE_id_to_string((yyvsp[-3].y_id), &operation_name);
#ifdef DUMPERS
        if (cmd_opt[opt_dump_acf])
            dump_attributes("ACF operation", operation_name, &operation_attr);
#endif

        /*
         *  Operation and parameter attributes are ignored for imported
         *  interfaces.  Operations and parameters within imported interfaces
         *  are not put in the AST.
         */
        if (ASTP_parsing_main_idl)
        {
            /*
             *  Lookup the operation_name parsed and verify that it is a valid
             *  operation node.  Then set the parsed attributes in the operation
             *  node.  For each parameter_name that was parsed for this
             *  operation, chase the parameter list off the AST operation node
             *  to verify that it is a valid parameter for that operation.
             *  Then set the parsed attributes for that parameter into the
             *  relevant parameter node.
             *
             *  operation_attr = bitmask of operation attributes parsed.
             *  operation_name = name of routine_t node to look up.
             *  [cs_tag_rtn_name] = cs_tag_rtn name.
             *  [cxx_static_name] = cxx_static name.
             *  [cxx_new_name]    = cxx_new name.
             *  parameter_list = linked list of parameter information.
             */

            if (lookup_operation(operation_name, TRUE, &op_id, &op_p))
            {
                /* Store source information. */
                if (op_p->fe_info != NULL)
                {
                    op_p->fe_info->acf_file = error_file_name_id;
                    op_p->fe_info->acf_source_line = acf_yylineno;
                }

                if (operation_attr.bit.comm_status)
                {
                    /*
                     * Assume the AST Builder always builds a result param,
                     * even for void operations.
                     */
                    AST_SET_COMM_STATUS(op_p->result);
                }
                if (operation_attr.bit.fault_status)
                    AST_SET_FAULT_STATUS(op_p->result);

                if (operation_attr.bit.code)
                    AST_SET_CODE(op_p);
                if (operation_attr.bit.nocode)
                    AST_SET_NO_CODE(op_p);
                if (operation_attr.bit.decode)
                    AST_SET_DECODE(op_p);
                if (operation_attr.bit.encode)
                    AST_SET_ENCODE(op_p);
                if (operation_attr.bit.enable_allocate)
                    AST_SET_ENABLE_ALLOCATE(op_p);
                if (operation_attr.bit.explicit_handle)
                    AST_SET_EXPLICIT_HANDLE(op_p);
                if (operation_attr.bit.cs_tag_rtn)
                    op_p->cs_tag_rtn_name = NAMETABLE_add_id(cs_tag_rtn_name);
		if (operation_attr.bit.cxx_new)
		{
                    op_p->cxx_new_name = NAMETABLE_add_id(cxx_new_name);
		}
		if (operation_attr.bit.cxx_static)
		{
                    AST_SET_STATIC(op_p);
                    if (cxx_static_name == NULL)
                        op_p->cxx_static_name = NULL;
                    else
                        op_p->cxx_static_name = NAMETABLE_add_id(cxx_static_name);
		}

                for (p = parameter_list ; p != NULL ; p = p->next)
                {
                    /*
                     * Most parameter attributes, if present, require that the
                     * referenced parameter be defined in the IDL.  If only
                     * [comm_status] and/or [fault_status] is present, the
                     * parameter  needn't be IDL-defined.
                     */
                    if (!p->parameter_attr.bit.heap
                        &&  !p->parameter_attr.bit.in_line
                        &&  !p->parameter_attr.bit.out_of_line
                        &&  !p->parameter_attr.bit.cs_stag
                        &&  !p->parameter_attr.bit.cs_drtag
                        &&  !p->parameter_attr.bit.cs_rtag
                        &&  (p->parameter_attr.bit.comm_status
                             || p->parameter_attr.bit.fault_status))
                        log_error = FALSE;
                    else
                        log_error = TRUE;

                    NAMETABLE_id_to_string(p->param_id, &param_name);
                    if (lookup_parameter(op_p, param_name, log_error,
                                         &param_id, &param_p))
                    {
                        /* Store source information. */
                        if (param_p->fe_info != NULL)
                        {
                            param_p->fe_info->acf_file = error_file_name_id;
                            param_p->fe_info->acf_source_line = acf_yylineno;
                        }
                        if (p->parameter_attr.bit.comm_status)
                            AST_SET_COMM_STATUS(param_p);
                        if (p->parameter_attr.bit.fault_status)
                            AST_SET_FAULT_STATUS(param_p);
                        if (p->parameter_attr.bit.heap)
                        {
                            AST_type_n_t *ref_type_p;
                            ref_type_p = param_follow_ref_ptr(param_p,
                                                              CHK_follow_ref);
                            if (ref_type_p->kind != AST_pipe_k
                                && !AST_CONTEXT_SET(param_p)
                                && !AST_CONTEXT_RD_SET(ref_type_p)
                                && !type_is_scalar(ref_type_p))
                                AST_SET_HEAP(param_p);
                        }
                        if (p->parameter_attr.bit.in_line)
                            AST_SET_IN_LINE(param_p);
                        /*
                         * We parse the [out_of_line] parameter attribute,
                         * but disallow it.
                         */
                        if (p->parameter_attr.bit.out_of_line)
                            acf_error(NIDL_INVOOLPRM);
                        if (p->parameter_attr.bit.cs_stag)
                            AST_SET_CS_STAG(param_p);
                        if (p->parameter_attr.bit.cs_drtag)
                            AST_SET_CS_DRTAG(param_p);
                        if (p->parameter_attr.bit.cs_rtag)
                            AST_SET_CS_RTAG(param_p);
                    }
                    else if (log_error == FALSE)
                    {
                        /*
                         * Lookup failed, but OK since the parameter only has
                         * attribute(s) that specify an additional parameter.
                         * Append a parameter to the operation parameter list.
                         */
                        NAMETABLE_id_to_string(p->param_id, &param_name);
                        append_parameter(op_p, param_name, &p->parameter_attr);
                    }
                }
            }
        }

        free_param_list(&parameter_list);       /* Free parameter list */

        operation_name = NULL;
    }
#line 2484 "acf_y_tab.c"
    break;

  case 82:
#line 1134 "acf.y"
    {
        if (operation_attr.bit.comm_status)
            log_warning(yylineno, NIDL_MULATTRDEF);
        operation_attr.bit.comm_status = TRUE;
    }
#line 2494 "acf_y_tab.c"
    break;

  case 83:
#line 1140 "acf.y"
    {
        if (operation_attr.bit.code)
            log_warning(yylineno, NIDL_MULATTRDEF);
        operation_attr.bit.code = TRUE;
    }
#line 2504 "acf_y_tab.c"
    break;

  case 84:
#line 1146 "acf.y"
    {
        if (operation_attr.bit.nocode)
            log_warning(yylineno, NIDL_MULATTRDEF);
        operation_attr.bit.nocode = TRUE;
    }
#line 2514 "acf_y_tab.c"
    break;

  case 85:
#line 1152 "acf.y"
    {
        if (operation_attr.bit.cs_tag_rtn)
            log_error(yylineno, NIDL_ATTRUSEMULT);
        operation_attr.bit.cs_tag_rtn = TRUE;
    }
#line 2524 "acf_y_tab.c"
    break;

  case 86:
#line 1158 "acf.y"
    {
        if (operation_attr.bit.cxx_static)
            log_error(yylineno, NIDL_ATTRUSEMULT);
        operation_attr.bit.cxx_static = TRUE;
    }
#line 2534 "acf_y_tab.c"
    break;

  case 87:
#line 1164 "acf.y"
    {
        if (operation_attr.bit.enable_allocate)
            log_warning(yylineno, NIDL_MULATTRDEF);
        operation_attr.bit.enable_allocate = TRUE;
    }
#line 2544 "acf_y_tab.c"
    break;

  case 88:
#line 1170 "acf.y"
    {
        if (operation_attr.bit.explicit_handle)
            log_warning(yylineno, NIDL_MULATTRDEF);
        operation_attr.bit.explicit_handle = TRUE;
    }
#line 2554 "acf_y_tab.c"
    break;

  case 89:
#line 1176 "acf.y"
    {
        if (operation_attr.bit.fault_status)
            log_warning(yylineno, NIDL_MULATTRDEF);
        operation_attr.bit.fault_status = TRUE;
    }
#line 2564 "acf_y_tab.c"
    break;

  case 90:
#line 1182 "acf.y"
    {
        if (operation_attr.bit.cxx_new)
            log_error(yylineno, NIDL_ATTRUSEMULT);
        operation_attr.bit.cxx_new = TRUE;
    }
#line 2574 "acf_y_tab.c"
    break;

  case 91:
#line 1188 "acf.y"
    {
        if (NAMETABLE_add_id("decode") == (yyvsp[0].y_id))
        {
            if (operation_attr.bit.decode)
                log_warning(yylineno, NIDL_MULATTRDEF);
            operation_attr.bit.decode = TRUE;
        }
        else if (NAMETABLE_add_id("encode") == (yyvsp[0].y_id))
        {
            if (operation_attr.bit.encode)
                log_warning(yylineno, NIDL_MULATTRDEF);
            operation_attr.bit.encode = TRUE;
        }
	else 
	    log_error(yylineno, NIDL_ERRINATTR);
    }
#line 2595 "acf_y_tab.c"
    break;

  case 93:
#line 1212 "acf.y"
    {
        NAMETABLE_id_to_string((yyvsp[0].y_id), &cxx_new_name);
    }
#line 2603 "acf_y_tab.c"
    break;

  case 95:
#line 1223 "acf.y"
    {
        NAMETABLE_id_to_string((yyvsp[0].y_id), &binding_callout_name);
    }
#line 2611 "acf_y_tab.c"
    break;

  case 97:
#line 1234 "acf.y"
    {
        NAMETABLE_id_to_string((yyvsp[0].y_id), &lookup_callout_name);
    }
#line 2619 "acf_y_tab.c"
    break;

  case 99:
#line 1245 "acf.y"
    {
        NAMETABLE_id_to_string((yyvsp[0].y_id), &malloc_callout_name);
    }
#line 2627 "acf_y_tab.c"
    break;

  case 100:
#line 1252 "acf.y"
    {
        NAMETABLE_id_to_string((yyvsp[0].y_id), &free_callout_name);
    }
#line 2635 "acf_y_tab.c"
    break;

  case 102:
#line 1264 "acf.y"
    {
        NAMETABLE_id_to_string((yyvsp[0].y_id), &cxx_delegate_name);
    }
#line 2643 "acf_y_tab.c"
    break;

  case 104:
#line 1275 "acf.y"
    {
        NAMETABLE_id_to_string((yyvsp[0].y_id), &cs_tag_rtn_name);
    }
#line 2651 "acf_y_tab.c"
    break;

  case 105:
#line 1282 "acf.y"
    {
        NAMETABLE_id_to_string((yyvsp[-1].y_id), &cxx_static_name);
    }
#line 2659 "acf_y_tab.c"
    break;

  case 106:
#line 1286 "acf.y"
    {
        cxx_static_name = NULL;
    }
#line 2667 "acf_y_tab.c"
    break;

  case 111:
#line 1303 "acf.y"
    {
#ifdef DUMPERS
        if (cmd_opt[opt_dump_acf])
        {
            char *param_name;
            NAMETABLE_id_to_string((yyvsp[0].y_id), &param_name);
            dump_attributes("ACF parameter", param_name, &parameter_attr);
        }
#endif

        if (parameter_attr_list)        /* If there were param attributes: */
        {
            acf_param_t *p;             /* Pointer to parameter record */

            /*
             * Allocate and initialize a parameter record.
             */
            p = alloc_param();
            p->parameter_attr = parameter_attr;
            p->param_id = (yyvsp[0].y_id);

            /*
             * Add to end of parameter list.
             */
            add_param_to_list(p, &parameter_list);

            parameter_attr.mask = 0;
        }
    }
#line 2701 "acf_y_tab.c"
    break;

  case 112:
#line 1336 "acf.y"
    {
        parameter_attr_list = TRUE;     /* Flag that we have param attributes */
    }
#line 2709 "acf_y_tab.c"
    break;

  case 113:
#line 1340 "acf.y"
    {
        parameter_attr_list = FALSE;
    }
#line 2717 "acf_y_tab.c"
    break;

  case 116:
#line 1352 "acf.y"
    {
        if (parameter_attr.bit.comm_status)
            log_warning(yylineno, NIDL_MULATTRDEF);
        parameter_attr.bit.comm_status = TRUE;
    }
#line 2727 "acf_y_tab.c"
    break;

  case 117:
#line 1358 "acf.y"
    {
        if (parameter_attr.bit.fault_status)
            log_warning(yylineno, NIDL_MULATTRDEF);
        parameter_attr.bit.fault_status = TRUE;
    }
#line 2737 "acf_y_tab.c"
    break;

  case 118:
#line 1364 "acf.y"
    {
        if (parameter_attr.bit.heap)
            log_warning(yylineno, NIDL_MULATTRDEF);
        parameter_attr.bit.heap = TRUE;
    }
#line 2747 "acf_y_tab.c"
    break;

  case 119:
#line 1370 "acf.y"
    {
        if (parameter_attr.bit.in_line)
            log_warning(yylineno, NIDL_MULATTRDEF);
        parameter_attr.bit.in_line = TRUE;
    }
#line 2757 "acf_y_tab.c"
    break;

  case 120:
#line 1376 "acf.y"
    {
        if (parameter_attr.bit.out_of_line)
            log_warning(yylineno, NIDL_MULATTRDEF);
        parameter_attr.bit.out_of_line = TRUE;
    }
#line 2767 "acf_y_tab.c"
    break;

  case 121:
#line 1382 "acf.y"
    {
        if (NAMETABLE_add_id("cs_stag") == (yyvsp[0].y_id))
        {
            if (parameter_attr.bit.cs_stag)
                log_warning(yylineno, NIDL_MULATTRDEF);
            parameter_attr.bit.cs_stag = TRUE;
        }
        else if (NAMETABLE_add_id("cs_drtag") == (yyvsp[0].y_id))
        {
            if (parameter_attr.bit.cs_drtag)
                log_warning(yylineno, NIDL_MULATTRDEF);
            parameter_attr.bit.cs_drtag = TRUE;
        }
        else if (NAMETABLE_add_id("cs_rtag") == (yyvsp[0].y_id))
        {
            if (parameter_attr.bit.cs_rtag)
                log_warning(yylineno, NIDL_MULATTRDEF);
            parameter_attr.bit.cs_rtag = TRUE;
        }
        else
            log_error(yylineno, NIDL_ERRINATTR);
    }
#line 2794 "acf_y_tab.c"
    break;


#line 2798 "acf_y_tab.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = YY_CAST (char *, YYSTACK_ALLOC (YY_CAST (YYSIZE_T, yymsg_alloc)));
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[+*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 1417 "acf.y"



/***************************
 *  yacc programs section  *
 ***************************/

/*
 *  a c f _ i n i t
 *
 *  Function:   Called before ACF parsing to initialize variables.
 *
 */

void acf_init
#ifdef PROTO
(
    boolean     *cmd_opt_arr,   /* [in] Array of command option flags */
    void        **cmd_val_arr,  /* [in] Array of command option values */
    char        *acf_file       /* [in] ACF file name */
)
#else
(cmd_opt_arr, cmd_val_arr, acf_file)
    boolean     *cmd_opt_arr;   /* [in] Array of command option flags */
    void        **cmd_val_arr;  /* [in] Array of command option values */
    char        *acf_file;      /* [in] ACF file name */
#endif

{
    /* Save passed command array and interface node addrs in static storage. */
    cmd_opt = cmd_opt_arr;
    cmd_val = cmd_val_arr;

    /* Set global (STRTAB_str_t error_file_name_id) for error processing. */
    set_name_for_errors(acf_file);

    interface_attr.mask = 0;
    type_attr.mask      = 0;
    operation_attr.mask = 0;
    parameter_attr.mask = 0;

    interface_name      = NULL;
    type_name           = NULL;
    repr_type_name      = NULL;
    cs_char_type_name   = NULL;
    operation_name      = NULL;
    binding_callout_name= NULL;
    lookup_callout_name = NULL;
    malloc_callout_name = NULL;
    free_callout_name   = NULL;
    cs_tag_rtn_name     = NULL;

    include_list        = NULL;

    parameter_list      = NULL;
    parameter_free_list = NULL;

    malloc_callout_name = NULL;
    free_callout_name   = NULL;
}


/*
 *  a c f _ c l e a n u p
 *
 *  Function:   Called after ACF parsing to free allocated memory.
 *
 */

#ifdef PROTO
void acf_cleanup(void)
#else
void acf_cleanup()
#endif

{
    acf_param_t *p, *q;     /* Ptrs to parameter record */

    p = parameter_free_list;

    while (p != NULL)
    {
        q = p;
        p = p->next;
        FREE(q);
    }
}


/*
**  a c f _ e r r o r
**
**  Issues an error message, and bumps the error count.
**
**  Note:       This function is not prototyped since the way we use it allows
**              it to be called with 1 to 6 arguments.
*/

static void acf_error(msgid, arg1, arg2, arg3, arg4, arg5)
    long    msgid;              /* [in] Message id */
    char    *arg1;              /* [in] 0 to 5 arguments to fill in message */
    char    *arg2;              /*      directives */
    char    *arg3;
    char    *arg4;
    char    *arg5;

{
    log_error(acf_yylineno, msgid, arg1, arg2, arg3, arg4, arg5);
}


/*
**  a c f _ w a r n i n g
**
**  Issues a warning message.
**
**  Note:       This function is not prototyped since the way we use it allows
**              it to be called with 1 to 6 arguments.
*/

static void acf_warning(msgid, arg1, arg2, arg3, arg4, arg5)
    long    msgid;              /* [in] Message id */
    char    *arg1;              /* [in] 0 to 5 arguments to fill in message */
    char    *arg2;              /*      directives */
    char    *arg3;
    char    *arg4;
    char    *arg5;

{
    log_warning(acf_yylineno, msgid, arg1, arg2, arg3, arg4, arg5);
}


/*
**  l o o k u p _ e x c e p t i o n
**
**  Looks up a name in the nametable, and if it is bound to a valid exception
**  node, returns the address of the exception node.
**
**  Returns:    TRUE if lookup succeeds, FALSE otherwise.
*/

static boolean lookup_exception
#ifdef PROTO
(
    NAMETABLE_id_t  excep_id,     /* [in] Nametable id of exception name */
    boolean         log_error,    /* [in] TRUE => log error if name not found */
    AST_exception_n_t **excep_ptr /*[out] Ptr to AST exception node */
)
#else
(excep_id, log_error, excep_ptr)
    NAMETABLE_id_t  excep_id;     /* [in] Nametable id of exception name */
    boolean         log_error;    /* [in] TRUE => log error if name not found */
    AST_exception_n_t **excep_ptr;/*[out] Ptr to AST exception node */
#endif

{
    AST_exception_n_t *excep_p;     /* Ptr to node bound to looked up name */
    char            *perm_excep_name;   /* Ptr to permanent copy */

    if (excep_id != NAMETABLE_NIL_ID)
    {
        excep_p = (AST_exception_n_t *)NAMETABLE_lookup_binding(excep_id);

        if (excep_p != NULL && excep_p->fe_info->node_kind == fe_exception_n_k)
        {
            *excep_ptr = excep_p;
            return TRUE;
        }
    }

    if (log_error)
    {
        NAMETABLE_id_to_string(excep_id, &perm_excep_name);
        acf_error(NIDL_EXCNOTDEF, perm_excep_name);
    }

    *excep_ptr = NULL;
    return FALSE;
}


/*
**  l o o k u p _ t y p e
**
**  Looks up a name in the nametable, and if it is bound to a valid type
**  node, returns the address of the type node.
**
**  Returns:    TRUE if lookup succeeds, FALSE otherwise.
*/

static boolean lookup_type
#ifdef PROTO
(
    char            *type_name, /* [in] Name to look up */
    boolean         log_error,  /* [in] TRUE => log error if name not found */
    NAMETABLE_id_t  *type_id,   /*[out] Nametable id of type name */
    AST_type_n_t    **type_ptr  /*[out] Ptr to AST type node */
)
#else
(type_name, log_error, type_id, type_ptr)
    char            *type_name; /* [in] Name to look up */
    boolean         log_error;  /* [in] TRUE => log error if name not found */
    NAMETABLE_id_t  *type_id;   /*[out] Nametable id of type name */
    AST_type_n_t    **type_ptr; /*[out] Ptr to AST type node */
#endif

{
    AST_type_n_t    *type_p;    /* Ptr to node bound to looked up name */
    char            *perm_type_name;    /* Ptr to permanent copy */
    NAMETABLE_id_t  name_id;            /* Handle on permanent copy */

    *type_id = NAMETABLE_lookup_id(type_name);

    if (*type_id != NAMETABLE_NIL_ID)
    {
        type_p = (AST_type_n_t *)NAMETABLE_lookup_binding(*type_id);

        if (type_p != NULL && type_p->fe_info->node_kind == fe_type_n_k)
        {
            *type_ptr = type_p;
            return TRUE;
        }
    }

    if (log_error)
    {
        name_id = NAMETABLE_add_id(type_name);
        NAMETABLE_id_to_string(name_id, &perm_type_name);
        acf_error(NIDL_TYPNOTDEF, perm_type_name);
    }

    *type_ptr = NULL;
    return FALSE;
}


/*
**  l o o k u p _ o p e r a t i o n
**
**  Looks up a name in the nametable, and if it is bound to a valid operation
**  node, returns the address of the operation node.
**
**  Returns:    TRUE if lookup succeeds, FALSE otherwise.
*/

static boolean lookup_operation
#ifdef PROTO
(
    char            *op_name,   /* [in] Name to look up */
    boolean         log_error,  /* [in] TRUE => log error if name not found */
    NAMETABLE_id_t  *op_id,     /*[out] Nametable id of operation name */
    AST_operation_n_t **op_ptr  /*[out] Ptr to AST operation node */
)
#else
(op_name, log_error, op_id, op_ptr)
    char            *op_name;   /* [in] Name to look up */
    boolean         log_error;  /* [in] TRUE => log error if name not found */
    NAMETABLE_id_t  *op_id;     /*[out] Nametable id of operation name */
    AST_operation_n_t **op_ptr; /*[out] Ptr to AST operation node */
#endif

{
    AST_operation_n_t   *op_p;  /* Ptr to node bound to looked up name */
    char            *perm_op_name;      /* Ptr to permanent copy */
    NAMETABLE_id_t  name_id;            /* Handle on permanent copy */

    *op_id = NAMETABLE_lookup_id(op_name);

    if (*op_id != NAMETABLE_NIL_ID)
    {
        op_p = (AST_operation_n_t *)NAMETABLE_lookup_binding(*op_id);

        if (op_p != NULL && op_p->fe_info->node_kind == fe_operation_n_k)
        {
            *op_ptr = op_p;
            return TRUE;
        }
    }

    if (log_error)
    {
        name_id = NAMETABLE_add_id(op_name);
        NAMETABLE_id_to_string(name_id, &perm_op_name);
        acf_error(NIDL_OPNOTDEF, perm_op_name);
    }

    *op_ptr = NULL;
    return FALSE;
}


/*
**  l o o k u p _ p a r a m e t e r
**
**  Searches an operation node's parameter list for the parameter name passed.
**  If found, returns the address of the parameter node.
**
**  Returns:    TRUE if lookup succeeds, FALSE otherwise.
*/

static boolean lookup_parameter
#ifdef PROTO
(
    AST_operation_n_t   *op_p,          /* [in] Ptr to AST operation node */
    char                *param_name,    /* [in] Parameter name to look up */
    boolean             log_error,      /* [in] TRUE=> log error if not found */
    NAMETABLE_id_t      *param_id,      /*[out] Nametable id of param name */
    AST_parameter_n_t   **param_ptr     /*[out] Ptr to AST parameter node */
)
#else
(op_p, param_name, log_error, param_id, param_ptr)
    AST_operation_n_t   *op_p;          /* [in] Ptr to AST operation node */
    char                *param_name;    /* [in] Parameter name to look up */
    boolean             log_error;      /* [in] TRUE=> log error if not found */
    NAMETABLE_id_t      *param_id;      /*[out] Nametable id of param name */
    AST_parameter_n_t   **param_ptr;    /*[out] Ptr to AST parameter node */
#endif

{
    AST_parameter_n_t   *param_p;       /* Ptr to operation parameter node */
    char                *op_param_name; /* Name of an operation parameter */
    char                *op_name;       /* Operation name */
    char            *perm_param_name;   /* Ptr to permanent copy */
    NAMETABLE_id_t  name_id;            /* Handle on permanent copy */

    for (param_p = op_p->parameters ; param_p != NULL ; param_p = param_p->next)
    {
        NAMETABLE_id_to_string(param_p->name, &op_param_name);

        if (strcmp(param_name, op_param_name) == 0)
        {
            *param_id   = param_p->name;
            *param_ptr  = param_p;
            return TRUE;
        }
    }

    if (log_error)
    {
        char    *file_name;     /* Related file name */

        NAMETABLE_id_to_string(op_p->name, &op_name);
        name_id = NAMETABLE_add_id(param_name);
        NAMETABLE_id_to_string(name_id, &perm_param_name);

        STRTAB_str_to_string(op_p->fe_info->file, &file_name);

        acf_error(NIDL_PRMNOTDEF, perm_param_name, op_name);
        acf_error(NIDL_NAMEDECLAT, op_name, file_name,
                  op_p->fe_info->source_line);
    }

    return FALSE;
}


/*
**  l o o k u p _ r e p _ a s _ n a m e
**
**  Scans a list of type nodes that have represent_as types for a match with
**  the type name given by the parameter repr_name_id.  If so, returns the
**  address of the found type node and a pointer to the associated
**  represent_as type name.
**
**  Returns:    TRUE if lookup succeeds, FALSE otherwise.
*/

static boolean lookup_rep_as_name
#ifdef PROTO
(
    AST_type_p_n_t  *typep_p,           /* [in] Listhead of type ptr nodes */
    NAMETABLE_id_t  repr_name_id,       /* [in] represent_as name to look up */
    AST_type_n_t    **ret_type_p,       /*[out] Type node if found */
    char            **ret_type_name     /*[out] Type name if found */
)
#else
(typep_p, repr_name_id, ret_type_p, ret_type_name)
    AST_type_p_n_t  *typep_p;           /* [in] Listhead of type ptr nodes */
    NAMETABLE_id_t  repr_name_id;       /* [in] represent_as name to look up */
    AST_type_n_t    **ret_type_p;       /*[out] Type node if found */
    char            **ret_type_name;    /*[out] Type name if found */
#endif

{
    AST_type_n_t    *type_p;            /* Ptr to a type node */

    for ( ; typep_p != NULL ; typep_p = typep_p->next )
    {
        type_p = typep_p->type;
        if (type_p->name == repr_name_id)
        {
            *ret_type_p = type_p;
            NAMETABLE_id_to_string(type_p->rep_as_type->type_name,
                                   ret_type_name);
            return TRUE;
        }
    }

    return FALSE;
}


/*
**  l o o k u p _ c s _ c h a r _ n a m e
**
**  Scans a list of type nodes that have cs_char types for a match with
**  the type name given by the parameter cs_char_name_id.  If so, returns the
**  address of the found type node and a pointer to the associated
**  cs_char type name.
**
**  Returns:    TRUE if lookup succeeds, FALSE otherwise.
*/

static boolean lookup_cs_char_name
#ifdef PROTO
(
    AST_type_p_n_t  *typep_p,           /* [in] Listhead of type ptr nodes */
    NAMETABLE_id_t  cs_char_name_id,    /* [in] cs_char name to look up */
    AST_type_n_t    **ret_type_p,       /*[out] Type node if found */
    char            **ret_type_name     /*[out] Type name if found */
)
#else
(typep_p, cs_char_name_id, ret_type_p, ret_type_name)
    AST_type_p_n_t  *typep_p;           /* [in] Listhead of type ptr nodes */
    NAMETABLE_id_t  cs_char_name_id;    /* [in] cs_char name to look up */
    AST_type_n_t    **ret_type_p;       /*[out] Type node if found */
    char            **ret_type_name;    /*[out] Type name if found */
#endif

{
    AST_type_n_t    *type_p;            /* Ptr to a type node */

    for ( ; typep_p != NULL ; typep_p = typep_p->next )
    {
        type_p = typep_p->type;
        if (type_p->name == cs_char_name_id)
        {
            *ret_type_p = type_p;
            NAMETABLE_id_to_string(type_p->cs_char_type->type_name,
                                   ret_type_name);
            return TRUE;
        }
    }

    return FALSE;
}


/*
 *  a c f _ a l l o c _ p a r a m
 *
 *  Function:   Allocates an acf_param_t, either from the free list or heap.
 *
 *  Returns:    Address of acf_param_t
 *
 *  Globals:    parameter_free_list - listhead for free list
 *
 *  Side Effects:   Exits program if unable to allocate memory.
 */

#ifdef PROTO
static acf_param_t *alloc_param(void)
#else
static acf_param_t *alloc_param()
#endif

{
    acf_param_t *p;     /* Ptr to parameter record */

    if (parameter_free_list != NULL)
    {
        p = parameter_free_list;
        parameter_free_list = parameter_free_list->next;
    }
    else
    {
        p = (acf_param_t *)MALLOC(sizeof(acf_param_t));
        p->next                 = NULL;
        p->parameter_attr.mask  = 0;
        p->param_id             = NAMETABLE_NIL_ID;
    }

    return p;
}


/*
 *  a c f _ f r e e _ p a r a m
 *
 *  Function:   Frees an acf_param_t by reinitilizing it and returning it to
 *              the head of the free list.
 *
 *  Input:      p - Pointer to acf_param_t record
 *
 *  Globals:    parameter_free_list - listhead for free list
 */

static void free_param
#ifdef PROTO
(
    acf_param_t *p              /* [in] Pointer to acf_param_t record */
)
#else
(p)
    acf_param_t *p;             /* [in] Pointer to acf_param_t record */
#endif

{
    p->parameter_attr.mask  = 0;
    p->param_id             = NAMETABLE_NIL_ID;

    p->next                 = parameter_free_list;
    parameter_free_list     = p;
}


/*
 *  a c f _ f r e e _ p a r a m _ l i s t
 *
 *  Function:   Frees a list of acf_param_t records.
 *
 *  Input:      list - Address of list pointer
 *
 *  Output:     list pointer = NULL
 */

static void free_param_list
#ifdef PROTO
(
    acf_param_t **list          /* [in] Address of list pointer */
)
#else
(list)
    acf_param_t **list;         /* [in] Address of list pointer */
#endif

{
    acf_param_t *p, *q;     /* Ptrs to parameter record */

    p = *list;

    while (p != NULL)
    {
        q = p;
        p = p->next;
        free_param(q);
    }

    *list = NULL;            /* List now empty */
}


/*
 *  a d d _ p a r a m _ t o _ l i s t
 *
 *  Function:   Add a acf_param_t record to the end of a list.
 *
 *  Inputs:     p - Pointer to parameter record
 *              list - Address of list pointer
 *
 *  Outputs:    List is modified.
 */

void add_param_to_list
#ifdef PROTO
(
    acf_param_t *p,             /* [in] Pointer to parameter record */
    acf_param_t **list          /* [in] Address of list pointer */
)
#else
(p, list)
    acf_param_t *p;             /* [in] Pointer to parameter record */
    acf_param_t **list;         /* [in] Address of list pointer */
#endif

{
    acf_param_t *q;         /* Ptr to parameter record */

    if (*list == NULL)      /* If list empty */
        *list = p;          /* then list now points at param */
    else
    {
        for (q = *list ; q->next != NULL ; q = q->next)
            ;
        q->next = p;        /* else last record in list now points at param */
    }

    p->next = NULL;         /* Param is now last in list */
}


/*
**  a p p e n d _ p a r a m e t e r
**
**  Appends a parameter to an operation's parameter list.
*/

static void append_parameter
#ifdef PROTO
(
    AST_operation_n_t   *op_p,          /* [in] Ptr to AST operation node */
    char                *param_name,    /* [in] Parameter name */
    acf_attrib_t        *param_attr     /* [in] Parameter attributes */
)
#else
(op_p, param_name, param_attr)
    AST_operation_n_t   *op_p;          /* [in] Ptr to AST operation node */
    char                *param_name;    /* [in] Parameter name */
    acf_attrib_t        *param_attr;    /* [in] Parameter attributes */
#endif

{
    NAMETABLE_id_t      new_param_id;   /* Nametable id of new parameter name */
    AST_parameter_n_t   *new_param_p;   /* Ptr to new parameter node */
    AST_type_n_t        *new_type_p;    /* Ptr to new parameter type node */
    AST_pointer_n_t     *new_ptr_p;     /* Ptr to new pointer node */
    NAMETABLE_id_t      status_id;      /* Nametable id of status_t */
    AST_type_n_t        *status_type_p; /* Type node bound to status_t name */
    AST_parameter_n_t   *param_p;       /* Ptr to operation parameter node */

    /* Look up error_status_t type. */
    status_id = NAMETABLE_add_id("error_status_t");
    status_type_p = (AST_type_n_t *)NAMETABLE_lookup_binding(status_id);
    if (status_type_p == NULL)
    {
        acf_error(NIDL_ERRSTATDEF, "error_status_t", "nbase.idl");
        return;
    }

    /*
     * Have to create an '[out] error_status_t *param_name' parameter
     * that has the specified parameter attributes.
     */
    new_param_id = NAMETABLE_add_id(param_name);
    new_param_p = AST_parameter_node(new_param_id);
    new_type_p  = AST_type_node(AST_pointer_k);
    new_ptr_p   = AST_pointer_node(status_type_p);

    new_type_p->type_structure.pointer = new_ptr_p;
    AST_SET_REF(new_type_p);

    new_param_p->name = new_param_id;
    new_param_p->type = new_type_p;
    new_param_p->uplink = op_p;
    if (param_attr->bit.comm_status)
        AST_SET_ADD_COMM_STATUS(new_param_p);
    if (param_attr->bit.fault_status)
        AST_SET_ADD_FAULT_STATUS(new_param_p);
    AST_SET_OUT(new_param_p);
    AST_SET_REF(new_param_p);

    param_p = op_p->parameters;
    if (param_p == NULL)
    {
        /* Was null param list, now has one param. */
        op_p->parameters = new_param_p;
    }
    else if (param_p->last == NULL)
    {
        /* Was one param, now have two params. */
        param_p->next = new_param_p;
        param_p->last = new_param_p;
    }
    else
    {
        /* Was more than one param, now have one more. */
        param_p->last->next = new_param_p;
        param_p->last = new_param_p;
    }
}


/*
**  p r o c e s s _ r e p _ a s _ t y p e
**
**  Processes a [represent_as] clause applied to a type.  Validates that
**  [represent_as] types are not nested.  Adds the type to a list of types
**  that have the [represent_as] attribute.
*/

static void process_rep_as_type
#ifdef PROTO
(
    AST_interface_n_t   *int_p,     /* [in] Ptr to AST interface node */
    AST_type_n_t        *type_p,    /* [in] Ptr to AST type node */
    char            *ref_type_name  /* [in] Name in represent_as() clause */
)
#else
(int_p, type_p, ref_name)
    AST_interface_n_t   *int_p;     /* [in] Ptr to AST interface node */
    AST_type_n_t        *type_p;    /* [in] Ptr to AST type node */
    char            *ref_type_name; /* [in] Name in represent_as() clause */
#endif

{
    NAMETABLE_id_t  ref_type_id;    /* Nametable id of referenced name */
    char            *file_name;     /* Related file name */
    char            *perm_name;     /* Permanent copy of referenced name */
    AST_type_n_t    *parent_type_p; /* Parent type with same attribute */
    char            *parent_name;   /* Name of parent type */

    ref_type_id = NAMETABLE_add_id(ref_type_name);

    /*
     * Report error if the type name referenced in the attribute is an AST
     * type which also has the same attribute, i.e. types with this attribute
     * cannot nest.
     */
    if (lookup_rep_as_name(int_p->ra_types, ref_type_id, &parent_type_p,
                           &perm_name))
    {
        NAMETABLE_id_to_string(parent_type_p->name, &parent_name);
        STRTAB_str_to_string(parent_type_p->fe_info->acf_file, &file_name);

        acf_error(NIDL_REPASNEST);
        acf_error(NIDL_TYPEREPAS, parent_name, perm_name);
        acf_error(NIDL_NAMEDECLAT, parent_name, file_name,
                  parent_type_p->fe_info->acf_source_line);
    }

    /*
     * If the type node already has a type name for this attribute,
     * this one must duplicate that same name.
     */
    if (type_p->rep_as_type != NULL)
    {
        NAMETABLE_id_to_string(type_p->rep_as_type->type_name, &perm_name);

        if (strcmp(perm_name, ref_type_name) != 0)
        {
            char    *new_ref_type_name; /* Ptr to permanent copy */
            NAMETABLE_id_t  name_id;    /* Handle on perm copy */

            name_id = NAMETABLE_add_id(ref_type_name);
            NAMETABLE_id_to_string(name_id, &new_ref_type_name);

            STRTAB_str_to_string(
                            type_p->rep_as_type->fe_info->acf_file, &file_name);

            acf_error(NIDL_CONFREPRTYPE, new_ref_type_name, perm_name);
            acf_error(NIDL_NAMEDECLAT, perm_name, file_name,
                      type_p->rep_as_type->fe_info->acf_source_line);
        }
    }
    else
    {
        /*
         * Process valid [represent_as] clause.
         */
        AST_type_p_n_t  *typep_p;       /* Used to link type nodes */
        AST_rep_as_n_t  *repas_p;       /* Ptr to represent_as node */

        /* Add represent_as type name and build rep_as AST node. */

        repas_p = type_p->rep_as_type = AST_represent_as_node(ref_type_id);
        /* Store source information. */
        if (repas_p->fe_info != NULL)
        {
            repas_p->fe_info->acf_file = error_file_name_id;
            repas_p->fe_info->acf_source_line = acf_yylineno;
        }

        /* Check for associated def-as-tag node. */

        if (type_p->fe_info->tag_ptr != NULL)
            type_p->fe_info->tag_ptr->rep_as_type = type_p->rep_as_type;

        /* Link type node into list of represent_as types. */

        typep_p = AST_type_ptr_node();
        typep_p->type = type_p;

        int_p->ra_types = (AST_type_p_n_t *)AST_concat_element(
                                                (ASTP_node_t *)int_p->ra_types,
                                                (ASTP_node_t *)typep_p);
    }
}


/*
**  p r o c e s s _ c s _ c h a r _ t y p e
**
**  Processes a [cs_char] clause applied to a type.  Validates that
**  [cs_char] types are not nested.  Adds the type to a list of types
**  that have the [cs_char] attribute.
*/

static void process_cs_char_type
#ifdef PROTO
(
    AST_interface_n_t   *int_p,     /* [in] Ptr to AST interface node */
    AST_type_n_t        *type_p,    /* [in] Ptr to AST type node */
    char            *ref_type_name  /* [in] Name in cs_char() clause */
)
#else
(int_p, type_p, ref_name)
    AST_interface_n_t   *int_p;     /* [in] Ptr to AST interface node */
    AST_type_n_t        *type_p;    /* [in] Ptr to AST type node */
    char            *ref_type_name; /* [in] Name in cs_char() clause */
#endif

{
    NAMETABLE_id_t  ref_type_id;    /* Nametable id of referenced name */
    char            *file_name;     /* Related file name */
    char            *perm_name;     /* Permanent copy of referenced name */
    AST_type_n_t    *parent_type_p; /* Parent type with same attribute */
    char            *parent_name;   /* Name of parent type */

    ref_type_id = NAMETABLE_add_id(ref_type_name);

    /*
     * Report error if the type name referenced in the attribute is an AST
     * type which also has the same attribute, i.e. types with this attribute
     * cannot nest.
     */
    if (lookup_cs_char_name(int_p->cs_types, ref_type_id, &parent_type_p,
                            &perm_name))
    {
        NAMETABLE_id_to_string(parent_type_p->name, &parent_name);
        STRTAB_str_to_string(parent_type_p->fe_info->acf_file, &file_name);

        /*** This needs updating ***/
        acf_error(NIDL_REPASNEST);
        acf_error(NIDL_TYPEREPAS, parent_name, perm_name);
        acf_error(NIDL_NAMEDECLAT, parent_name, file_name,
                  parent_type_p->fe_info->acf_source_line);
    }

    /*
     * If the type node already has a type name for this attribute,
     * this one must duplicate that same name.
     */
    if (type_p->cs_char_type != NULL)
    {
        NAMETABLE_id_to_string(type_p->cs_char_type->type_name, &perm_name);

        if (strcmp(perm_name, ref_type_name) != 0)
        {
            char    *new_ref_type_name; /* Ptr to permanent copy */
            NAMETABLE_id_t  name_id;    /* Handle on perm copy */

            name_id = NAMETABLE_add_id(ref_type_name);
            NAMETABLE_id_to_string(name_id, &new_ref_type_name);

            STRTAB_str_to_string(
                        type_p->cs_char_type->fe_info->acf_file, &file_name);

            /*** This needs updating ***/
            acf_error(NIDL_CONFREPRTYPE, new_ref_type_name, perm_name);
            acf_error(NIDL_NAMEDECLAT, perm_name, file_name,
                      type_p->cs_char_type->fe_info->acf_source_line);
        }
    }
    else
    {
        /*
         * Process valid [cs_char] clause.
         */
        AST_type_p_n_t  *typep_p;       /* Used to link type nodes */
        AST_cs_char_n_t *cschar_p;      /* Ptr to cs_char node */

        /* Add cs_char type name and build cs_char AST node. */

        cschar_p = type_p->cs_char_type = AST_cs_char_node(ref_type_id);
        /* Store source information. */
        if (cschar_p->fe_info != NULL)
        {
            cschar_p->fe_info->acf_file = error_file_name_id;
            cschar_p->fe_info->acf_source_line = acf_yylineno;
        }

        /* Check for associated def-as-tag node. */

        if (type_p->fe_info->tag_ptr != NULL)
            type_p->fe_info->tag_ptr->cs_char_type = type_p->cs_char_type;

        /* Link type node into list of cs_char types. */

        typep_p = AST_type_ptr_node();
        typep_p->type = type_p;

        int_p->cs_types = (AST_type_p_n_t *)AST_concat_element(
                                                (ASTP_node_t *)int_p->cs_types,
                                                (ASTP_node_t *)typep_p);
    }
}


#ifdef DUMPERS
/*
 *  d u m p _ a t t r i b u t e s
 *
 *  Function:   Prints list of attributes parsed for a particular node type
 *
 *  Inputs:     header_text - Initial text before node name and attributes
 *              node_name   - Name of interface, type, operation, or parameter
 *              node_attr_p - Address of node attributes structure
 *
 *  Globals:    repr_type_name  - represent_as type name, used if bit is set
 *              cs_char_type_name - cs_char type name, used if bit is set
 *              cs_tag_rtn_name - cs_tag_rtn name, used if bit is set
 *              cxx_static_name - cxx_static name, used if bit is set
 *              binding_callout_name - binding_callout name, used if bit is set
 *              lookup_callout_name - cxx_lookup name, used if bit is set
 *              malloc_callout_name - client_memory malloc name, used if bit set
 *              free_callout_name - client_memory free name, used if bit set
 */

static void dump_attributes
#ifdef PROTO
(
    char            *header_text,       /* [in] Initial output text */
    char            *node_name,         /* [in] Name of tree node */
    acf_attrib_t    *node_attr_p        /* [in] Node attributes ptr */
)
#else
(header_text, node_name, node_attr_p)
    char            *header_text;       /* [in] Initial output text */
    char            *node_name;         /* [in] Name of tree node */
    acf_attrib_t    *node_attr_p;       /* [in] Node attributes ptr */
#endif

#define MAX_ATTR_TEXT   1024    /* Big enough for lots of extern_exceptions */

{
    char            attr_text[MAX_ATTR_TEXT];   /* Buf for formatting attrs */
    int             pos;                /* Position in buffer */
    acf_attrib_t    node_attr;          /* Node attributes */

    node_attr = *node_attr_p;

    printf("%s %s", header_text, node_name);

    if (node_attr.mask == 0)
        printf("\n");
    else
    {
        printf(" attributes: ");
        strcpy(attr_text, "[");

        if (node_attr.bit.auto_handle)
            strcat(attr_text, "auto_handle, ");
        if (node_attr.bit.code)
            strcat(attr_text, "code, ");
        if (node_attr.bit.nocode)
            strcat(attr_text, "nocode, ");
        if (node_attr.bit.comm_status)
            strcat(attr_text, "comm_status, ");
        if (node_attr.bit.decode)
            strcat(attr_text, "decode, ");
        if (node_attr.bit.enable_allocate)
            strcat(attr_text, "enable_allocate, ");
        if (node_attr.bit.encode)
            strcat(attr_text, "encode, ");
        if (node_attr.bit.explicit_handle)
            strcat(attr_text, "explicit_handle, ");
        if (node_attr.bit.extern_exceps && ASTP_parsing_main_idl)
        {
            AST_exception_n_t   *excep_p;
            char                *name;
            strcat(attr_text, "extern_exceptions(");
            for (excep_p = the_interface->exceptions;
                 excep_p != NULL;
                 excep_p = excep_p->next)
            {
                if (AST_EXTERN_SET(excep_p))
                {
                    NAMETABLE_id_to_string(excep_p->name, &name);
                    strcat(attr_text, name);
                    strcat(attr_text, ",");
                }
            }
            attr_text[strlen(attr_text)-1] = '\0';  /* overwrite trailing ',' */
            strcat(attr_text, "), ");
        }
        if (node_attr.bit.fault_status)
            strcat(attr_text, "fault_status, ");
        if (node_attr.bit.heap)
            strcat(attr_text, "heap, ");
        if (node_attr.bit.implicit_handle)
            strcat(attr_text, "implicit_handle, ");
        if (node_attr.bit.in_line)
            strcat(attr_text, "in_line, ");
        if (node_attr.bit.out_of_line)
            strcat(attr_text, "out_of_line, ");
        if (node_attr.bit.cs_stag)
            strcat(attr_text, "cs_stag, ");
        if (node_attr.bit.cs_drtag)
            strcat(attr_text, "cs_drtag, ");
        if (node_attr.bit.cs_rtag)
            strcat(attr_text, "cs_rtag, ");
        if (node_attr.bit.represent_as)
        {
            strcat(attr_text, "represent_as(");
            strcat(attr_text, repr_type_name);
            strcat(attr_text, "), ");
        }
        if (node_attr.bit.cs_char)
        {
            strcat(attr_text, "cs_char(");
            strcat(attr_text, cs_char_type_name);
            strcat(attr_text, "), ");
        }
        if (node_attr.bit.cs_tag_rtn)
        {
            strcat(attr_text, "cs_tag_rtn(");
            strcat(attr_text, cs_tag_rtn_name);
            strcat(attr_text, "), ");
        }
        if (node_attr.bit.binding_callout)
        {
            strcat(attr_text, "binding_callout(");
            strcat(attr_text, binding_callout_name);
            strcat(attr_text, "), ");
        }
        if (node_attr.bit.lookup_callout)
        {
            strcat(attr_text, "lookup_callout(");
            strcat(attr_text, lookup_callout_name);
            strcat(attr_text, "), ");
        }
        if (node_attr.bit.malloc_callout_name)
        {
            strcat(attr_text, "client_memory(");
            strcat(attr_text, malloc_callout_name);
            strcat(attr_text, "), ");
        }
        if (node_attr.bit.free_callout_name)
        {
            strcat(attr_text, "client_memory(");
            strcat(attr_text, free_callout_name);
            strcat(attr_text, "), ");
        }
        if (node_attr.bit.cxx_static)
        {
            strcat(attr_text, "cxx_static");
            if (cxx_static_name != NULL)
            {
                strcat(attr_text, "(");
                strcat(attr_text, cxx_static_name);
                strcat(attr_text, ")");
            }
            strcat(attr_text, ", ");
        }
        if (node_attr.bit.cxx_new)
        {
            strcat(attr_text, "cxx_new");
            if (cxx_new_name != NULL)
            {
                strcat(attr_text, "(");
                strcat(attr_text, cxx_new_name);
                strcat(attr_text, ")");
            }
            strcat(attr_text, ", ");
        }

        /* Overwrite trailing ", " with "]" */

        pos = strlen(attr_text) - strlen(", ");
        attr_text[pos] = ']';
        attr_text[pos+1] = '\0';

        printf("%s\n", attr_text);
    }
}
#endif
