#!/bin/python
import subprocess

'''
The purpose of this script is to build the external dependencies for the idl compiler.
It hard codes things, but I don't expect them to change, so if they do in the future,
someone else can fix it to be more general.

What are we building:

(1) The yacc grammar for ACF files (acf_y_tab.c acf_y_tab.h)
(2) The lex parser for ACF files (acf_lex_yy.c)
(3) The messages used by the IDL compiler (default_msg.h)
(4) The message _catalog_ (idl.cat)
(5) The yacc grammar for IDL (nidl_y_tab.c nidl_y_tab.h)
(6) The lex parser for IDL (nid_lex_yy.c)
'''


def run(args: list, stdout=None):
    if stdout != None:
        r = subprocess.run(args, stdout=stdout)
    else:
        r = subprocess.run(args)
    if (0 != r.returncode):
        print('Command <{}> failed with status {}'.format(
            ' '.join(args), r.returncode))
        exit(r.returncode)


run(['bison', '--defines=acf_y_tab.h', '--output=acf_y_tab.c', 'acf.y'])
run(['flex', '-o', 'acf_lex_yy_i.c', 'acf.l'])
with open('default_msg.h', 'wt') as fd:
    run(['sed', '-e', '/^\$/d;/^$/d;s/^[^ ]* /"/;s/$/",/;', 'nidlmsg.m'], stdout=fd)
run(['gencat', 'idl.cat', 'nidlmsg.m'])
run(['bison', '--defines=nidl_y_tab.h', '--output=nidl_y_tab.c', 'nidl.y'])
run(['flex', '-o', 'nidl_lex_yy_i.c', 'nidl.l'])
