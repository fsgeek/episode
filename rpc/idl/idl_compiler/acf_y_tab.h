/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

#ifndef YY_YY_ACF_Y_TAB_H_INCLUDED
# define YY_YY_ACF_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    LONG_KW = 258,
    AUTO_HANDLE_KW = 259,
    BINDING_CALLOUT_KW = 260,
    CODE_KW = 261,
    COMM_STATUS_KW = 262,
    CS_CHAR_KW = 263,
    CS_TAG_RTN_KW = 264,
    CXX_STATIC_KW = 265,
    ENABLE_ALLOCATE_KW = 266,
    EXPLICIT_HANDLE_KW = 267,
    EXTERN_EXCEPS_KW = 268,
    FAULT_STATUS_KW = 269,
    HANDLE_T_KW = 270,
    HEAP_KW = 271,
    IMPLICIT_HANDLE_KW = 272,
    INCLUDE_KW = 273,
    SSTUB_KW = 274,
    CSTUB_KW = 275,
    INTERFACE_KW = 276,
    IN_LINE_KW = 277,
    NOCODE_KW = 278,
    OUT_OF_LINE_KW = 279,
    REPRESENT_AS_KW = 280,
    TYPEDEF_KW = 281,
    CXX_LOOKUP_KW = 282,
    CLIENT_MEMORY_KW = 283,
    CXX_DELEGATE_KW = 284,
    CXX_NEW_KW = 285,
    COMMA = 286,
    LBRACE = 287,
    LBRACKET = 288,
    LPAREN = 289,
    RBRACE = 290,
    RBRACKET = 291,
    RPAREN = 292,
    SEMI = 293,
    TILDE = 294,
    UNKNOWN = 295,
    IDENTIFIER = 296,
    CXX_IDENTIFIER = 297,
    STRING = 298,
    INTEGER_NUMERIC = 299
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 244 "acf.y"

    NAMETABLE_id_t  y_id;       /* Identifier */
    STRTAB_str_t    y_string;   /* Text string */
    long      	    y_ival ;        /* Integer constant     */
#ifdef SNI_SVR4
    STRTAB_str_t           y_float ;       /* Float constant       */
    AST_export_n_t*        y_export ;      /* an export node       */
    AST_import_n_t*        y_import ;      /* Import node          */
    AST_constant_n_t*      y_constant;     /* Constant node        */
    AST_parameter_n_t*     y_parameter ;   /* Parameter node       */
    AST_type_n_t*          y_type ;        /* Type node            */
    AST_type_p_n_t*        y_type_ptr ;    /* Type pointer node    */
    AST_field_n_t*         y_field ;       /* Field node           */
    AST_arm_n_t*           y_arm ;         /* Union variant arm    */
    AST_operation_n_t*     y_operation ;   /* Routine node         */
    AST_interface_n_t*     y_interface ;   /* Interface node       */
    AST_case_label_n_t*    y_label ;       /* Union tags           */
    ASTP_declarator_n_t*   y_declarator ;  /* Declarator info      */
    ASTP_array_index_n_t*  y_index ;       /* Array index info     */
    nidl_uuid_t            y_uuid ;        /* Universal UID        */
    char                   y_char;         /* character constant   */
    ASTP_attributes_t      y_attributes;   /* attributes flags     */
    struct {
        AST_type_k_t    int_size;
        int             int_signed;
        }                  y_int_info;     /* int size and signedness */
    ASTP_exp_n_t           y_exp;          /* constant expression info */
#endif /*SNI_SVR4*/

#line 132 "acf_y_tab.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_ACF_Y_TAB_H_INCLUDED  */
