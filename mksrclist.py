#!/usr/bin/python

import os
import sys


def extract_files(all_files: list, suffix: str):
    return [x for x in all_files if x.endswith(suffix)]


def print_files(files: list, label: str):
    if len(files) > 0:
        print('{}={}\n'.format(label, files))


all_files = os.listdir('.')
c_files = extract_files(all_files, '.c')
h_files = extract_files(all_files, '.h')
et_files = extract_files(all_files, '.et')
y_files = extract_files(all_files, '.y')
l_files = extract_files(all_files, '.l')

print_files(c_files, 'SOURCES')
print_files(h_files, 'HEADERS')
print_files(et_files, 'ERROR_TABLE')
print_files(y_files, 'YACC_FILES')
print_files(l_files, 'LEX_FILES')
