/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 */

/* volume_table.h -- contains the external interface to the volume table
 *     module. */

/* Copyright (C) 1991, 1990 Transarc Corporation - All rights reserved */

#ifndef TRANSARC_EPI_VOLUME_TABLE_H
#define TRANSARC_EPI_VOLUME_TABLE_H

#include <epi_anode.h>

/* For specifying type of index to allocate. */

/* SHARED */
int epit_useOldFidIndex; /* support old fid indexing */

#define EPIT_TYPE_DONTCARE 0
#define EPIT_TYPE_FILE 1
#define EPIT_TYPE_AUX 2
#define EPIT_DONT_WITHHOLD 4

/* External procedure definitions */

EXPORT long epit_Init _TAKES((void));

EXPORT long epit_InitVolumeTable _TAKES((
    IN epi_anode_t volC,
    IN u_int reservedIndexes));

EXPORT long epit_Allocate _TAKES((
    IN buffer_tranRec_t trans,
    IN epi_anode_t volC,
    IN int flags,      /* type of index; use withholding */
    IN u_long nearest, /* nearby index, if possible */
    OUT u_long *indexP));

EXPORT long epit_Free _TAKES((
    IN buffer_tranRec_t trans,
    IN epi_anode_t volC,
    IN u_long index));

EXPORT long epit_EnumerateVolumeTable _TAKES((
    IN epi_anode_t volC,
    IN long (*proc)(void *rock, void *, unsigned long), /* call for each entry */
    IN char *rock));                                    /* pass through parameter */

EXPORT long epit_Create _TAKES((
    IN buffer_tranRec_t trans,
    IN epi_anode_t volC,
    IN u_long reservedIndexes));

EXPORT long epit_Delete _TAKES((
    IN elbb_tranRec_t trans,
    IN epi_anode_t volC,
    IN u_int reservedIndexes));

EXPORT long epit_CreateAVL _TAKES((
    IN struct async_device * device,
    IN daddr_t avlAnodeBlock,
    IN u_long reservedIndexes,
    OUT epi_anode_t *avlP));

EXPORT long epit_LastIndex _TAKES((
    IN epi_anode_t volC,
    OUT u_long *indexP));

EXPORT long epit_Deflate _TAKES((
    IN elbb_tranRec_t trans,
    IN epi_anode_t volC));

#endif
