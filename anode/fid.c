/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 */

/* fid.c -- provides functions to map between files indexes in fids and
 *     epiv-style volume indexes.  The important feature of the mapping being
 *     that it is sparce and leaves indexes corresponding to every other page
 *     of the volume table unused.  These indexes can then be used for auxilary
 *     containers such as acls.
 *
 *     The kernel of this idea is due to Bob Sidebotham. */

/* Copyright (C) 1992, 1991 Transarc Corporation - All rights reserved. */

#include <dcedfs_param.h>
#include <dcedfs_stds.h>
#include <dcedfs_osi.h>
#include <dcedfs_debug.h>

RCSID("$Header: /u0/rcs_trees/dce/rcs/file/episode/anode/fid.c,v 1.1.107.1 1996/10/02 17:17:14 damon Exp $")

#include <epi_anode.h>
#include <anode_block.h>
#include <volume_table.h> /* get epit_useOldFidIndex */
#include <fid.h>

#include <arpa/inet.h>

#if !defined(KERNEL)
#include <pthread.h>
#endif /* !KERNEL */

/* epid_IndexFromFid -- converts the index in a fid to a epiv-style index.
 *
 *     Do the arithmetic on epia-style indexes so that the page breaks appear
 *     at the correct places.  Break the index at the page boundary double the
 *     page number and put it back together.  This maps all fid-style indexes
 *     to epia-style index that live on even numbered volume table pages. */

SHARED u_long epid_IndexFromFid(fid, dev)
    IN epi_volFileId_t *fid;
IN struct async_device *dev;
{
  u_long i = epid_ToIndex(fid); /* get fid-style index */
  u_int page;
  u_int offset;
  u_int b = epic_anodesPerBlock[dev->logBlkSize - 10];
  u_long j;

  if (epit_useOldFidIndex)
    return i;

  i++;            /* to epia-style origin */
  page = i / b;   /* volume table page */
  offset = i % b; /* offset in vt page */

  j = (page << 1) * b + offset;
  j--; /* to epiv-style */
  return j;
}

/* epid_IndexForFid -- maps an epiv-style index to a fid-style index suitable
 *     for inserting into a fid.  It uses the inverse of the algorithm
 *     described above.
 *
 * RETURN CODES -- If the fid is unmappable, namely it would reside on an odd
 *     numbered volume table page, the function returns a zero.  Zero is
 *     invalid in every style. */

SHARED u_long epid_IndexForFid(i, dev)
    IN u_long i; /* epiv-style index */
IN struct async_device *dev;
{
  u_int page;
  u_int offset;
  u_int b = epic_anodesPerBlock[dev->logBlkSize - 10];
  u_long j;

  if (epit_useOldFidIndex)
    return i;

  i++;          /* to epia-style */
  page = i / b; /* volume table page */
  if (page & 1)
    return 0;     /* odd pages disallowed for fids */
  offset = i % b; /* offset in vt page */

  j = (page >> 1) * b + offset;
  j--; /* to fid-style */
  return j;
}

/* epid_LastIndex -- returns the larges legal fid-style index given the largest
 *     possible epiv-style index (e.g. the value epiv_lastIndex returns). */

EXPORT u_long epid_LastIndex(lastIndex, dev)
    IN u_long lastIndex; /* epiv-style index */
IN struct async_device *dev;
{
  u_long i;
  u_int page;
  u_int offset;
  u_int b = epic_anodesPerBlock[dev->logBlkSize - 10];
  u_long j;

  if (epit_useOldFidIndex)
    return lastIndex;

  i = lastIndex + 1; /* to epia-style */
  page = i / b;      /* volume table page */
  if (page & 1)
  {
    /* a non-file index maps to next smaller index for a file */
    offset = b - 1;
  }
  else
    offset = i % b; /* offset in vt page */

  j = (page >> 1) * b + offset;
  j--; /* to fid-style */
  return j;
}

#ifndef KERNEL

/* epid_PrintedFid -- produces a printed representation of a fid given the fid,
 *     the length of a buffer and the buffer itself.  Presently the buffer
 *     length is ignored, but should be at least 24 chars long. */

EXPORT char *epid_PrintedFid(fid, len, buf)
    IN epi_volFileId_t *fid;
IN int len;
OUT char *buf;
{
  buf[len - 1] = 0;

  sprintf(buf, "(%u,%u)", epid_ToIndex(fid), epid_ToUnique(fid));

  afsl_MBZ(buf[len - 1]); /* check for overwrite */
  return buf;
}

#endif /* !KERNEL */
