#!/bin/python
import subprocess
import os

'''
The purpose of this script is to build the external dependencies for logbuf layer.
It hard codes things, but I don't expect them to change, so if they do in the future,
someone else can fix it to be more general.

What are we building:

(1) the lex parser for testbuf
(2) the lex parser for testtran
(3) the parser for testbuf
(4) the parser for testtran
(5) the catalog for trace messages
(6) the catalog for error messages
'''


def run(args: list, stdout=None):
    if stdout != None:
        r = subprocess.run(args, stdout=stdout)
    else:
        r = subprocess.run(args)
    if (0 != r.returncode):
        print('Command <{}> failed with status {}'.format(
            ' '.join(args), r.returncode))
        exit(r.returncode)


print(os.getcwd())
run(['flex', '-o', 'testbuf_lex.c', 'testbuf_lex.l'])
run(['bison', '-vd', '--defines=testbuf_gram.h',
     '--output=testbuf_gram.c', 'testbuf_gram.y'])
# run(['flex', '-o', 'testtran_lex.c', 'testtran_lex.l'])
# run(['bison', '-vd', '--defines=testtran_gram.h',
#     '--output=testtran_gram.c', 'testtran_gram.y'])
