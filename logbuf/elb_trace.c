	N_("$Header: /u0/rcs_trees/dce/rcs/file/episode/logbuf/elb_trace.et,v 1.1.22.1 1996/10/02 17:24:58 damon Exp $"),
	N_("WaitForTran(%d): to complete from state %#x"),
	N_("WaitForTran(%d): wakeup in state %#x"),
	N_("WaitForTran(%d): start GCtran"),
	N_("WaitForTran(%d): end GCtran"),
	N_("WaitForTran(%d): start wait for any"),
	N_("WaitForTran(%d): end wait for any"),
	N_("CompleteEC: ecSize is %d"),
	N_("elbl_StartTran: logfull; %d running, active size data/tran=%d/%d"),
	N_("AllocBuffer: found no clean buffers in %dK pool"),
    0
};

struct error_table {
    char const * const * msgs;
    long base;
    int n_msgs;
};
struct et_list {
    struct et_list *next;
    const struct error_table * table;
};
extern struct et_list *_et_list;

const struct error_table et__error_table = { text, 0L, 10 };

static struct et_list link = { 0, 0 };

void initialize__error_table_r(struct et_list **list);
void initialize__error_table(void);

void initialize__error_table(void) {
    initialize__error_table_r(&_et_list);
}

/* For Heimdal compatibility */
void initialize__error_table_r(struct et_list **list)
{
    struct et_list *et, **end;

    for (end = list, et = *list; et; end = &et->next, et = et->next)
        if (et->table->msgs == text)
            return;
    et = malloc(sizeof(struct et_list));
    if (et == 0) {
        if (!link.table)
            et = &link;
        else
            return;
    }
    et->table = &et__error_table;
    et->next = 0;
    *end = et;
}
