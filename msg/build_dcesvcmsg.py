#!/bin/python
import subprocess
import argparse
import os

'''
The purpose of this script is to build the SAMS based messages.

What are we building:

TBD
'''


def run(args: list, stdout=None):
    if stdout != None:
        r = subprocess.run(args, stdout=stdout)
    else:
        r = subprocess.run(args)
    if (0 != r.returncode):
        print('Command <{}> failed with status {}'.format(
            ' '.join(args), r.returncode))
        exit(r.returncode)


def exists(fname: str) -> bool:
    e = os.path.exists(fname)
    if not e:
        print('{} does not exist'.format(fname))
        exit(1)
    return e


parser = argparse.ArgumentParser(description='Ancilliary build helper')
parser.add_argument('utility', type=str,
                    help='path to the utility to use')
parser.add_argument('input', type=str,
                    help='name of input file')
parser.add_argument('output', type=str,
                    help='name of output file')
args = parser.parse_args()

path, target = os.path.split(args.output)

assert '.c' == target[-2:], "Expected output to be a .c file name"

hfile = '{}.h'.format(target[:-2])
cfile = '{}.c'.format(target[:-2])

run([args.utility, args.input, '-ot'])
run([args.utility, args.input, '-oh'])

exists(hfile)
exists(cfile)

os.replace(hfile, '{}.h'.format(args.output[:-2]))
os.replace(cfile, '{}.c'.format(args.output[:-2]))

exit(0)
